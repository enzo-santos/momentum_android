package com.example.momentumapp.util

/**
 * Represents a item cache.
 */
interface Cache<K, V> {
    /**
     * The associated key to the cached value.
     *
     * This key can be used to do a fast-lookup to check if an item is cached.
     */
    var key: K?

    /**
     * The value to be cached.
     */
    var value: V?

    /**
     * Checks if there is no item cached.
     */
    val isEmpty: Boolean get() = key == null

    /**
     * Checks if there is a item cached.
     */
    val containsCache: Boolean get() = key != null

    /**
     * Check if there is a item cached associated with a [key].
     */
    fun isCached(key: K): Boolean = this.key == key

    /**
     * Returns the item cached.
     */
    fun loadCache(): V = value!!

    /**
     * Caches a [value] associated with a [key].
     */
    fun cache(key: K, value: V) {
        this.key = key
        this.value = value
    }
}

/**
 * Represents the cache of a record analysis.
 *
 * In this context, it's used to avoid remaking the analysis of a record.
 */
object RecordCache : Cache<String, AnalysisResult> {
    override var key: String? = null
    override var value: AnalysisResult? = null
}