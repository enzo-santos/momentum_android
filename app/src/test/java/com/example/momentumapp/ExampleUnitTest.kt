package com.example.momentumapp

import com.example.momentumapp.util.get
import com.example.momentumapp.util.i
import org.junit.Test
import org.junit.Assert.*
import org.apache.commons.math3.linear.EigenDecomposition
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue
import org.apache.commons.math3.linear.Array2DRowRealMatrix as Matriz

fun Array<DoubleArray>.repr() = "[" + joinToString(", ") { it.repr() } + "]"
fun Matriz.repr() = data.repr()
fun Double.repr() = String.format("%.3f", this).replace(",", ".")
fun DoubleArray.repr() = "[" + joinToString(", ") { it.repr() } + "]"
fun ArrayList<Pair<Double, Matriz>>.prepr() = "[" + joinToString(", ") { it.repr() } + "]"
fun ArrayList<DoubleArray>.repr() = "[" + joinToString(", ") { it.repr() } + "]"
fun Pair<Double, Matriz>.repr() = "(${first.repr()}, ${second.repr()})"

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class LDAUnitTest {
    private val features = arrayOf(
        arrayOf(6.7, 3.0, 5.2, 2.3),
        arrayOf(6.3, 2.5, 5.0, 1.9),
        arrayOf(6.5, 3.0, 5.2, 2.0),
        arrayOf(6.2, 3.4, 5.4, 2.3),
        arrayOf(5.9, 3.0, 5.1, 1.8),
    )

    private val classes = arrayOf(0, 1, 2, 1, 0)

    private val featuresMatrix = Matriz(features.map { it.toDoubleArray() }.toTypedArray())
    private val uniqueClasses = classes.toSet()

    private val numSamples = featuresMatrix.rowDimension
    private val numFeatures = featuresMatrix.columnDimension
    val numClasses = uniqueClasses.size

    private val meanVectors: ArrayList<DoubleArray>
        get() {
            val meanVectors = ArrayList<DoubleArray>()
            for (cl in uniqueClasses) {
                var meanVector = doubleArrayOf()
                val classSamples = (0 until numSamples).count { classes[it] == cl }

                for (c in 0 until numFeatures) {
                    var sum = 0.0

                    for (r in 0 until numSamples) {
                        if (classes[r] == cl) {
                            sum += featuresMatrix[r, c]
                        }
                    }

                    meanVector += sum / classSamples
                }

                meanVectors.add(meanVector)
            }

            return meanVectors
        }

    private val withinMatrix: Matriz
        get() {
            var withinMatrix = Matriz(numFeatures, numFeatures)
            for ((cl, mv) in uniqueClasses zip meanVectors) {
                var withinClass = Matriz(numFeatures, numFeatures)

                for ((r, feature) in features.withIndex()) {
                    if (classes[r] == cl) {
                        val featureMatrix = Matriz(feature.toDoubleArray())
                        val meanVecMatrix = Matriz(mv)
                        val diffMatrix = featureMatrix.subtract(meanVecMatrix)
                        withinClass = withinClass.add(diffMatrix.multiply(diffMatrix.transpose())) as Matriz
                    }
                }

                withinMatrix = withinMatrix.add(withinClass)
            }

            return withinMatrix
        }

    private val overallMean: DoubleArray
        get() {
            var overallMean = doubleArrayOf()
            for (c in 0 until numFeatures) {
                var sum = 0.0
                for (r in 0 until numSamples) {
                    sum += featuresMatrix[r, c]
                }
                overallMean += sum / numSamples
            }

            return overallMean
        }

    private val betweenMatrix: Matriz
        get() {
            var betweenMatrix = Matriz(numFeatures, numFeatures)
            for ((cl, mv) in meanVectors.withIndex()) {
                val numClass = classes.count { it == cl }.toDouble()
                val meanVecMatrix = Matriz(mv)
                val overallMatrix = Matriz(overallMean)
                val diffMatrix = meanVecMatrix.subtract(overallMatrix)
                betweenMatrix = betweenMatrix.add(diffMatrix.multiply(diffMatrix.transpose()).scalarMultiply(numClass)) as Matriz
            }

            return betweenMatrix
        }

    private val eigenPairs: ArrayList<Pair<Double, Matriz>>
        get() {
            val eigenDec = EigenDecomposition(withinMatrix.i.multiply(betweenMatrix))
            val eigenVals = eigenDec.d as Matriz
            val eigenVecs = eigenDec.v as Matriz

            val eigenPairs = arrayListOf<Pair<Double, Matriz>>()
            for (i in 0 until numFeatures) {
                eigenPairs.add(eigenVals[0, i].absoluteValue to eigenVecs.getColumnMatrix(i) as Matriz)
            }

            eigenPairs.sortWith(compareBy { it.first })
            eigenPairs.reverse()

            return eigenPairs
        }

    private val reducedMatrix: Matriz
        get() {
            val reducedMatrix = Matriz(numFeatures, 2)
            reducedMatrix.setColumnMatrix(0, eigenPairs[0].second)
            reducedMatrix.setColumnMatrix(1, eigenPairs[1].second)
            return reducedMatrix
        }

    private val ldaMatrix = featuresMatrix.multiply(reducedMatrix)

    @Test
    fun meanVectors_isCorrect() {
        /*  PYTHON CODE:
         *
         *  >>> mean_vectors = []
         *  >>> for c in range(3):
         *  ...     mean_vectors.plot(np.mean(x[y == c], axis=0))
         *  ...
         *  >>> mean_vectors
         *  [array([6.3 , 3.  , 5.15, 2.05]),
         *   array([6.25, 2.95, 5.2 , 2.1 ]),
         *   array([6.5 , 3.  , 5.2 , 2.  ])]
         */
        val e = arrayListOf(
            doubleArrayOf(6.30, 3.00, 5.15, 2.05),
            doubleArrayOf(6.25, 2.95, 5.20, 2.10),
            doubleArrayOf(6.50, 3.00, 5.20, 2.00)
        )

        val a = meanVectors

        assertEquals(e.repr(), a.repr())
    }

    @Test
    fun withinMatrix_isCorrect() {
        /*  PYTHON CODE:
         *
         *  >>> S_W = np.zeros((4, 4))
         *  >>> for cl, mv in zip(range(3), mean_vectors):
         *  ...     class_sc_mat = np.zeros((4, 4))
         *  ...     for row in x[y == cl]:
         *  ...         row, mv = row.reshape(4, 1), mv.reshape(4, 1)
         *  ...         class_sc_mat += (row-mv).dot((row-mv).T)
         *  ...     S_W += class_sc_mat
         *  ...
         *  >>> S_W
         *  array([[ 0.325, -0.045,  0.02 ,  0.18 ],
         *         [-0.045,  0.405,  0.18 ,  0.18 ],
         *         [ 0.02 ,  0.18 ,  0.085,  0.105],
         *         [ 0.18 ,  0.18 ,  0.105,  0.205]])
         */
        val e = Matriz(arrayOf(
            doubleArrayOf( 0.325, -0.045,  0.020,  0.180),
            doubleArrayOf(-0.045,  0.405,  0.180,  0.180),
            doubleArrayOf( 0.020,  0.180,  0.085,  0.105),
            doubleArrayOf( 0.180,  0.180,  0.105,  0.205),
        ))

        val a = withinMatrix

        assertEquals(e.repr(), a.repr())
    }

    @Test
    fun overallMean_isCorrect() {
        /*  PYTHON CODE:
         *
         *  >>> overall_mean = np.mean(x, axis=0)
         *  >>> overall_mean
         *  array([6.32, 2.98, 5.18, 2.06])
         */
        val e = doubleArrayOf(6.32, 2.98, 5.18, 2.06)

        val a = overallMean

        assertEquals(e.repr(), a.repr())
    }

    @Test
    fun betweenMatrix_isCorrect() {
        /*  PYTHON CODE:
         *
         *  >>> S_B = np.zeros((4, 4))
         *  >>> for i, mean_vec in enumerate(mean_vectors):
         *  ...     n = x[y == i, :].shape[0]
         *  ...     mv = mean_vec.reshape(4, 1)
         *  ...     om = overall_mean.reshape(4, 1)
         *  ...     S_B += n * (mv-om).dot((mv-om).T)
         *  ...
         *  >>> S_B
         *  array([[ 0.043,  0.007,  0.002, -0.016],
         *         [ 0.007,  0.003, -0.002, -0.004],
         *         [ 0.002, -0.002,  0.003,  0.001],
         *         [-0.016, -0.004,  0.001,  0.007]])
         */
        val e = Matriz(arrayOf(
            doubleArrayOf( 0.043,  0.007,  0.002, -0.016),
            doubleArrayOf( 0.007,  0.003, -0.002, -0.004),
            doubleArrayOf( 0.002, -0.002,  0.003,  0.001),
            doubleArrayOf(-0.016, -0.004,  0.001,  0.007),
        ))

        val a = betweenMatrix

        assertEquals(e.repr(), a.repr())
    }

    @Test
    fun eigenPairs_isCorrect() {
        /*  PYTHON CODE:
         *
         *  >>> eig_vals, eig_vecs = np.linalg.eig(np.linalg.inv(S_W).dot(S_B))
         *  >>> eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]
         *  >>> eig_pairs = sorted(eig_pairs, key=lambda k: k[0], reverse=True)
         *  >>> eig_pairs
         *  [(2830461888494487.5, array([-0.42078747, -0.18489649, -0.45304966,  0.76386989])),
         *   (200443106103233.25, array([-0.0548872 , -0.37270041,  0.92129327, -0.09643914])),
         *   (0.3440555268438658, array([ 0.31573127, -0.32064282, -0.63391832,  0.62900676])),
         *   (0.05840082669241646, array([0.12578122, 0.71188021, 0.16726827, 0.67039315]))]
         *
         *  No caso do exemplo, a matriz 'withinMatrix' é singular, ie, não é inversível.
         *  Por isso, para encontrar a matriz inversa, será utilizado SingularValueDecomposition
         *  ao invés de LUDecomposition, visto que o último lança o erro SingularMatrixException.
         *
         *  val e = arrayListOf(
         *       2830461888494487.5 to Matriz(doubleArrayOf(-0.42078747, -0.18489649, -0.45304966,  0.76386989)),
         *       200443106103233.25 to Matriz(doubleArrayOf(-0.0548872 , -0.37270041,  0.92129327, -0.09643914)),
         *       0.3440555268438658 to Matriz(doubleArrayOf( 0.31573127, -0.32064282, -0.63391832,  0.62900676)),
         *      0.05840082669241646 to Matriz(doubleArrayOf(0.12578122, 0.71188021, 0.16726827, 0.67039315))
         *  )
         */
        val e = arrayListOf(
            0.048 to Matriz(doubleArrayOf(-0.870,  0.303,  0.030, -0.388)),
            0.000 to Matriz(doubleArrayOf( 0.501, -4.399, -2.951, -0.947)),
            0.000 to Matriz(doubleArrayOf( 0.987, 41.252, 19.146, 23.093)),
            0.000 to Matriz(doubleArrayOf(-0.009, -0.824, -0.379, -0.429))
        )

        val a = eigenPairs

        assertEquals(e.prepr(), a.prepr())
    }

    @Test
    fun reducedMatrix_isCorrect() {
        val e = Matriz(arrayOf(
            doubleArrayOf(-0.870,  0.501),
            doubleArrayOf( 0.303, -4.399),
            doubleArrayOf( 0.030, -2.951),
            doubleArrayOf(-0.388, -0.947)
        ))

        val a = reducedMatrix

        assertEquals(e.repr(), a.repr())
    }

    @Test
    fun ldaMatrix_isCorrect() {
        assertTrue(ldaMatrix.rowDimension == featuresMatrix.rowDimension)
        assertTrue(ldaMatrix.columnDimension == 2)
    }
}
