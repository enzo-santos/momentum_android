package com.example.momentumapp.option

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.example.momentumapp.R
import com.example.momentumapp.util.*

class OptionMutableType<T>(
    title: String,
    saveKey: String,
    val hint: String,
    val description: String,
    var value: T,
    val typeEncoding: StringEncoding<T>,
    val typeDecoding: StringDecoding<T>,
) : BaseOptionType(title, OPTION_TYPE_MUTABLE, saveKey) {
    class Builder<T> {
        private var title: String? = null
        private var key: String? = null
        private var hint: String? = null
        private var value: T? = null
        private var description: String? = null
        private var typeEncoding: StringEncoding<T>? = null
        private var typeDecoding: StringDecoding<T>? = null

        fun setTitle(value: String) = apply { title = value }
        fun setDefaultValue(value: T) = apply { this.value = value }
        fun setKey(value: String) = apply { key = value }
        fun setHint(value: String) = apply { hint = value }
        fun setDescription(value: String) = apply { description = value }
        fun setTypeEncoding(value: StringEncoding<T>) = apply { typeEncoding = value }
        fun setTypeDecoding(value: StringDecoding<T>) = apply { typeDecoding = value }
        fun build(): OptionMutableType<T> = OptionMutableType(
            title = title ?: error("'setTitle' must be called"),
            saveKey = key ?: error("'setKey' must be called"),
            hint = hint ?: error("'setHint' must be called"),
            value = value ?: error("'setDefaultValue' must be called"),
            description = description ?: error("'setDescription' must be called"),
            typeDecoding = typeDecoding ?: error("'setTypeDecoding' must be called"),
            typeEncoding = typeEncoding ?: { value -> value.toString() }
        )
    }

    class ViewHolder(
        view: View,
        container: OptionContainer,
    ) : BaseViewHolder(view) {
        override val txtOptionTitle: TextView = view.findViewById(R.id.txt_sel_option_title)
        override val txtOptionDescription: TextView =
            view.findViewById(R.id.txt_sel_option_description)
        override val layoutOption: LinearLayout = view.findViewById(R.id.lly_sel_option)

        init {
            layoutOption.setOnClickListener {
                container.onOptionClicked(absoluteAdapterPosition)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder) {
        holder.txtOptionTitle.text = title
        holder.txtOptionDescription.text = description.format(value)
    }

    override fun getDialog(context: Context, action: () -> Unit): AlertDialog =
        AlertDialog.Builder(context, R.style.AppDialog).let { builder ->
            val layout = (context as Activity).inflate(R.layout.dialog_editable)
            val edtNomeArq = layout.findViewById<EditText>(R.id.edtDialog).also { edt ->
                edt.setText(typeEncoding(value))
                edt.hint = hint
                edt.inputType = InputType.TYPE_CLASS_NUMBER
            }

            builder.setTitle(title)
                .setView(layout)
                .setPositiveButton(context.getString(R.string.ok)) { dialog, _ ->
                    val text = edtNomeArq.text.toString()
                    if (text.isNotEmpty()) {
                        value = typeDecoding(text)
                        save(context, value)
                        action()
                    }

                    dialog.dismiss()

                }.create()
        }
}