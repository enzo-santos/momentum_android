package com.example.momentumapp.util

/**
 * Provided functionality for a frequently-updated metric.
 */
interface Metric {
    /**
     * The actual value for this metric.
     *
     * If it's not possible to determine the actual value, its value is null.
     */
    var value: Double?

    /**
     * The previous value of this metric.
     *
     * If it's not possible to determine the old value, its value is null.
     */
    var oldValue: Double?

    /**
     * Function used to generate a new value given two values related to each other.
     *
     * In this context, its result should update the [value] property, given the [oldValue] property
     * as first parameter and a new value as second parameter, that can be obtained in each call of
     * the [update] method by its parameter.
     */
    val calculate: (Double, Double) -> Double

    /**
     * Updates a [newValue] of this metric.
     */
    fun update(newValue: Double)
}

/**
 * Represents a acceleration metric.
 *
 * It must be passed a value representing an acceleration value in the [update] method.
 */
open class AccelerationMetric(
    override val calculate: (Double, Double) -> Double,
) : Metric {
    override var oldValue: Double? = null
    override var value: Double? = null

    override fun update(newValue: Double) {
        val oldValue = oldValue
        if (oldValue != null)
            value = calculate(oldValue, newValue)

        this.oldValue = newValue
    }
}

/**
 * Represents a rotation metric.
 *
 * It must be passed a value representing an rotation value in the [update] method.
 */
class RotationMetric(
    override val calculate: (Double, Double) -> Double,
) : Metric {
    override var oldValue: Double? = null
    override var value: Double? = null

    override fun update(newValue: Double) {
        val oldValue = oldValue
        if (oldValue != null)
            value = calculate(oldValue, newValue)

        this.oldValue = newValue
    }
}

/**
 * Represents a metric of a three-axis sensor.
 */
interface SensorMetric {
    /**
     * Function used to generate a new value given two values related to each other.
     *
     * In this context, its result should update the [Metric.value] property for each axis, given
     * the [Metric.oldValue] property as first parameter and a new value as second parameter, that
     * can be obtained in each call of the [updateX], [updateY], [updateZ] or [update] methods by
     * their parameters and their corresponding axis.
     */
    val calculate: (Double, Double) -> Double

    /**
     * The metrics related to this sensor.
     *
     * It must have at least three elements: one for each sensor axis.
     */
    val metrics: List<Metric>

    /**
     * The value of the metric corresponding to the x-axis.
     *
     * If it's not possible to determine the actual value for this axis, its value is null.
     */
    val xValue: Double? get() = metrics[0].value

    /**
     * The value of the metric corresponding to the y-axis.
     *
     * If it's not possible to determine the actual value for this axis, its value is null.
     */
    val yValue: Double? get() = metrics[1].value

    /**
     * The value of the metric corresponding to the z-axis.
     *
     * If it's not possible to determine the actual value for this axis, its value is null.
     */
    val zValue: Double? get() = metrics[2].value

    /**
     * The values of the metrics for each sensor axis.
     *
     * If it's not possible to determine all the values for each axis, its value is null.
     */
    val values: List<Double>?
        get() = metrics.mapNotNull(Metric::value).let { values ->
            values.takeIf { values.size == metrics.size }
        }

    /**
     * Updates a [newValue] for the metric corresponding to the x-axis.
     */
    fun updateX(newValue: Double) = metrics[0].update(newValue)

    /**
     * Updates a [newValue] for the metric corresponding to the y-axis.
     */
    fun updateY(newValue: Double) = metrics[1].update(newValue)

    /**
     * Updates a [newValue] for the metric corresponding to the z-axis.
     */
    fun updateZ(newValue: Double) = metrics[2].update(newValue)

    /**
     * Updates the metric for each sensor axis.
     */
    fun update(newXValue: Double, newYValue: Double, newZValue: Double) {
        updateX(newXValue)
        updateY(newYValue)
        updateZ(newZValue)
    }
}

/**
 * Represents a sensor metric that can be represented in a file.
 */
abstract class WritableSensorMetric(
    private val decimalSeparator: String,
    private val decimalPlaces: Int,
) : SensorMetric, Writable {
    override val content: String
        get() = "${xValue?.toString(decimalSeparator, decimalPlaces)}" + FILE_SEPARATOR +
                "${yValue?.toString(decimalSeparator, decimalPlaces)}" + FILE_SEPARATOR +
                "${zValue?.toString(decimalSeparator, decimalPlaces)}"
}

/**
 * Represents a accelerometer metric.
 */
class AccelerometerMetric(
    decimalSeparator: String,
    decimalPlaces: Int,
    override val calculate: (Double, Double) -> Double,
) : WritableSensorMetric(decimalSeparator, decimalPlaces) {
    override val header: String = "ACC EIXO X${FILE_SEPARATOR}ACC EIXO Y${FILE_SEPARATOR}ACC EIXO Z"
    override val metrics: List<AccelerationMetric> = listOf(
        AccelerationMetric(calculate),
        AccelerationMetric(calculate),
        AccelerationMetric(calculate)
    )
}

/**
 * Represents a gyroscope metric.
 */
class GyroscopeMetric(
    decimalSeparator: String,
    decimalPlaces: Int,
    override val calculate: (Double, Double) -> Double,
) : WritableSensorMetric(decimalSeparator, decimalPlaces) {
    override val header: String = "GIR EIXO X${FILE_SEPARATOR}GIR EIXO Y${FILE_SEPARATOR}GIR EIXO Z"
    override val metrics: List<RotationMetric> = listOf(
        RotationMetric(calculate),
        RotationMetric(calculate),
        RotationMetric(calculate)
    )
}