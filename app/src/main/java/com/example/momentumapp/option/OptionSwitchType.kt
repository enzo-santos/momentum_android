package com.example.momentumapp.option

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.momentumapp.R
import com.example.momentumapp.util.OPTION_TYPE_SWITCH
import com.example.momentumapp.util.prefs
import com.example.momentumapp.util.toInt

/**
 * Implementation of a binary option.
 */
class OptionSwitchType(
    title: String,
    key: String,
    var descriptions: Array<String>,
    var isSwitchOn: Boolean,
) : BaseOptionType(title, OPTION_TYPE_SWITCH, key) {

    class Builder(private val context: Context) {
        private var optionTitle = ""
        private var optionKey = ""
        private var optionDescriptionOn = ""
        private var optionDescriptionOff = ""
        private var isOnValueDefault = false

        fun setTitle(value: String) = apply { optionTitle = value }
        fun setKey(value: String) = apply { optionKey = value }
        fun setOnValue(value: String) = apply { optionDescriptionOn = value }
        fun setOffValue(value: String) = apply { optionDescriptionOff = value }
        fun setOnValueAsDefault() = apply { isOnValueDefault = true }
        fun setOffValueAsDefault() = apply { isOnValueDefault = false }

        fun build() = OptionSwitchType(
            title = optionTitle,
            key = optionKey,
            descriptions = arrayOf(optionDescriptionOff, optionDescriptionOn),
            isSwitchOn = context.prefs.getBoolean(optionKey, isOnValueDefault)
        )
    }

    class ViewHolder(
        view: View,
        adapter: RecyclerView.Adapter<*>,
        options: MutableList<BaseOptionType>,
    ) : BaseViewHolder(view) {
        override val txtOptionTitle: TextView = view.findViewById(R.id.txt_edt_option_title)
        override val txtOptionDescription: TextView =
            view.findViewById(R.id.txt_edt_option_description)
        override val layoutOption: LinearLayout = view.findViewById(R.id.lly_edt_option)
        val swtOption: Switch = view.findViewById(R.id.swt_option_enable)
        var isUserCheck = true

        init {
            swtOption.setOnCheckedChangeListener { _, isChecked ->
                val option = options[absoluteAdapterPosition]
                if (isUserCheck && option is OptionSwitchType) {
                    option.isSwitchOn = isChecked
                    option.save(view.context, isChecked)

                    adapter.notifyItemChanged(absoluteAdapterPosition)
                }
            }
        }
    }

    override fun getDialog(context: Context, action: () -> Unit): AlertDialog? = null

    override fun onBindViewHolder(holder: BaseViewHolder) {
        if (holder !is ViewHolder) return

        holder.txtOptionTitle.text = title

        holder.isUserCheck = false
        holder.swtOption.isChecked = isSwitchOn
        holder.isUserCheck = true

        holder.txtOptionDescription.text = descriptions[isSwitchOn.toInt()]
    }
}