package com.example.momentumapp.util

/**
 * Represents the data of a recording metric.
 */
data class RecordData(
    var duration: DoubleArray,
    var xAxis: DoubleArray,
    var yAxis: DoubleArray,
    var zAxis: DoubleArray,
) {
    constructor() : this(doubleArrayOf(), doubleArrayOf(), doubleArrayOf(), doubleArrayOf())

    /**
     * Converts the information of this metric to the frequency domain.
     */
    fun toRFFT() = RecordData().also { record ->
        val normalize: DoubleArray.() -> DoubleArray
        normalize = { drop(1).map { v -> v / size }.toDoubleArray() }

        record.xAxis = xAxis.toComplexArray().rfft().abs().normalize()
        record.yAxis = yAxis.toComplexArray().rfft().abs().normalize()
        record.zAxis = zAxis.toComplexArray().rfft().abs().normalize()
        record.duration = linspace(record.xAxis.size, 1 / (duration[1] - duration[0]))
    }

    operator fun get(index: Int): List<Double> =
        listOf(duration[index], xAxis[index], yAxis[index], zAxis[index])

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RecordData

        if (!duration.contentEquals(other.duration)) return false
        if (!xAxis.contentEquals(other.xAxis)) return false
        if (!yAxis.contentEquals(other.yAxis)) return false
        if (!zAxis.contentEquals(other.zAxis)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = duration.contentHashCode()
        result = 31 * result + xAxis.contentHashCode()
        result = 31 * result + yAxis.contentHashCode()
        result = 31 * result + zAxis.contentHashCode()
        return result
    }
}