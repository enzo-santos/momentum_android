package com.example.momentumapp.plotting

import com.example.momentumapp.util.Plotting

/**
 * Manages multiple sensor plotters.
 */
abstract class MultiSensorPlotter(
    /**
     * The sensor plotters to be managed.
     */
    private val sensorPlotters: MutableList<GraphSensorPlotter>,
) : CacheSensorPlotter {
    /**
     * The actual plotter index that will be plotted when calling the [plot] method.
     */
    protected abstract var plotterIndex: Int

    /**
     * Adds a [sensorPlotter] to be managed.
     */
    fun addSensorPlotter(sensorPlotter: GraphSensorPlotter) {
        sensorPlotters.add(sensorPlotter)
    }

    /**
     * Sets the plotter at the given [index] that will be plotted when calling the [plot] method.
     */
    fun setPlotter(index: Int): MultiSensorPlotter = apply {
        plotterIndex = index
    }

    override fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    ) {
        sensorPlotters[plotterIndex].plot(xAxisData, yAxisData, zAxisData, colors, cache, plotting)
    }
}