package com.example.momentumapp.util

/**
 * Represents a recording manager.
 */
interface Recorder {
    /**
     * Starts the recording.
     */
    fun startRecording()

    /**
     * Stops the recording, if started.
     */
    fun stopRecording()

    /**
     * Pauses the recording, if started.
     */
    fun pauseRecording()

    /**
     * Resumes the recording, if paused.
     */
    fun resumeRecording()
}