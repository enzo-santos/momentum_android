package com.example.momentumapp

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.os.Bundle
import com.example.momentumapp.plotting.InertialMultiSensorPlotter
import com.example.momentumapp.util.KEY_IS_PARENT_ACTIVITY
import com.example.momentumapp.util.TimePrecisionFilter
import com.example.momentumapp.util.launchActivity
import java.io.FileWriter

open class MainRecorderActivity : InertialRecorderActivity() {
    private var writer: FileWriter? = null
    private lateinit var timeFilter: TimePrecisionFilter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        timeFilter = TimePrecisionFilter(recordingDelay)

        startRecording()
        btnStop.setOnClickListener {
            stopRecording()
        }
    }

    override fun startRecording() {
        startRegistering()
        writer = createWriter { FileWriter(fileDirectory, false) }
        timeFilter.start()
    }

    override fun stopRecording() {
        timeFilter.stop()
        stopRegistering()
        writer?.closeIfContent()

        if (intent?.getBooleanExtra(KEY_IS_PARENT_ACTIVITY, false) != true) {
            launchActivity<MainActivity>()
        }

        finish()
    }

    override fun pauseRecording() {
        timeFilter.pause()
    }

    override fun resumeRecording() {
        timeFilter.resume()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        sensorContent.update(event)

        timeFilter.use {
            writer?.writeContent()
            checkForLineLimit()
            sensorPlotter.updateGraphs()

            sensorContent.duration += recordingDelay
        }
    }
}