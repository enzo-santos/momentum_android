package com.example.momentumapp.plotting

import com.example.momentumapp.util.Plotting

/**
 * Provided functionality for a three-axis sensor plotter.
 */
interface SensorPlotter {
    /**
     * Plots the [xAxisData], the [yAxisData] and the [zAxisData] given its respective [colors]
     * and a [plotting] action.
     */
    fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        plotting: Plotting,
    )
}