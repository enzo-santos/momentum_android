package com.example.momentumapp.util

import org.apache.commons.math3.linear.EigenDecomposition
import org.apache.commons.math3.linear.LUDecomposition
import org.apache.commons.math3.linear.SingularMatrixException
import org.apache.commons.math3.linear.SingularValueDecomposition
import kotlin.math.*
import org.apache.commons.math3.linear.Array2DRowRealMatrix as Matrix

/**
 * Represents a complex number.
 */
class Complex(
    /**
     * The real part of this number.
     */
    val a: Double,

    /**
     * The imaginary part of this number.
     */
    val b: Double,
) {
    companion object {
        /**
         * Creates a Complex number from a real number [x].
         */
        fun fromReal(x: Double) = Complex(x, .0)

        /**
         * Creates a Complex number from an angle [x] in radians.
         */
        fun fromAngle(x: Double) = Complex(cos(x), sin(x))
    }

    operator fun plus(other: Complex) =
        Complex(a + other.a, b + other.b)

    operator fun minus(other: Complex) =
        Complex(a - other.a, b - other.b)

    operator fun times(other: Complex) =
        Complex(a * other.a - b * other.b, a * other.b + b * other.a)

    operator fun div(other: Complex) =
        Complex(
            (a * other.a + b * other.b) / (other.a * other.a + other.b * other.b),
            (b * other.a - a * other.b) / (other.a * other.a + other.b * other.b)
        )

    val abs: Double
        get() = sqrt(a * a + b * b)

    override fun toString() = "%.3f%+.3fi".format(a, b)
}

/**
 * Returns a Complex array containing all the elements of this collection.
 */
fun DoubleArray.toComplexArray() = map { v -> Complex.fromReal(v) }.toTypedArray()

/**
 * Returns the sum of all the elements of this Complex array.
 */
fun Array<Complex>.sum() = fold(Complex(.0, .0)) { acc, value -> acc + value }

/**
 * Returns the absolute value of all the elements of this Complex array.
 */
fun Array<Complex>.abs() = map(Complex::abs).toDoubleArray()

/**
 * Returns the Discrete Fourier Transform of a Complex array.
 */
fun Array<Complex>.fft() = Array(size) { i ->
    Array(size) { j -> Complex.fromAngle(-PI * 2 * i * j / size) * this[j] }.sum()
}

/**
 * Returns the Discrete Fourier Transform for a real input.
 */
fun Array<Complex>.rfft() = fft().slice(0..(size / 2 + 1)).toTypedArray()

/**
 * Returns [num] evenly spaced samples, calculated over the interval [0, [max]].
 */
fun linspace(num: Int, max: Double) = DoubleArray(num) { i -> i * max / (num - 1.0) }

/**
 * Return the maximum of an array.
 */
fun DoubleArray.amax() = maxOrNull() ?: .0

/**
 * Returns the element in an array [xData] in the same position as the maximum of array [yData].
 */
fun amax(xData: DoubleArray, yData: DoubleArray): Pair<Double, Double> {
    val peak = yData.amax()

    return xData
        .filterIndexed { i, _ -> yData[i] == peak }
        .firstOrNull()
        ?.let { Pair(it, peak) } ?: Pair(xData.last(), peak)
}

/**
 * Return the minimum of an array.
 */
fun DoubleArray.amin() = minOrNull() ?: .0

/**
 * Returns the difference between the maximum and the minimum values of an array.
 */
fun DoubleArray.range() = amax() - amin()

/**
 * Returns the average of this array elements.
 */
fun DoubleArray.mean() = average()

/**
 * Returns the variance of this array elements.
 */
fun DoubleArray.variance() = fold(.0) { acc, value ->
    acc + (value - mean()).pow(2)
} / size

/**
 * Returns the standard deviation of this array elements.
 */
fun DoubleArray.stdev() = sqrt(variance())

/**
 * Returns the skewness of this array elements.
 */
fun DoubleArray.skewness() = fold(.0) { acc, value ->
    acc + ((value - mean()).pow(3) / stdev().pow(3))
} / size

/**
 * Returns the kurtosis of this array elements.
 */
fun DoubleArray.kurtosis() = fold(.0) { acc, value ->
    acc + ((value - mean()).pow(4) / stdev().pow(4))
} / size

/**
 * Returns the mean square of this array elements.
 */
fun DoubleArray.meanSquare() = fold(.0) { acc, value -> acc + value.pow(2) } / size

/**
 * Integrate [yData] ([xData]) using the trapezoidal rule.
 */
fun trapz(xData: DoubleArray, yData: DoubleArray): Double =
    (1 until xData.size).sumByDouble { i -> (xData[i] - xData[i - 1]) * (yData[i] + yData[i - 1]) / 2 }

/**
 * Returns a sequence of numbers starting from [start], incrementing by [step] and stopping before [stop].
 *
 * [step] must be positive.
 */
fun arange(start: Double, stop: Double, step: Double): DoubleArray {
    require(step > 0) { "step must be positive" }
    val size = floor((stop - start - 1) / step).toInt() + 1
    return DoubleArray(size) { i -> start + i * step }
}

/**
 * Returns the mean frequency from elements of [yData] based on a time-domain signal [xData].
 */
fun meanFrequency(xData: DoubleArray, yData: DoubleArray): Pair<Double, Double> {
    val totalArea = trapz(xData, yData)

    return xData.indices.firstOrNull { i ->
        val xPartial = xData.slice(0 until i).toDoubleArray()
        val yPartial = yData.slice(0 until i).toDoubleArray()
        trapz(xPartial, yPartial) > totalArea / 2.0

    }?.let { i -> Pair(xData[i - 1], yData[i - 1]) } ?: Pair(xData.last(), yData.last())
}

val Matrix.t: Matrix
    get() = transpose() as Matrix

val Matrix.i: Matrix
    get() = try {
        LUDecomposition(this).solver.inverse

    } catch (e: SingularMatrixException) {
        SingularValueDecomposition(this).solver.inverse
    } as Matrix


operator fun Matrix.get(row: Int, col: Int) = getEntry(row, col)
operator fun Matrix.plus(other: Matrix): Matrix = add(other)
operator fun Matrix.minus(other: Matrix): Matrix = subtract(other)
operator fun Matrix.times(other: Matrix): Matrix = multiply(other)
operator fun Matrix.times(other: Double): Matrix = scalarMultiply(other) as Matrix

fun Array<DoubleArray>.getLDA(classes: IntArray): Array<DoubleArray> {
    val featuresMatrix = Matrix(this)
    val nRows = featuresMatrix.rowDimension
    val nCols = featuresMatrix.columnDimension
    require(nRows == classes.size)
    val uniqueClasses = classes.toSet()

    // Calculate mean vectors
    val meanVectors = uniqueClasses.map { `class` ->
        // Count how many individual classes are in the dataset
        val numClassSamples = classes.count { c -> c == `class` }

        DoubleArray(nCols) { col ->
            (0 until nRows)
                .filter { row -> classes[row] == `class` }
                .sumByDouble { row -> featuresMatrix[row, col] } / numClassSamples
        }
    }

    // Calculate within-class scatter matrix
    var withinMatrix = Matrix(nCols, nCols)
    (uniqueClasses zip meanVectors).forEach { (`class`, meanVector) ->
        var withinClassMatrix = Matrix(nCols, nCols)

        filterIndexed { row, _ -> classes[row] == `class` }.forEach { feature ->
            val diffMatrix = Matrix(feature) - Matrix(meanVector)
            withinClassMatrix += diffMatrix * diffMatrix.t
        }

        withinMatrix += withinClassMatrix
    }

    // Calculate overall mean
    val overallMean = DoubleArray(nCols) { col ->
        (0 until nRows).sumByDouble { row -> featuresMatrix[row, col] } / nRows
    }

    // Calculate between-class matrix
    var betweenMatrix = Matrix(nCols, nCols)
    meanVectors.forEachIndexed { `class`, meanVector ->
        val numClassSamples = classes.count { c -> c == `class` }.toDouble()
        val diffMatrix = Matrix(meanVector) - Matrix(overallMean)
        betweenMatrix += (diffMatrix * diffMatrix.t) * numClassSamples
    }

    // Calculate linear discriminants
    val eigenDec = EigenDecomposition(withinMatrix.i * betweenMatrix)
    val eigenValues = eigenDec.d as Matrix
    val eigenVectors = eigenDec.v as Matrix

    val eigenPairs = (0 until nCols).map { col ->
        eigenValues[0, col].absoluteValue to eigenVectors.getColumnMatrix(col) as Matrix
    }.sortedByDescending { pair -> pair.first }.map { pair -> pair.second }

    // Calculating reduced matrix
    val reducedMatrix = Matrix(nCols, 2)
    reducedMatrix.setColumnMatrix(0, eigenPairs[0])
    reducedMatrix.setColumnMatrix(1, eigenPairs[1])

    val ldaMatrix = featuresMatrix * reducedMatrix
    return ldaMatrix.data
}