package com.example.momentumapp.layout

import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import com.example.momentumapp.modules.fml.Range
import com.jjoe64.graphview.GraphView

fun View.expand() {
    measure(
        View.MeasureSpec.makeMeasureSpec(
            (parent as View).width,
            View.MeasureSpec.EXACTLY
        ),
        View.MeasureSpec.makeMeasureSpec(
            0,
            View.MeasureSpec.UNSPECIFIED
        ),
    )

    val targetHeight = measuredHeight
    layoutParams.height = 1
    visibility = View.VISIBLE

    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            layoutParams.height = if (interpolatedTime == 1f)
                ViewGroup.LayoutParams.WRAP_CONTENT
            else
                (targetHeight * interpolatedTime).toInt()

            requestLayout()
        }

        override fun willChangeBounds(): Boolean = true
    }

    a.duration = (targetHeight / context.resources.displayMetrics.density).toLong()
    startAnimation(a)
}

fun View.collapse() {
    val initialHeight = measuredHeight
    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            if (interpolatedTime == 1f) {
                visibility = View.GONE
            } else {
                layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean = true
    }

    a.duration = (initialHeight / context.resources.displayMetrics.density).toLong()
    startAnimation(a)
}

var GraphView.horizontalRange: Range
    get() = Range(
        viewport.getMinX(true),
        viewport.getMaxX(true),
    )
    set(range) {
        viewport.setMinX(range.minimum)
        viewport.setMaxX(range.maximum)
        viewport.isXAxisBoundsManual = true
    }

var GraphView.verticalRange: Range
    get() = Range(
        viewport.getMinY(true),
        viewport.getMaxY(true),
    )
    set(range) {
        viewport.setMinY(range.minimum)
        viewport.setMaxY(range.maximum)
        viewport.isYAxisBoundsManual = true
    }