package com.example.momentumapp.util

import android.os.AsyncTask
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.BluetoothRecorderActivity
import com.example.momentumapp.R
import java.lang.ref.WeakReference

class RecordingConnectionTask(
    activity: BluetoothRecorderActivity,
) : AsyncTask<Void, Void?, Void?>() {
    private val weakReference = WeakReference(activity)
    private val bluetoothManager: BluetoothManager? = activity.bluetoothManager
    private var progressDialog: AlertDialog? = null

    override fun onPreExecute() {
        weakReference.get()?.also { activity ->
            progressDialog =
                AlertDialog.Builder(activity)
                    .setView(activity.inflate(R.layout.dialog_progress))
                    .setCancelable(false)
                    .setNegativeButton(activity.getString(R.string.cancel)) { dialog, _ ->
                        dialog.cancel()

                        bluetoothManager?.stopSearching()
                        activity.stopRecording()
                        cancel(true)
                        activity.toast(activity.getString(R.string.recorder_bluetooth_toast_search_cancelled))
                    }
                    .create()

            progressDialog?.show()
            progressDialog?.window?.setBackgroundDrawableResource(R.color.colorToolbar)
        }
    }

    override fun doInBackground(vararg params: Void?): Void? {
        while (bluetoothManager?.actualState != BluetoothManager.STATUS_CONNECTED) {
            if (isCancelled) {
                return null
            }
        }

        return null
    }

    override fun onPostExecute(result: Void?) {
        progressDialog?.dismiss()
    }
}