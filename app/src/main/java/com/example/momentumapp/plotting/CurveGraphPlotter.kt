package com.example.momentumapp.plotting

import com.example.momentumapp.util.amax
import com.example.momentumapp.util.amin
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.BaseSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

/**
 * Plots horizontal and vertical values representing a curve.
 */
class CurveGraphPlotter(
    graphView: GraphView,
    private val resizeHorizontally: Boolean,
    resizeVertically: Boolean,
) : GraphPlotter(graphView, resizeVertically) {
    override fun plot(data: GraphData, color: Int) {
        super.plot(data, color)

        val size = data.size
        val xData = data.getXData()
        val yData = data.getYData()

        val series: BaseSeries<DataPoint>
        series = LineGraphSeries(Array(size) { DataPoint(xData[it], yData[it]) })
        series.color = color
        series.thickness = 2

        if (resizeHorizontally) {
            minX = if (xData.amin() < minX) xData.amin() else minX
            maxX = if (xData.amax() > maxX) xData.amax() else maxX
        }

        graphView.addSeries(series)
    }
}