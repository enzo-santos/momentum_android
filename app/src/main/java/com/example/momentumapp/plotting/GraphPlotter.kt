package com.example.momentumapp.plotting

import com.example.momentumapp.util.amax
import com.example.momentumapp.util.amin
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.Viewport

/**
 * Provided functionality of a graph plotter.
 */
abstract class GraphPlotter(
    /**
     * The GraphView where some data will be plotted.
     */
    protected val graphView: GraphView,

    /**
     * If, at the insertion of new data, this graph should resize its vertical bounds.
     */
    private val resizeVertically: Boolean,

    /**
     * The minimum value of this plot.
     */
    var minimum: Double? = null,

    /**
     * The maximum value of this plot.
     */
    var maximum: Double? = null,
) : Plotter {
    /**
     * The viewport of this GraphView.
     */
    protected val viewport: Viewport = graphView.viewport

    /**
     * Manages the left bound of this graph.
     */
    var minX: Double
        get() = viewport.getMinX(false)
        set(value) {
            if (!viewport.isXAxisBoundsManual)
                viewport.isXAxisBoundsManual = true

            viewport.setMinX(value)
        }

    /**
     * Manages the right bound of this graph.
     */
    var maxX: Double
        get() = viewport.getMaxX(false)
        set(value) {
            if (!viewport.isXAxisBoundsManual)
                viewport.isXAxisBoundsManual = true

            viewport.setMaxX(value)
        }

    /**
     * Manages the bottom bound of this graph.
     */
    var minY: Double
        get() = viewport.getMinY(false)
        set(value) {
            if (!viewport.isYAxisBoundsManual)
                viewport.isYAxisBoundsManual = true

            viewport.setMinY(value)
        }

    /**
     * Manages the top bound of this graph.
     */
    var maxY: Double
        get() = viewport.getMaxY(false)
        set(value) {
            if (!viewport.isYAxisBoundsManual)
                viewport.isYAxisBoundsManual = true

            viewport.setMaxY(value)
        }

    /**
     * Resizes this graph vertically based on some [data], if necessary.
     */
    fun checkForVerticalUpdates(data: GraphData) {
        val yData = data.getYData()

        val minYValues = yData.amin()
        val minimum = minimum
        if (minimum == null || minYValues < minimum) {
            this.minimum = minYValues
            minY = minYValues - .5
        }

        val maxYValues = yData.amax()
        val maximum = maximum
        if (maximum == null || maxYValues > maximum) {
            this.maximum = maxYValues
            maxY = maxYValues + .5
        }
    }

    override fun plot(data: GraphData, color: Int) {
        if (resizeVertically)
            checkForVerticalUpdates(data)
    }
}

