package com.example.momentumapp.plotting

import com.example.momentumapp.layout.SensorGraphView
import com.example.momentumapp.util.Plotting
import com.jjoe64.graphview.GraphView

/**
 * Manages a single graph sensor plotter.
 *
 * In this context, it will be plotted the three-axis data in only one graph.
 */
class SingleGraphSensorPlotter(
    graph: GraphView,
) : GraphSensorPlotter(arrayOf(graph)) {
    companion object {
        /**
         * Creates a single graph sensor plotter from a list.
         */
        fun fromList(graphs: List<GraphView>): SingleGraphSensorPlotter =
            SingleGraphSensorPlotter(graphs[0])
    }

    constructor(sensorGraphView: SensorGraphView, init: ((SensorGraphView) -> Unit)? = null)
            : this(sensorGraphView.also(init ?: defaultInit).graph)

    override fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    ) {
        val plotter = if (!cache) plotting(graphs[0]) else cache(plotting, 0)

        if (xAxisData != null) plotter.plot(xAxisData, colors[0])
        if (yAxisData != null) plotter.plot(yAxisData, colors[1])
        if (zAxisData != null) plotter.plot(zAxisData, colors[2])
    }
}