package com.example.momentumapp.plotting

/**
 * Provided functionality of a plotter.
 */
interface Plotter {
    /**
     * Plots some [data] with the given [color].
     */
    fun plot(data: GraphData, color: Int)
}