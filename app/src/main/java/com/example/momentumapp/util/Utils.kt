package com.example.momentumapp.util

import android.Manifest
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.momentumapp.R
import com.example.momentumapp.plotting.GraphPlotter
import com.jjoe64.graphview.GraphView
import java.io.File
import java.io.FileWriter
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.concurrent.*
import kotlin.reflect.KClass

/**
 * Type definition representing the construction of a GraphPlotter given a GraphView.
 */
typealias Plotting = (GraphView) -> GraphPlotter

typealias AnalysisResult = Map<String, Map<String, Map<String, Double>>?>

typealias StringEncoding<T> = (T) -> String

typealias StringDecoding<T> = (String) -> T

/**
 * Represents a simple dismiss action for AlertDialogs.
 */
val dismissAction: (DialogInterface, Int) -> Unit = { dialog, _ -> dialog.dismiss() }

/**
 * Returns the value associated with some [key] in this map else some [lazyDefault] value.
 */
fun <K, V> Map<K, V>.getOrDefault(key: K, lazyDefault: () -> V): V = get(key) ?: lazyDefault()

/**
 * Returns the value associated with some [key] in this map else adds some [lazyDefault] value
 * to this map and returns it.
 */
fun <K, V> MutableMap<K, V>.getOrPutDefault(key: K, lazyDefault: () -> V): V =
    get(key) ?: lazyDefault().also { value -> put(key, value) }

/**
 * Returns this string in uppercase using the default locale.
 */
fun String.upper() = toUpperCase(Locale.getDefault())

/**
 * Checks if this string ends with any of the [suffixes] given.
 */
fun String.endsWithAny(vararg suffixes: String): Boolean =
    suffixes.filter { suffix -> endsWith(suffix) }.any()

/**
 * Parses this string as a Double using [sep] as decimal separator with [dec] decimal places.
 */
fun String.toDouble(sep: String, dec: Int): Double {
    require(sep in ",.") { "decimal separator must be a comma or a period" }
    return DecimalFormat(
        "#." + "#".repeat(dec),
        DecimalFormatSymbols(if (sep == ",") Locale.FRANCE else Locale.ENGLISH)
    ).parse(this)?.toDouble() ?: throw NumberFormatException(this)
}

/**
 * Returns the int value of this boolean.
 */
fun Boolean.toInt(): Int = if (this) 1 else 0

/**
 * Returns if all the elements of this boolean array are true.
 */
fun BooleanArray.all(): Boolean = all { it }

/**
 * Converts this value in milliseconds to minutes.
 */
fun Long.toMinutes(): Long {
    return TimeUnit.MILLISECONDS.toMinutes(this) % TimeUnit.HOURS.toMinutes(1)
}

/**
 * Converts this value in milliseconds to seconds.
 */
fun Long.toSeconds(): Long {
    return TimeUnit.MILLISECONDS.toSeconds(this) % TimeUnit.MINUTES.toSeconds(1)
}

/**
 * Returns the boolean value of this int in a C-style.
 */
fun Int.toBoolean(): Boolean = this != 0

/**
 * Transforms this int into a boolean array of given [size].
 *
 * The boolean array returned contains the last [size] bits of the base 2 representation of this
 * int, right to left.
 */
fun Int.toBooleanArray(size: Int): BooleanArray = BooleanArray(size) { i -> this[i] }

/**
 * Returns the bit at given [index] of the base 2 representation of this int, right to left.
 */
operator fun Int.get(index: Int): Boolean = ((this shr index) and 1).toBoolean()

/**
 * Returns this int with given [bit] set at given [index] of the base 2 representation of this int,
 * right to left.
 */
operator fun Int.set(index: Int, bit: Boolean): Int = if (bit) setOneAt(index) else setZeroAt(index)

/**
 * Returns this int with the bit 1 set at given [index] of the base 2 representation of this int,
 * right to left.
 */
fun Int.setOneAt(index: Int): Int = this or (1 shl index)

/**
 * Returns this int with the bit 0 set at given [index] of the base 2 representation of this int,
 * right to left.
 */
fun Int.setZeroAt(index: Int): Int = this and (1 shl index).inv()

/**
 * Returns the ARGB color associated with this int mixed with given [color] by the given [amount].
 *
 * The given [amount] should be between 0 (this color is unchanged) and 1 ([color] is returned).
 */
fun Int.mixWithColor(color: Int, amount: Float = 0.5f): Int {
    val alphaChannel = 0b11000
    val redChannel = 0b10000
    val greenChannel = 0b01000
    val blueChannel = 0b00000
    val inverseAmount = 1.0f - amount

    val a = ((this shr alphaChannel and 0xff).toFloat() * amount +
            (color shr alphaChannel and 0xff).toFloat() * inverseAmount).toInt() and 0xff

    val r = ((this shr redChannel and 0xff).toFloat() * amount +
            (color shr redChannel and 0xff).toFloat() * inverseAmount).toInt() and 0xff

    val g = ((this shr greenChannel and 0xff).toFloat() * amount +
            (color shr greenChannel and 0xff).toFloat() * inverseAmount).toInt() and 0xff

    val b = ((this and 0xff).toFloat() * amount +
            (color and 0xff).toFloat() * inverseAmount).toInt() and 0xff

    return a shl alphaChannel or (r shl redChannel) or (g shl greenChannel) or (b shl blueChannel)
}

/**
 * Converts this Double as a String, using [sep] as decimal separator with [dec] decimal places.
 */
fun Double.toString(sep: String, dec: Int): String {
    require(sep in ",.") { "decimal separator must be a comma or a period" }
    return "%.${dec}f".format(
        if (sep == ",") Locale.FRANCE else Locale.ENGLISH,
        this
    )
}

/**
 * Sorts this double array.
 */
fun DoubleArray.sorted(): DoubleArray = copyOf().also(DoubleArray::sort)

/**
 * Writes a log message to a file in the Android root directory.
 *
 * This should not be used in production and it's only used for debug purposes.
 */
fun <T> log(msg: T) {
    FileWriter("/storage/emulated/0/log.txt", true).append("$msg\n").close()
}

/**
 * Send a message through a Handler given a [messageSetup].
 */
fun Handler.sendMessage(messageSetup: (Message) -> Unit) = sendMessage(Message().also(messageSetup))

/**
 * Returns the corresponding activity of this View.
 */
val View.activity: Activity?
    get() {
        var context = context ?: null
        while (context is ContextWrapper) {
            if (context is Activity)
                return context

            context = context.baseContext
        }

        return null
    }

/**
 * Returns the save directory of given [filename] in the application record data.
 *
 * It can handle duplicated filenames (adds a number in the suffix of the file) and
 * redundant filename extension (removes any leading extension in the filename).
 */
fun Context.getSaveDirectory(filename: String): String {
    val fileExtension = fileExtension

    if (filename.endsWith(fileExtension))
        filename.dropLast(fileExtension.length)

    var fileCount = 0
    var strFileCounter = ""
    while (true) {
        val file = File(saveFolder, "$filename$strFileCounter.$fileExtension")
        if (!file.exists())
            return file.absolutePath

        fileCount += 1
        strFileCounter = " (%02d)".format(fileCount)
    }
}

/**
 * Returns if this application can write in a Android device.
 */
val Context.canWrite: Boolean
    get() = ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

/**
 * Requests the writing permission to be granted to this application using the given [requestCode].
 */
fun Activity.requestWritePermission(requestCode: Int) {
    ActivityCompat.requestPermissions(
        this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), requestCode
    )
}

/**
 * Returns the folder where this application can save data in a Android device.
 */
val Context.saveFolder: File
    get() = File(getExternalFilesDir(null), SAVE_FOLDER_NAME).also { folder ->
        if (!folder.exists()) {
            folder.mkdirs()
        }
    }

/**
 * Returns the shared preferences of this application.
 */
val Context.prefs: SharedPreferences
    get() = getSharedPreferences(APP_TITLE, Context.MODE_PRIVATE)

/**
 * Clears all the temporary files from the application save folder.
 */
fun Context.clearTemporaryFiles() {
    saveFolder.listFiles { file -> file.name.endsWith(".tmp") }?.forEach(File::delete)
}

/**
 * Converts a Density Independent Pixel (dp) value to a Pixel (px) value.
 */
fun Context.toPixels(dp: Int): Int =
    (dp * resources.displayMetrics.density + 0.5f).toInt()

/**
 * Inflates a view represented by its [viewId] in this Activity layout.
 */
fun Activity.inflate(viewId: Int): View =
    layoutInflater.inflate(viewId, null, false)

/**
 * Inflates a view represented by its [viewId] in this ViewGroup layout.
 */
fun ViewGroup.inflate(viewId: Int): View =
    LayoutInflater.from(context).inflate(viewId, this, false)

/**
 * Shows a toast with short duration.
 */
fun Context.toast(message: CharSequence): Unit =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

/**
 * Shows a toast with long duration.
 */
fun Context.toastLong(message: CharSequence): Unit =
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()

/**
 * Launches an activity [T] with no data.
 */
inline fun <reified T : Activity> Activity.launchActivity(): Unit =
    startActivity(Intent(this, T::class.java))

/**
 * Launches an activity [T] with some data generated by given [function].
 */
inline fun <reified T : Activity> Activity.launchActivity(function: (Intent) -> Unit) {
    val intent = Intent(this, T::class.java).also(function)
    startActivity(intent)
}

/**
 * Launches an dialog with setup provided by given [function].
 */
fun Context.launchDialog(function: (AlertDialog.Builder) -> Unit): AlertDialog {
    return AlertDialog.Builder(this, R.style.AppDialog)
        .also(function).create().also(AlertDialog::show)
}

/**
 * Launches an dialog with given [title] and setup provided by given [function].
 */
fun Context.launchDialog(title: String, function: (AlertDialog.Builder) -> Unit): AlertDialog {
    return launchDialog { builder ->
        builder.setTitle(title)
        function(builder)
    }
}

/**
 * Applies given [transform] function to each element of [this@transformAsync] in parallel using [n] threads.
 */
fun <I, O> List<I>.mapAsync(n: Int, transform: (v: I) -> O): List<O> {
    class TransformTask(val input: I) : Callable<O> {
        override fun call(): O = transform(input)
    }

    return try {
        (Executors.newFixedThreadPool(n) as ExecutorService)
            .invokeAll(map(::TransformTask)).map(Future<O>::get)

    } catch (e: InterruptedException) {
        listOf()
    }
}