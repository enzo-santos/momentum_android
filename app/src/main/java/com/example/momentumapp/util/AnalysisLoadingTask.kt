package com.example.momentumapp.util

import android.content.Context
import android.os.AsyncTask
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.AnalysisActivity
import com.example.momentumapp.R
import java.lang.ref.WeakReference

class AnalysisLoadingTask(activity: AnalysisActivity) : AsyncTask<String, Int, AnalysisResult>() {
    private val weakReference = WeakReference(activity)
    private lateinit var progressDialog: AlertDialog
    private lateinit var progressText: TextView
    private var actualProgressValue = 0
    private var maxProgressValue = 0.0

    val context: Context?
        get() = weakReference.get()?.applicationContext

    @Synchronized
    fun updateProgress(value: Int) = publishProgress(value)

    private fun generateTimeFeaturesMap(y: DoubleArray): Map<String, Double> {
        val featuresMap = mutableMapOf<String, Double>()
        val context = context ?: return featuresMap

        featuresMap[context.getString(R.string.max_value)] = y.amax()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.min_value)] = y.amin()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.range)] = y.range()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.standard_deviation)] = y.stdev()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.skew)] = y.skewness()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.kurtosis)] = y.kurtosis()
        if (isCancelled) return featuresMap
        updateProgress(1)

        return featuresMap.toMap()
    }

    private fun generateFrequencyFeaturesMap(
        xf: DoubleArray,
        yf: DoubleArray,
    ): Map<String, Double> {
        val featuresMap = mutableMapOf<String, Double>()
        val context = context ?: return featuresMap

        val (xPeak, yPeak) = amax(xf, yf)
        featuresMap[context.getString(R.string.peak_frequency)] = xPeak
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.peak_value)] = yPeak
        if (isCancelled) return featuresMap
        updateProgress(1)

        val (_, yMeanFreq) = meanFrequency(xf, yf)
        featuresMap[context.getString(R.string.mean_frequency)] = yMeanFreq
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.skew)] = yf.skewness()
        if (isCancelled) return featuresMap
        updateProgress(1)

        featuresMap[context.getString(R.string.kurtosis)] = yf.kurtosis()
        if (isCancelled) return featuresMap
        updateProgress(1)

        return featuresMap.toMap()
    }

    private fun generateTimeMap(reg: RecordData?) = reg?.let {
        val context = context ?: return null

        val featuresMapX = generateTimeFeaturesMap(it.xAxis)
        if (isCancelled) return null

        val featuresMapY = generateTimeFeaturesMap(it.yAxis)
        if (isCancelled) return null

        val featuresMapZ = generateTimeFeaturesMap(it.zAxis)
        if (isCancelled) return null

        mapOf(
            context.getString(R.string.x_axis) to featuresMapX,
            context.getString(R.string.y_axis) to featuresMapY,
            context.getString(R.string.z_axis) to featuresMapZ
        )
    }

    private fun generateFrequencyMap(reg: RecordData?) = reg?.let {
        val context = context ?: return null

        val featuresMapX = generateFrequencyFeaturesMap(it.duration, it.xAxis)
        if (isCancelled) return null

        val featuresMapY = generateFrequencyFeaturesMap(it.duration, it.yAxis)
        if (isCancelled) return null

        val featuresMapZ = generateFrequencyFeaturesMap(it.duration, it.zAxis)
        if (isCancelled) return null

        mapOf(
            context.getString(R.string.x_axis) to featuresMapX,
            context.getString(R.string.y_axis) to featuresMapY,
            context.getString(R.string.z_axis) to featuresMapZ
        )
    }

    private fun generateMetricMap(dir: String): AnalysisResult? {
        return RecordLoader.fromPath(dir).let { loader ->
            val context = context ?: return null

            loader.accelerationData?.also { maxProgressValue += 33 }
            loader.velocityData?.also { maxProgressValue += 33 }
            loader.displacementData?.also { maxProgressValue += 33 }
            loader.rotationData?.also { maxProgressValue += 33 }

            val fftLoader = loader.toRFFT()

            val (
                accelerationFeatures,
                velocityFeatures,
                displacementFeatures,
                rotationFeatures,
            ) = listOf(
                loader.accelerationData,
                loader.velocityData,
                loader.displacementData,
                loader.rotationData,
            ).mapAsync(2) { reg -> generateTimeMap(reg) }

            if (isCancelled) return null

            val (
                accelerationFFTFeatures,
                velocityFFTFeatures,
                displacementFFTFeatures,
                rotationFFTFeatures,
            ) = listOf(
                fftLoader.accelerationData,
                fftLoader.velocityData,
                fftLoader.displacementData,
                fftLoader.rotationData,
            ).mapAsync(2) { reg -> generateFrequencyMap(reg) }

            if (isCancelled) return null

            mapOf(
                context.getString(R.string.acceleration) to accelerationFeatures,
                context.getString(R.string.velocity) to velocityFeatures,
                context.getString(R.string.displacement) to displacementFeatures,
                context.getString(R.string.rotation) to rotationFeatures,
                "${context.getString(R.string.acceleration)} (${context.getString(R.string.fft)})" to accelerationFFTFeatures,
                "${context.getString(R.string.velocity)} (${context.getString(R.string.fft)})" to velocityFFTFeatures,
                "${context.getString(R.string.displacement)} (${context.getString(R.string.fft)})" to displacementFFTFeatures,
                "${context.getString(R.string.rotation)} (${context.getString(R.string.fft)})" to rotationFFTFeatures,
            )
        }
    }

    override fun onPreExecute() {
        weakReference.get()?.also { activity ->
            progressDialog = activity.launchDialog { builder ->
                val dialogLayout = activity.inflate(R.layout.dialog_progress)
                dialogLayout
                    .findViewById<TextView>(R.id.txt_dialog_progress_title)
                    .text = activity.getString(R.string.analysis_dialog_creating_text)

                progressText = dialogLayout.findViewById(R.id.txt_dialog_progress)
                progressText.text = "0%"

                builder.setView(dialogLayout)
                    .setCancelable(false)
                    .setNegativeButton(activity.getString(R.string.cancel)) { dialog, _ ->
                        dialog.dismiss()
                        cancel(true)
                        activity.finish()
                    }
                    .create()
            }

            progressDialog.window?.setBackgroundDrawableResource(R.color.colorToolbar)
        }
    }

    override fun onProgressUpdate(vararg values: Int?) {
        values[0]?.also { value ->
            actualProgressValue += value
            progressText.text = "${(actualProgressValue / maxProgressValue * 100).toInt()}%"
        }
    }

    override fun doInBackground(vararg params: String) = generateMetricMap(params[0])

    override fun onPostExecute(result: AnalysisResult) {
        weakReference.get()?.also { activity ->
            RecordCache.cache(activity.directory.split("/").last(), result)
            activity.setLayoutFromFeatures(result)
            progressDialog.dismiss()
        }
    }
}