package com.example.momentumapp.util

import android.os.AsyncTask
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.R
import com.example.momentumapp.RecordActivity
import java.lang.ref.WeakReference

class RecordLoadingTask(activity: RecordActivity) : AsyncTask<String, Void, Array<RecordData?>>() {
    private val weakReference = WeakReference(activity)
    private var loadingDialog: AlertDialog? = null

    override fun onPreExecute() {
        super.onPreExecute()
        loadingDialog = weakReference.get()?.let {
            val dialogLayout = it.inflate(R.layout.dialog_progress)
            dialogLayout
                .findViewById<TextView>(R.id.txt_dialog_progress_title)
                .text = it.getString(R.string.record_dialog_loading_text)

            AlertDialog.Builder(it, R.style.AppDialog).setView(dialogLayout)
                .setCancelable(false).create()
        }

        loadingDialog?.show()
    }

    override fun doInBackground(vararg params: String): Array<RecordData?> {
        val recordsData = arrayOfNulls<RecordData>(8)

        RecordLoader.fromPath(params[0]).also { loader ->
            recordsData[0] = loader.accelerationData
            recordsData[1] = loader.velocityData
            recordsData[2] = loader.displacementData
            recordsData[3] = loader.rotationData

            val fftLoader = loader.toRFFT()
            recordsData[4] = fftLoader.accelerationData
            recordsData[5] = fftLoader.velocityData
            recordsData[6] = fftLoader.displacementData
            recordsData[7] = fftLoader.rotationData
        }

        return recordsData
    }

    override fun onPostExecute(result: Array<RecordData?>) {
        super.onPostExecute(result)
        weakReference.get()?.also {
            it.recordingsArray = result
            it.isCompactAxis = false
        }

        loadingDialog?.dismiss()
    }
}