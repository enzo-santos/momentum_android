package com.example.momentumapp.plotting

import com.example.momentumapp.util.Plotting

/**
 * Manages inertial sensor plotters, such as accelerometer and gyroscope data.
 */
class InertialMultiSensorPlotter(
    mutableList: MutableList<GraphSensorPlotter>,
) : MultiSensorPlotter(mutableList) {
    override var plotterIndex = 0

    constructor() : this(mutableListOf())

    fun plotAccelerometer(
        xData: GraphData?,
        yData: GraphData?,
        zData: GraphData?,
        colors: IntArray,
        plotting: Plotting,
    ) = plotAccelerometer(xData, yData, zData, colors, false, plotting)

    fun plotAccelerometer(
        xData: GraphData?,
        yData: GraphData?,
        zData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    ) {
        setPlotter(0).plot(xData, yData, zData, colors, cache, plotting)
    }

    fun plotGyroscope(
        xData: GraphData?,
        yData: GraphData?,
        zData: GraphData?,
        colors: IntArray,
        plotting: Plotting,
    ) = plotGyroscope(xData, yData, zData, colors, false, plotting)

    fun plotGyroscope(
        xData: GraphData?,
        yData: GraphData?,
        zData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    ) {
        setPlotter(1).plot(xData, yData, zData, colors, cache, plotting)
    }
}