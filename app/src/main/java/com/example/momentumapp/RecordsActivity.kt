package com.example.momentumapp

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.example.momentumapp.layout.collapse
import com.example.momentumapp.layout.expand
import com.example.momentumapp.util.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class RecordsActivity : DispatchTouchActivity(), RecordContainer {
    private lateinit var edtSearchBar: EditText
    private lateinit var tlbRecords: Toolbar
    private lateinit var rlyActionBar: RelativeLayout

    private lateinit var adapter: RecordArrayAdapter

    private var isOrderByName = true
    private var isOrderAscending = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records)

        val fileHolders: MutableList<RecordArrayAdapter.Holder> = mutableListOf()
        saveFolder.listFiles { file -> file.name.endsWithAny(".csv", ".txt") }
            ?.map(RecordArrayAdapter.Holder::fromFile)
            ?.forEach(fileHolders::add)

        adapter = RecordArrayAdapter(this, fileHolders)

        findViewById<ListView>(R.id.lvw_records).also { lly ->
            lly.adapter = adapter
            orderFiles(isOrderByName, isOrderAscending, true)
        }

        rlyActionBar = findViewById(R.id.rly_records_bar)
        findViewById<ImageView>(R.id.img_records_bar_close).setOnClickListener {
            adapter.clearSelection()
        }

        findViewById<ImageView>(R.id.img_records_bar_delete).setOnClickListener {
            adapter.removeSelection()
        }

        tlbRecords = findViewById(R.id.tlb_records)
        setSupportActionBar(tlbRecords)

        val orderingType = prefs.getInt(KEY_ORDER_RECORDS_BY_TYPE, OPTION_ORDER_BY_DATE)
        isOrderByName = orderingType == OPTION_ORDER_BY_NAME

        val orderingMode = prefs.getInt(KEY_ORDER_RECORDS_MODE, OPTION_ORDER_DESCENDING)
        isOrderAscending = orderingMode == OPTION_ORDER_ASCENDING

        edtSearchBar = findViewById(R.id.edt_records)
        edtSearchBar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                adapter.filter.filter(s.toString().toLowerCase(Locale.getDefault()))
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int,
            ) {
            }
        })
    }

    private fun orderFiles(orderByName: Boolean, orderAscending: Boolean, firstTime: Boolean) {
        if ((orderByName != isOrderByName || orderAscending != isOrderAscending) || firstTime) {
            isOrderByName = orderByName
            isOrderAscending = orderAscending

            prefs.edit()
                .putInt(
                    KEY_ORDER_RECORDS_BY_TYPE,
                    if (orderByName) OPTION_ORDER_BY_NAME else OPTION_ORDER_BY_DATE
                )
                .putInt(
                    KEY_ORDER_RECORDS_MODE,
                    if (orderAscending) OPTION_ORDER_ASCENDING else OPTION_ORDER_DESCENDING
                )
                .apply()

            val c = if (!orderAscending) -1 else 1
            if (orderByName) {
                adapter.sort { o1, o2 ->
                    o1.filename.compareTo(o2.filename) * c
                }

            } else {
                adapter.sort { o1, o2 ->
                    val sdf = SimpleDateFormat("dd/MM/yyyy, HH:mm:ss", Locale.US)
                    val v1 = sdf.parse(o1.lastModified)!!.time
                    val v2 = sdf.parse(o2.lastModified)!!.time
                    v1.compareTo(v2) * c
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_records, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item1: MenuItem): Boolean {
        return when (item1.itemId) {
            R.id.item_records_orderby -> {
                val itemView = findViewById<View>(R.id.item_records_orderby)

                PopupMenu(this, itemView).also {
                    it.menuInflater.inflate(R.menu.menu_records_orderby, it.menu)
                    it.menu.getItem(if (isOrderByName) 0 else 1).isChecked = true

                    it.setOnMenuItemClickListener { menuItem ->
                        val isOrderByName = when (menuItem.itemId) {
                            R.id.item_records_orderby_name -> {
                                it.menu.getItem(0).isChecked = true
                                it.menu.getItem(1).isChecked = false
                                true
                            }

                            R.id.item_records_orderby_date -> {
                                it.menu.getItem(0).isChecked = false
                                it.menu.getItem(1).isChecked = true
                                false
                            }

                            else -> throw RuntimeException()
                        }

                        orderFiles(
                            orderByName = isOrderByName,
                            orderAscending = isOrderAscending,
                            firstTime = false
                        )

                        true
                    }

                    it.show()
                }

                true
            }

            R.id.item_records_orderhow -> {
                val itemView = findViewById<View>(R.id.item_records_orderhow)

                PopupMenu(this, itemView).also {
                    it.menuInflater.inflate(R.menu.menu_records_orderhow, it.menu)
                    it.menu.getItem(if (isOrderAscending) 0 else 1).isChecked = true

                    it.setOnMenuItemClickListener { menuItem ->
                        val isAscending = when (menuItem.itemId) {
                            R.id.item_records_orderhow_up -> {
                                it.menu.getItem(0).isChecked = true
                                it.menu.getItem(1).isChecked = false
                                true

                            }

                            R.id.item_records_orderhow_down -> {
                                it.menu.getItem(0).isChecked = false
                                it.menu.getItem(1).isChecked = true
                                false
                            }

                            else -> throw RuntimeException()
                        }

                        orderFiles(
                            orderByName = isOrderByName,
                            orderAscending = isAscending,
                            firstTime = false
                        )

                        true
                    }

                    it.show()
                }

                true
            }

            else -> super.onOptionsItemSelected(item1)
        }
    }

    override fun onSelectionModeChanged(mode: Int) {
        if (mode == 0) edtSearchBar.expand() else edtSearchBar.collapse()
        tlbRecords.visibility = if (mode == 0) View.VISIBLE else View.INVISIBLE
        rlyActionBar.visibility = if (mode == 0) View.GONE else View.VISIBLE
    }

    override fun onBackPressed() {
        if (adapter.hasSelection())
            adapter.clearSelection()
        else
            super.onBackPressed()
    }

    override fun onCardClicked(position: Int) {
        val fileHolder = adapter.getItem(position) ?: return

        if (File(fileHolder.directory).length() == 0L) {
            toastLong(getString(R.string.records_toast_empty_file))

        } else {
            launchActivity<RecordActivity> {
                it.putExtra(KEY_FILE_DIRECTORY, fileHolder.directory)
            }
        }
    }

    override fun onItemClicked(name: String, position: Int) {
        when (name) {
            "Excluir" -> {
                val holder = adapter.getItem(position) ?: return

                AlertDialog.Builder(this, R.style.AppDialog)
                    .setTitle(getString(R.string.records_dialog_remove_title))
                    .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                        if (!File(holder.directory).delete()) {
                            toast(getString(R.string.records_toast_remove_empty_file))
                        }

                        adapter.remove(holder)
                        adapter.notifyDataSetChanged()
                        dialog.dismiss()
                    }
                    .setMessage(
                        getString(
                            R.string.records_dialog_remove_message,
                            holder.filename
                        )
                    )
                    .setNegativeButton(getString(R.string.cancel), dismissAction)
                    .create().show()
            }
        }
    }
}