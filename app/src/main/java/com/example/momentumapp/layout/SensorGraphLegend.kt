package com.example.momentumapp.layout

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.StyleableRes
import com.example.momentumapp.R

class SensorGraphLegend : LinearLayout {
    @StyleableRes
    private val indexXAxisColor = 0

    @StyleableRes
    private val indexYAxisColor = 1

    @StyleableRes
    private val indexZAxisColor = 2

    @StyleableRes
    private val indexXAxisLabel = 3

    @StyleableRes
    private val indexYAxisLabel = 4

    @StyleableRes
    private val indexZAxisLabel = 5

    private val attrs: AttributeSet?

    private lateinit var txtXAxis: TextView
    private lateinit var txtYAxis: TextView
    private lateinit var txtZAxis: TextView

    constructor (c: Context) : super(c) {
        attrs = null
        initComponents()
    }

    constructor (c: Context, attrSet: AttributeSet) : super(c, attrSet) {
        attrs = attrSet
        initComponents()
    }

    constructor(c: Context, attrSet: AttributeSet, defStyleAttr: Int) : super(
        c,
        attrSet,
        defStyleAttr
    ) {
        attrs = attrSet
        initComponents()
    }

    private fun initComponents() {
        inflate(context, R.layout.view_sensor_graph_legend, this)

        attrs ?: return
        val styledAttrs = context.obtainStyledAttributes(
            attrs,
            intArrayOf(
                R.attr.xAxisColor,
                R.attr.yAxisColor,
                R.attr.zAxisColor,
                R.attr.xAxisLabel,
                R.attr.yAxisLabel,
                R.attr.zAxisLabel
            )
        )

        val xAxisColor = styledAttrs.getResourceId(indexXAxisColor, R.color.x_axis)
        val yAxisColor = styledAttrs.getResourceId(indexYAxisColor, R.color.y_axis)
        val zAxisColor = styledAttrs.getResourceId(indexZAxisColor, R.color.z_axis)
        val xAxisLabel = styledAttrs.getText(indexXAxisLabel) ?: ""
        val yAxisLabel = styledAttrs.getText(indexYAxisLabel) ?: ""
        val zAxisLabel = styledAttrs.getText(indexZAxisLabel) ?: ""

        styledAttrs.recycle()

        findViewById<ImageView>(R.id.img_legend_x_color).setImageResource(xAxisColor)
        findViewById<ImageView>(R.id.img_legend_y_color).setImageResource(yAxisColor)
        findViewById<ImageView>(R.id.img_legend_z_color).setImageResource(zAxisColor)

        txtXAxis = findViewById(R.id.txt_legend_x_axis)
        txtYAxis = findViewById(R.id.txt_legend_y_axis)
        txtZAxis = findViewById(R.id.txt_legend_z_axis)

        txtXAxis.text = if (xAxisLabel.isEmpty()) context.getString(R.string.x_axis) else xAxisLabel
        txtYAxis.text = if (yAxisLabel.isEmpty()) context.getString(R.string.y_axis) else yAxisLabel
        txtZAxis.text = if (zAxisLabel.isEmpty()) context.getString(R.string.z_axis) else zAxisLabel
    }

    var xAxisColor: Int
        get() = txtXAxis.currentTextColor
        set(value) = txtXAxis.setTextColor(value)

    var yAxisColor: Int
        get() = txtYAxis.currentTextColor
        set(value) = txtYAxis.setTextColor(value)

    var zAxisColor: Int
        get() = txtZAxis.currentTextColor
        set(value) = txtZAxis.setTextColor(value)
}