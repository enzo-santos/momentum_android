package com.example.momentumapp.plotting

import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlin.math.abs

class UnorderedCurveGraphPlotter(
    graphView: GraphView,
    private val resizeHorizontally: Boolean,
    resizeVertically: Boolean,
    minimum: Double? = null,
    maximum: Double? = null,
) : GraphPlotter(graphView, resizeVertically, minimum, maximum) {
    var onProgressUpdateListener: ((Int, Int) -> Unit)? = null

    override fun plot(data: GraphData, color: Int) {
        super.plot(data, color)

        val xData = data.getXData().asList()
        val yData = data.getYData().asList()

        if (resizeHorizontally) {
            val minX = xData.minOrNull()
            if (minX != null && minX < this.minX) {
                this.minX = minX - abs(minX) * .3
            }

            val maxX = xData.maxOrNull()
            if (maxX != null && maxX > this.maxX) {
                this.maxX = maxX + abs(maxX) * .3
            }
        }
        val dataSeries: MutableList<List<DataPoint>> = arrayListOf()
        var dataChunks: MutableList<DataPoint> = arrayListOf()

        val xPrevData = xData.subList(0, xData.size - 1)
        val xNextData = xData.subList(1, xData.size)
        (xPrevData zip xNextData).forEachIndexed { i, (xPrev, xNext) ->
            dataChunks.add(DataPoint(xPrev, yData[i]))

            if (xPrev > xNext) {
                dataSeries.add(listOf(DataPoint(xNext, yData[i + 1]), DataPoint(xPrev, yData[i])))
                if (dataChunks.isNotEmpty()) {
                    dataSeries.add(dataChunks)
                }

                dataChunks = arrayListOf()
            }
        }

        dataChunks.add(DataPoint(xData.last(), yData.last()))
        dataSeries.add(dataChunks)


        val numSeries = dataSeries.size
        dataSeries.forEachIndexed { i, dataPoints ->
            onProgressUpdateListener?.invoke(i + 1, numSeries)

            graphView.addSeries(LineGraphSeries(dataPoints.toTypedArray()).also { series ->
                series.thickness = 2
                series.color = color
            })
        }
    }
}