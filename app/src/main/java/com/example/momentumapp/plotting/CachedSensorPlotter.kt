package com.example.momentumapp.plotting

/**
 * Provided functionality for a three-axis sensor plotter, with concrete caching.
 */
interface CachedSensorPlotter<T> : CacheSensorPlotter {
    /**
     * The cached plotters.
     */
    val cachedPlotters: MutableMap<T, GraphPlotter>

    /**
     * Returns a [plotter] given its respective [axis].
     *
     * If it's cached, returns the cached plotter. Otherwise, caches it and returns.
     */
    fun cache(plotter: GraphPlotter, axis: Int): GraphPlotter
}