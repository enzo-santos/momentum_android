package com.example.momentumapp.util

/**
 * Represents actions to be done in a option container.
 */
interface OptionContainer {
    /**
     * Action to be done when a option is clicked in a adapter [position].
     */
    fun onOptionClicked(position: Int)
}