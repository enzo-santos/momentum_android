# Momentum

O Momentum é um aplicativo para Android que registra no dispositivo os dados
produzidos pelos sensores de smartphones atuais, como o acelerômetro e o 
giroscópio. Atualmente, as funcionalidades fornecidas pelo aplicativo são:

- Armazenamento dos dados dos sensores em um arquivo no dispositivo

- Visualização em tempo real através de gráficos dos dados dos sensores

- Possibilidade de pausar e retomar o registro dos dados

- Regulação do intervalo de gravação dos sensores através de valores fixos, 
como a cada 200, 100, 60 ou 20 milissegundos

- Possibilidade de escolher o tipo do arquivo de saída (**.csv** ou **.txt**)

- Controle do tamanho do arquivo de saída, ie, se o registro dos dados deverá 
ser interrompido após o arquivo de saída atingir determinado número de linhas

- Definição do modo pelos quais os valores dos sensores serão calculados, ie, 
se deverão ser registrados os valores brutos dos sensores ou a variação entre 
o valor atual e o valor anterior

- Visualização de registros anteriores por meio de gráficos

- Compartilhamento dos arquivos de saída para outros dispositivos e aplicativos

- Cálculo de algumas medidas estatísticas sobre registros anteriores, como 
média, moda, mediana, desvio padrão, assimetria e curtose

- Cálculo da transformada de Fourier sobre registros anteriores e de suas 
medidas estatísticas. A [transformada de Fourier](https://pt.wikipedia.org/wiki/Transformada_de_Fourier)
é um algoritmo utilizado para converter uma série de dados no domínio do tempo 
para uma série de dados no domínio da frequência 

- Receber e armazenar registros de sensores de outros dispositivos 
em tempo real via Bluetooth

Em relação aos sensores, são registrados três eixos: o eixo X (horizontal e 
aponta para a direita), o eixo Y (vertical e aponta para cima) e o eixo Z 
(frontal, aponta para o lado de fora da tela). Os eixos não são alterados 
quando a orientação da tela do dispositivo muda.

O arquivo de saída é um arquivo separado por vírgulas, onde os valores dos 
sensores são registrados com duas casas decimais. A primeira linha do arquivo 
é o cabeçalho. A primeira coluna é a duração do registro, em milissegundos, 
que representa quanto tempo se passou após o começo do registro.

As colunas seguintes são os valores recebidos dos sensores, classificadas por 
eixos. Caso algum sensor não esteja presente no dispositivo, seu registro não 
será feito e no arquivo de saída não haverá uma coluna correspondente ao mesmo.

### Instalação

Para instalar, verifique as [releases do projeto](https://gitlab.com/enzo-santos/momentum/-/releases).

Na mais atual, ie. a que estiver mais ao topo da página, verifique em seu
conteúdo a seção **Download**. Clique em *momentum.apk* e o download será
efetuado automaticamente.

### Modo Bluetooth

O modo Bluetooth permite enviar ou receber registros em tempo real de outro 
disposivo por meio do Bluetooth. Para que esse recurso funcione corretamente, 
é preciso que ambos os dispositivos estejam pareados.

Para parear, ative o Bluetooth nos dois dispositivos. Para isso, vá em 
**Configurações > Conexões > *Bluetooth*** (pode variar de modelo para modelo) e 
ative a conectividade. Em um dos dispositivos, aguarde alguns segundos até que 
o outro dispositivo seja encontrado, e então, toque nele. Aparecerá uma mensagem 
com um código numérico nos dois dispositivos. Pressione o botão "Parear" e faça
o mesmo no outro celular.

Após o pareamento, abra o Momentum, clique nos três pontinhos no canto superior 
direito do menu principal e clique em **Modo Bluetooth**. No dispositivo que se 
deseja receber o registro, clique em **Dispositivo destinatário** e aguarde. No 
dispositivo que se deseja enviar o registro, clique em **Dispositivo remetente** 
e selecione, na lista de dispositivos pareados que aparecerá, o nome do outro 
dispositivo. Aguarde que a conexão seja feita até que a caixa de diálogo feche em
ambos os aparelhos.

> É importante que o dispositivo destinatário inicie a espera da conexão primeiro 
> do que o dispositivo remetente.

Em seguida, no dispositivo destinatário, clique em **Iniciar** no canto inferior 
da tela para que a gravação seja iniciada. Ao final da gravação, no dispositivo 
destinatário, irá aparecer um arquivo chamado *GravacaoBluetooth* contendo os 
dados recebidos do dispositivo remetente.

## Screenshots

<img src="https://i.imgur.com/tobWHSp.jpg" alt="Menu principal" width="300"/>

<img src="https://i.imgur.com/XRQR4G0.jpg" alt="Menu de configurações" width="300"/>

<img src="https://i.imgur.com/AtAnYCM.jpg" alt="Menu de gravação" width="300"/>

<img src="https://i.imgur.com/JlteHdX.jpg" alt="Menu de registro anteriores" width="300"/>

<img src="https://i.imgur.com/gt6Szyh.jpg" alt="Menu de registro único (1)" width="300"/>

<img src="https://i.imgur.com/DFHr9A2.jpg" alt="Menu de registro único (2)" width="300"/>

<img src="https://i.imgur.com/wXm7Dr1.jpg" alt="Menu de análise" width="300"/>

<img src="https://i.imgur.com/XQsSkNJ.jpg" alt="Recurso de compartilhamento" width="300"/>