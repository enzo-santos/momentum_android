package com.example.momentumapp.modules.fml

import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.MainRecorderActivity
import com.example.momentumapp.DispatchTouchActivity
import com.example.momentumapp.MainActivity
import com.example.momentumapp.R
import com.example.momentumapp.util.*
import java.io.File

class MainActivity : DispatchTouchActivity() {
    private var filename: String? = null
    private var isTemporaryFile = false
    private lateinit var buttonSelectFile: LinearLayout

    private fun showDialog() {
        val dialogLayout = inflate(R.layout.dialog_main_filename)
        val edtFilename = dialogLayout.findViewById<EditText>(R.id.edtDialog)
        edtFilename.hint = getString(R.string.filename)
        val checkBoxTempRec = dialogLayout.findViewById<CheckBox>(R.id.cbRegistroTemp)
        checkBoxTempRec.setOnCheckedChangeListener { _, isChecked ->
            edtFilename.isEnabled = !isChecked
        }

        launchDialog(getString(R.string.main_dialog_filename_title)) { builder ->
            builder.setView(dialogLayout)
                .setPositiveButton(getString(R.string.start)) { dialog, _ ->
                    val isTempRecording = checkBoxTempRec.isChecked
                    isTemporaryFile = isTempRecording

                    val tempFile = if (isTempRecording)
                        File.createTempFile("fml", null, saveFolder)
                    else
                        null

                    val filename =
                        if (isTempRecording) tempFile!!.name else edtFilename.text.toString()
                    val directory =
                        if (isTempRecording) tempFile!!.absolutePath else getSaveDirectory(filename)

                    if (!isTempRecording && filename != null && filename.isEmpty()) {
                        toast(getString(R.string.main_toast_invalid_filename))
                    } else {
                        dialog.dismiss()

                        if (canWrite) {
                            this.filename =
                                if (isTempRecording) filename else "$filename.${fileExtension}"
                            buttonSelectFile.visibility = View.GONE
                            launchActivity<MainRecorderActivity> { intent ->
                                intent.putExtra(KEY_FILE_DIRECTORY, directory)
                                intent.putExtra(KEY_IS_TEMPORARY_FILE, isTempRecording)
                                intent.putExtra(KEY_IS_PARENT_ACTIVITY, true)
                            }

                        } else {
                            toastLong(getString(R.string.main_toast_cant_start))
                        }
                    }
                }
                .setNegativeButton(getString(R.string.cancel), dismissAction)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fml_main)
        setSupportActionBar(findViewById(R.id.tlb_fml_main))
        title = "Módulo ${getString(R.string.fml)}" 

        if ((getSystemService(SENSOR_SERVICE) as SensorManager)
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null
        ) {
            toast("Seu celular não suporta o sensor de acelerômetro.")
            launchActivity<MainActivity>()
            return
        }

        AlertDialog.Builder(this, R.style.AppDialog)
            .setTitle(getString(R.string.fml_main_dialog_record_action_title))
            .setItems(
                arrayOf(
                    getString(R.string.fml_main_dialog_record_action_now),
                    getString(R.string.fml_main_dialog_record_previous),
                )
            ) { dialog, which ->
                if (which == 1) {
                    dialog.dismiss()
                } else {
                    showDialog()
                }
            }
            .setCancelable(false)
            .create()
            .show()

        val includeAxis = booleanArrayOf(false, false, false)
        val checkboxIncludeX = findViewById<CheckBox>(R.id.cbx_fml_main_include_x)
        val checkboxIncludeY = findViewById<CheckBox>(R.id.cbx_fml_main_include_y)
        val checkboxIncludeZ = findViewById<CheckBox>(R.id.cbx_fml_main_include_z)
        checkboxIncludeX.setOnCheckedChangeListener { _, isChecked -> includeAxis[0] = isChecked }
        checkboxIncludeY.setOnCheckedChangeListener { _, isChecked -> includeAxis[1] = isChecked }
        checkboxIncludeZ.setOnCheckedChangeListener { _, isChecked -> includeAxis[2] = isChecked }

        val layoutIncludeX = findViewById<LinearLayout>(R.id.lly_fml_main_include_x)
        val layoutIncludeY = findViewById<LinearLayout>(R.id.lly_fml_main_include_y)
        val layoutIncludeZ = findViewById<LinearLayout>(R.id.lly_fml_main_include_z)
        layoutIncludeX.setOnClickListener {
            checkboxIncludeX.isChecked = !checkboxIncludeX.isChecked
        }
        layoutIncludeY.setOnClickListener {
            checkboxIncludeY.isChecked = !checkboxIncludeY.isChecked
        }
        layoutIncludeZ.setOnClickListener {
            checkboxIncludeZ.isChecked = !checkboxIncludeZ.isChecked
        }

        val textSelectedFile = findViewById<TextView>(R.id.txt_fml_main_selected_record)
        buttonSelectFile = findViewById(R.id.lly_fml_main_choose_file)
        buttonSelectFile.setOnClickListener listener@{
            val filenames = saveFolder.listFiles()
                ?.map(File::getName)
                ?.toMutableList()
                ?.also { l -> l.add(0, getString(R.string.fml_main_dialog_records_item_none)) }
                ?.toTypedArray()

            if (filenames == null) {
                toast(getString(R.string.fml_main_toast_no_records))
                return@listener
            }

            var selectedItem = filenames.indexOf(filename)
            AlertDialog.Builder(this, R.style.AppDialog)
                .setTitle(getString(R.string.fml_main_dialog_records_title))
                .setSingleChoiceItems(filenames, selectedItem) { _, which ->
                    selectedItem = which
                }
                .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                    if (selectedItem == -1 || selectedItem == 0) {
                        filename = null
                        textSelectedFile.text =
                            getString(R.string.fml_main_dialog_records_subtitle_none)

                    } else {
                        filename = filenames[selectedItem]
                        textSelectedFile.text = filename
                    }

                    dialog.dismiss()
                }
                .setNegativeButton(getString(R.string.cancel), dismissAction)
                .create()
                .show()
        }

        var warner = getString(R.string.vibration)
        val textSelectedWarner = findViewById<TextView>(R.id.txt_fml_main_warner_type)
        val buttonSelectWarner = findViewById<LinearLayout>(R.id.lly_fml_main_choose_warner)
        buttonSelectWarner.setOnClickListener {
            val warnerNames = arrayOf(getString(R.string.vibration), getString(R.string.sound))
            var selectedItem = warnerNames.indexOf(warner)

            AlertDialog.Builder(this, R.style.AppDialog)
                .setTitle(getString(R.string.fml_main_dialog_warner_title))
                .setSingleChoiceItems(warnerNames, selectedItem) { _, which ->
                    selectedItem = which
                }
                .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                    warner = warnerNames[selectedItem]
                    textSelectedWarner.text = warner

                    dialog.dismiss()
                }
                .setNegativeButton(getString(R.string.cancel), dismissAction)
                .create()
                .show()
        }

        val buttonStart = findViewById<Button>(R.id.btn_fml_main_start)
        buttonStart.setOnClickListener listener@{
            if (filename == null) {
                toast(getString(R.string.fml_main_toast_choose_record))
                return@listener
            }

            if (!includeAxis.any { it }) {
                toast(getString(R.string.fml_main_toast_choose_axis))
                return@listener
            }

            val margin = findViewById<EditText>(R.id.edt_fml_main_choose_error)
                .text.toString().toDoubleOrNull() ?: 0.0

            val numberIterations = findViewById<EditText>(R.id.edt_fml_main_choose_iterations)
                .text.toString().toIntOrNull() ?: 1

            launchActivity<ComparatorRecorderActivity> { intent ->
                intent
                    .putExtra(KEY_FILE_DIRECTORY, File(saveFolder, filename ?: "").absolutePath)
                    .putExtra(KEY_IS_TEMPORARY_FILE, isTemporaryFile)
                    .putExtra(KEY_ERROR_MARGIN, margin)
                    .putExtra(KEY_ACTUAL_ITERATION, 1)
                    .putExtra(KEY_NUMBER_ITERATIONS, numberIterations)
                    .putExtra(KEY_AXES_INCLUDED, includeAxis)
                    .putExtra(
                        KEY_WARNER_TYPE, when (warner) {
                            getString(R.string.vibration) -> WARNER_TYPE_VIBRATOR
                            getString(R.string.sound) -> WARNER_TYPE_BUZZER
                            else -> WARNER_TYPE_VIBRATOR
                        }
                    )
            }

            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_fml_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_fml_main_help -> {
                AlertDialog.Builder(this, R.style.AppDialog)
                    .setTitle("Instruções")
                    .setMessage(getString(R.string.fml_main_dialog_help_message))
                    .setPositiveButton(getString(R.string.ok), dismissAction)
                    .create()
                    .show()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
