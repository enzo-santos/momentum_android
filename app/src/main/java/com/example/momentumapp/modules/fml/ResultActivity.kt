package com.example.momentumapp.modules.fml

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.momentumapp.R
import com.example.momentumapp.layout.Table
import com.example.momentumapp.util.*
import java.util.*

class ResultActivity : AppCompatActivity() {

    private fun getTimeString(millis: Long): String {
        val min = millis.toMinutes()
        val sec = millis.toSeconds()

        if (min == 0L)
            return "%02d ${getString(R.string.second_unit)}".format(sec)

        return "%02d ${getString(R.string.minute_unit)} %02d ${getString(R.string.second_unit)}".format(
            min,
            sec)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fml_result)
        title = getString(R.string.results)

        val bundle = intent?.extras ?: return
        val numPoints = bundle.getIntArray(KEY_TOTAL_HITS)?.let { hits ->
            if (!hits.all { it == hits[0] }) error("assertion failed")
            hits[0]
        } ?: 0
        val misses = bundle.getIntArray(KEY_TOTAL_MISSES) ?: intArrayOf()
        val total = IntArray(misses.size) { i -> misses[i] + numPoints }

        val recordingDelay = bundle.getInt(KEY_RECORDING_DELAY)
        val numIterations = total.size

        val xErrors = bundle.getDoubleArray(KEY_X_MEAN_ERROR)
        val yErrors = bundle.getDoubleArray(KEY_Y_MEAN_ERROR)
        val zErrors = bundle.getDoubleArray(KEY_Z_MEAN_ERROR)

        val table = findViewById<Table>(R.id.tbl_fml_result)

        if (total.size > 1) {
            total.forEachIndexed { i, score ->
                val content = linkedMapOf<String, String>()
                content[getString(R.string.hits)] =
                    "%.2f%%".format(Locale.US, numPoints * 100.0 / score)

                val totalTime = numPoints * recordingDelay
                val userTime = score * recordingDelay

                val prefix = getString(R.string.imprecision)

                if (xErrors != null)
                    content["$prefix (${getString(R.string.x_axis)})"] =
                        "%.2f%%".format(Locale.US, xErrors[i] * 100)

                if (yErrors != null)
                    content["$prefix (${getString(R.string.y_axis)})"] =
                        "%.2f%%".format(Locale.US, yErrors[i] * 100)

                if (zErrors != null)
                    content["$prefix (${getString(R.string.z_axis)})"] =
                        "%.2f%%".format(Locale.US, zErrors[i] * 100)

                content[getString(R.string.record_time)] = getTimeString(totalTime.toLong())
                content[getString(R.string.your_time)] = getTimeString(userTime.toLong())
                table.addContent("${getString(R.string.iteration).upper()} ${i + 1}", content)
            }
        }

        val totalTime = numPoints * numIterations * recordingDelay
        val userTime = total.sum() * recordingDelay
        table.addContent(
            getString(R.string.total).upper(), linkedMapOf(
                getString(R.string.hits) to "%.2f%%".format(
                    Locale.US,
                    numPoints * numIterations * 100.0 / total.sum()
                ),
                getString(R.string.expected_time) to getTimeString(totalTime.toLong()),
                getString(R.string.your_time) to getTimeString(userTime.toLong())
            )
        )
    }
}
