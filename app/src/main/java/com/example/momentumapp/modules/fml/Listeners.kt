package com.example.momentumapp.modules.fml

/**
 * Represents a warner that will act in some [duration], in seconds.
 */
abstract class Warner(protected val duration: Int) {
    /**
     * Warns something to the user based on some [value].
     */
    abstract fun warn(value: Double)
}

/**
 * Represents a range from [minimum] to [maximum].
 *
 * [minimum] can be greater than [maximum].
 */
data class Range(
    /**
     * The minimum value of this range.
     */
    val minimum: Double,

    /**
     * The maximum value of this range.
     */
    val maximum: Double,
) {
    /**
     * The length of this range.
     */
    val length = maximum - minimum

    /**
     * Checks if this range contains some [value].
     */
    operator fun contains(value: Double): Boolean = value in minimum..maximum

    /**
     * Returns this range with a new [minimum].
     */
    fun withMinimum(minimum: Double): Range = Range(minimum, maximum)

    /**
     * Returns this range with a new [maximum].
     */
    fun withMaximum(maximum: Double): Range = Range(minimum, maximum)

    /**
     * Scales a [value] from this range to [other] range.
     */
    fun scale(value: Double, other: Range): Double {
        return (((value - this.minimum) * other.length) / this.length) + other.minimum
    }

    /**
     * Fits a [value] in this range.
     */
    fun fit(value: Double): Double {
        return if (value < minimum) minimum else if (value > maximum) maximum else value
    }
}

/**
 * Represents a scaler that will act in some [range].
 */
abstract class Scaler(protected val range: Range)

/**
 * Represents a [Scaler] based in the values of a sensor.
 */
class SensorScaler(range: Range) : Scaler(range) {
    /**
     * Calculates how each value in [values] are out of its respective range in [ranges].
     *
     * - If all [values] are in its respective [ranges], the returned value is the
     * [Range.minimum] value of [SensorScaler.range].
     * - If all [values] are out of its respective [ranges], where each value is lesser than its
     * respective range's [Range.minimum] or is greater than its respective range's [Range.maximum],
     * the returned value is the [Range.maximum] of [SensorScaler.range].
     */
    fun scale(values: List<Double>, ranges: List<Range?>): Double {
        val numSensors = ranges.count { range -> range != null }

        val scaledValues: Array<Double?> = Array(ranges.size) init@{ i ->
            val value = values[i]
            val oldRange = ranges[i] ?: return@init null
            val newRange = Range(0.0, range.length / numSensors)
            if (oldRange.length == 0.0) return@init newRange.minimum

            if (value < oldRange.length) {
                Range(oldRange.minimum, oldRange.minimum - oldRange.length).scale(value, newRange)
            } else {
                Range(oldRange.maximum, oldRange.maximum + oldRange.length).scale(value, newRange)
            }
        }

        return scaledValues.filterNotNull()
            .map { value -> range.fit(value + range.minimum) }.sum()
    }
}

abstract class LeveledWarner(duration: Int, range: Range) : Warner(duration) {
    private val scaler = SensorScaler(range)
    fun warn(values: List<Double>, ranges: List<Range?>) {
        val magnitudeScale = scaler.scale(values, ranges)
        warn(magnitudeScale)
    }
}

abstract class NeutralWarner(duration: Int) : Warner(duration) {
    override fun warn(value: Double) = warn()
    abstract fun warn()
}

interface FollowerNotifier {
    val warner: Warner?
    fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>)
}

/**
 * Represents a listener that keeps track of the user motion.
 */
interface UserMotionListener {
    fun onUserInRange(values: Array<Double>)
    fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>)
}

interface FollowerListener : FollowerNotifier, UserMotionListener {
    var followerNotifier: FollowerNotifier?
    fun onUserCancelled()

    override val warner: Warner?
        get() = followerNotifier?.warner

    override fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>) {
        followerNotifier?.onUserOutOfRange(values, ranges)
    }
}

interface LeveledFollowerNotifier : FollowerNotifier {
    override val warner: LeveledWarner?
    override fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>) {
        warner?.warn(values, ranges)
    }
}

interface NeutralFollowerNotifier : FollowerNotifier {
    override val warner: NeutralWarner?
    override fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>) {
        warner?.warn()
    }
}