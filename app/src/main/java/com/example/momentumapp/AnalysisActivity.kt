package com.example.momentumapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.momentumapp.layout.Table
import com.example.momentumapp.util.*
import java.util.*
import kotlin.collections.LinkedHashMap

class AnalysisActivity : AppCompatActivity() {
    private lateinit var table: Table
    internal var directory = ""

    internal fun setLayoutFromFeatures(featuresMap: AnalysisResult) {
        val axes = arrayOf(
            getString(R.string.x_axis),
            getString(R.string.y_axis),
            getString(R.string.z_axis),
        )

        val simpleMetrics = arrayOf(
            getString(R.string.acceleration),
            getString(R.string.rotation),
        )

        val sensors = arrayOf(
            getString(R.string.accelerometer),
            getString(R.string.gyroscope),
        )

        (simpleMetrics zip sensors).forEach { (simpleMetric, sensor) ->
            val sensorMap = featuresMap.filterKeys { metric -> simpleMetric in metric }
            if (sensorMap.isEmpty()) return@forEach
            if (sensorMap.all { (_, value) -> value == null }) return@forEach

            axes.forEach { axis ->
                table.addHeader(axis, sensor)

                sensorMap.forEach inner@{ (metric, axesMap) ->
                    val axisMap = axesMap?.get(axis)?.mapValues { (_, value) ->
                        String.format(Locale.US, "%.5f", value)
                    } ?: return@inner

                    table.addContent(metric.upper(), LinkedHashMap(axisMap))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_analysis)
        setSupportActionBar(findViewById(R.id.tlb_analysis))

        table = findViewById(R.id.tbl_analysis)
        directory = intent.getStringExtra(KEY_FILE_DIRECTORY) ?: ""
        val filename = directory.split("/").last()
        title = getString(R.string.analysis_toolbar_title, filename)

        if (RecordCache.isEmpty || !RecordCache.isCached(filename)) {
            AnalysisLoadingTask(this).execute(directory)

        } else {
            setLayoutFromFeatures(RecordCache.loadCache())
        }
    }
}