package com.example.momentumapp.util

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.momentumapp.R
import com.example.momentumapp.option.*

/**
 * Represents the adapter of a option displayed in the [com.example.momentumapp.OptionsActivity].
 */
class OptionAdapter(
    private val options: MutableList<BaseOptionType>,
    private val listener: OptionContainer,
) : RecyclerView.Adapter<BaseOptionType.BaseViewHolder>() {
    /**
     * Types of each option is this adapter.
     */
    private val types = options.map(BaseOptionType::type)

    override fun getItemCount(): Int = types.size
    override fun getItemViewType(position: Int): Int = types[position]

    override fun onBindViewHolder(holder: BaseOptionType.BaseViewHolder, pos: Int) {
        options[pos].onBindViewHolder(holder)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): BaseOptionType.BaseViewHolder = when (viewType) {
        OPTION_TYPE_RADIO_BUTTON ->
            OptionRadioButtonType.ViewHolder(
                view = parent.inflate(R.layout.item_options_selectable),
                container = listener
            )

        OPTION_TYPE_CHECKBOX ->
            OptionCheckBoxType.ViewHolder(
                view = parent.inflate(R.layout.item_options_selectable),
                container = listener
            )

        OPTION_TYPE_SWITCH ->
            OptionSwitchType.ViewHolder(
                view = parent.inflate(R.layout.item_options_editable),
                adapter = this,
                options = options
            )

        OPTION_TYPE_MUTABLE_SWITCH ->
            OptionMutableSwitchType.ViewHolder(
                view = parent.inflate(R.layout.item_options_editable),
                container = listener,
                adapter = this,
                options = options
            )

        OPTION_TYPE_MUTABLE ->
            OptionMutableType.ViewHolder(
                view = parent.inflate(R.layout.item_options_selectable),
                container = listener
            )

        else -> throw IllegalArgumentException()
    }
}
