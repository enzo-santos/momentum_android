package com.example.momentumapp.plotting

import com.example.momentumapp.layout.SensorGraphView
import com.example.momentumapp.util.Plotting
import com.jjoe64.graphview.GraphView

/**
 * Manages a triple graph sensor plotter.
 *
 * In this context, it will be plotted the each of three-axis data in three different graphs.
 */
class TripleGraphSensorPlotter(
    xGraphView: GraphView,
    yGraphView: GraphView,
    zGraphView: GraphView,
    /**
     * If the vertical bounds of the graphs must be aligned.
     * Otherwise, the graphs will not be in the same scale.
     */
    private val alignGraphs: Boolean,
) : GraphSensorPlotter(arrayOf(xGraphView, yGraphView, zGraphView)) {
    companion object {
        /**
         * Creates a triple graph sensor plotter from a list.
         */
        fun fromList(
            graphs: List<GraphView>,
            alignGraphs: Boolean,
        ) = TripleGraphSensorPlotter(graphs[0], graphs[1], graphs[2], alignGraphs)

        fun fromList(
            graphs: List<SensorGraphView>,
            alignGraphs: Boolean,
            init: ((SensorGraphView) -> Unit)? = null,
        ) = TripleGraphSensorPlotter(
            graphs[0].also(init ?: defaultInit).graph,
            graphs[1].also(init ?: defaultInit).graph,
            graphs[2].also(init ?: defaultInit).graph,
            alignGraphs
        )
    }

    constructor(
        xGraphView: SensorGraphView,
        yGraphView: SensorGraphView,
        zGraphView: SensorGraphView,
        alignGraphs: Boolean,
        init: ((SensorGraphView) -> Unit)? = null,
    ) : this(
        xGraphView.also(init ?: defaultInit).graph,
        yGraphView.also(init ?: defaultInit).graph,
        zGraphView.also(init ?: defaultInit).graph,
        alignGraphs
    )

    override fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    ) {
        val xPlotter = if (!cache) plotting(graphs[0]) else cache(plotting, 0)
        val yPlotter = if (!cache) plotting(graphs[1]) else cache(plotting, 1)
        val zPlotter = if (!cache) plotting(graphs[2]) else cache(plotting, 2)

        if (xAxisData != null) xPlotter.plot(xAxisData, colors[0])
        if (yAxisData != null) yPlotter.plot(yAxisData, colors[1])
        if (zAxisData != null) zPlotter.plot(zAxisData, colors[2])

        if (alignGraphs) {
            arrayOf(xPlotter, yPlotter, zPlotter).forEach { plotter ->
                arrayOf(xAxisData, yAxisData, zAxisData).filterNotNull()
                    .forEach(plotter::checkForVerticalUpdates)
            }
        }
    }
}