package com.example.momentumapp.layout

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.annotation.StyleableRes
import com.example.momentumapp.R

class SensorGraphViewGroup : LinearLayout {
    @StyleableRes
    private var indexSensorLabel = 0

    @StyleableRes
    private var indexHorizontalLabel = 1

    @StyleableRes
    private var indexVerticalLabel = 2

    @StyleableRes
    private var indexIsHideGraphInfo = 3

    private val attrs: AttributeSet?
    lateinit var sensorGraphViews: Array<SensorGraphView>

    fun applyToGraphs(transform: (graph: SensorGraphView) -> Unit) {
        sensorGraphViews.forEach(transform)
    }

    var sensorName: String
        get() = sensorGraphViews[0].sensorName
        set(value) = applyToGraphs {
            it.sensorName = value
        }

    var horizontalAxisLabel: String
        get() = sensorGraphViews[0].horizontalAxisLabel
        set(value) = applyToGraphs {
            it.horizontalAxisLabel = value
        }


    var verticalAxisLabel: String
        get() = sensorGraphViews[0].verticalAxisLabel
        set(value) = applyToGraphs {
            it.verticalAxisLabel = value
        }

    private var isHideGraphInfo: Boolean
        get() = sensorGraphViews[0].isHideGraphInfo
        set(value) = applyToGraphs {
            it.isHideGraphInfo = value
        }

    constructor (context: Context) : super(context) {
        attrs = null
        initComponents()
    }

    constructor (context: Context, attrSet: AttributeSet) : super(context, attrSet) {
        attrs = attrSet
        initComponents()
    }

    constructor(c: Context, attrSet: AttributeSet, defStyleAttr: Int) : super(
        c,
        attrSet,
        defStyleAttr
    ) {
        attrs = attrSet
        initComponents()
    }

    private fun initComponents() {
        inflate(context, R.layout.view_sensor_graph_view_group, this)

        sensorGraphViews = arrayOf(
            findViewById(R.id.sgv_x_axis),
            findViewById(R.id.sgv_y_axis),
            findViewById(R.id.sgv_z_axis),
        )

        attrs ?: return

        val styledAttrs = context.obtainStyledAttributes(
            attrs,
            intArrayOf(
                R.attr.sensors,
                R.attr.xLabels,
                R.attr.yLabels,
                R.attr.isHideAllInfo
            )
        )

        val attrSensorName = styledAttrs.getText(indexSensorLabel)?.toString() ?: ""
        val attrHorizontalLabel = styledAttrs.getText(indexHorizontalLabel)?.toString() ?: ""
        val attrVerticalLabel = styledAttrs.getText(indexVerticalLabel)?.toString() ?: ""
        val attrHideInfo = styledAttrs.getBoolean(indexIsHideGraphInfo, false)

        styledAttrs.recycle()

        sensorName = attrSensorName
        horizontalAxisLabel = attrHorizontalLabel
        verticalAxisLabel = attrVerticalLabel
        isHideGraphInfo = attrHideInfo
    }
}