package com.example.momentumapp.modules.chair

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.MainActivity
import com.example.momentumapp.R
import com.example.momentumapp.RecordActivity
import com.example.momentumapp.RecorderActivity
import com.example.momentumapp.plotting.InertialMultiSensorPlotter
import com.example.momentumapp.util.*
import java.io.File
import java.io.FileWriter
import java.io.StringWriter

class MainActivity : RecorderActivity() {
    private val writer = StringWriter()
    private lateinit var countDownTimer: CountDownTimer
    private var isRecordCompleted = false

    override val isGyroscopeGraph: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chair_main)
        setSupportActionBar(findViewById(R.id.tlb_chair_main))
        title = "Módulo Sentar e Levantar"

        val layoutGraphs = findViewById<LinearLayout>(R.id.lly_chair_main_graphs)
        layoutGraphs.inflateGraphs()

        val duration = 30_000L
        val txtTimer = findViewById<TextView>(R.id.txt_chair_main_timer)
        txtTimer.text = "%02d:%02d".format(duration.toMinutes(), duration.toSeconds())
        countDownTimer = object : CountDownTimer(duration, 1_000L) {
            override fun onTick(millisUntilFinished: Long) {
                txtTimer.text = "%02d:%02d".format(
                    millisUntilFinished.toMinutes(),
                    millisUntilFinished.toSeconds(),
                )
            }

            override fun onFinish() {
                isRecordCompleted = true
                stopRecording()
            }
        }

        var btnPauseMode = BUTTON_TYPE_START
        val btnPause = findViewById<TextView>(R.id.btn_chair_main_pause)
        btnPause.setOnClickListener {
            when (btnPauseMode) {
                BUTTON_TYPE_START -> {
                    startRecording()
                    btnPauseMode = BUTTON_TYPE_STOP
                    btnPause.text = getString(R.string.stop)
                }

                BUTTON_TYPE_STOP -> stopRecording()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_chair_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_chair_main_help -> {
                AlertDialog.Builder(this, R.style.AppDialog)
                    .setTitle("Instruções")
                    .setMessage("Você deverá iniciar o teste sentado e, ao som do sinal sonoro, " +
                            "você deverá levantar completamente e sentar novamente de forma " +
                            "repetida até que um novo sinal sonoro lhe indique o final do tempo " +
                            "de teste.")
                    .setPositiveButton(R.string.ok, dismissAction)
                    .create()
                    .show()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun startRecording() {
        startRegistering()
        writer.append(sensorContent.header)
        countDownTimer.start()
    }

    override fun stopRecording() {
        stopRegistering()
        writer.close()

        if (!isRecordCompleted) {
            toast("Você cancelou o registro.")
            launchActivity<MainActivity>()

        } else {
            val file = File.createTempFile("chair", ".tmp", saveFolder)
            FileWriter(file).append(writer.toString()).close()

            launchActivity<RecordActivity> { intent ->
                intent.putExtra(KEY_FILE_DIRECTORY, file.absolutePath)
                intent.putExtra(KEY_ACTIVITY_TITLE, "Resultado")
            }
        }

        countDownTimer.cancel()
        finish()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun pauseRecording() {}
    override fun resumeRecording() {}

    override fun onSensorChanged(event: SensorEvent) {
        sensorContent.update(event)
        writer.writeContent()
        sensorPlotter.updateGraphs()
        sensorContent.duration += recordingDelay
    }
}