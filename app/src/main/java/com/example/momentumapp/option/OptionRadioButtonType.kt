package com.example.momentumapp.option

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.momentumapp.R
import com.example.momentumapp.util.OPTION_TYPE_RADIO_BUTTON
import com.example.momentumapp.util.OptionContainer
import com.example.momentumapp.util.dismissAction
import com.example.momentumapp.util.prefs

/**
 * Implementation of a single choice option.
 */
class OptionRadioButtonType(
    title: String,
    key: String,
    val descriptions: Array<String>,
    val values: IntArray,
    val dialogItems: Array<String>,
    var selectedItemPos: Int,
) : BaseOptionType(title, OPTION_TYPE_RADIO_BUTTON, key) {
    class Builder(context: Context) {
        private val prefs = context.prefs
        private var optionTitle: String = ""
        private var optionKey: String = ""
        private var optionDescriptions = arrayListOf<String>()
        private var optionItems = arrayListOf<String>()
        private var optionValues = arrayListOf<Int>()
        private var optionDefaultValue = 0

        fun setTitle(value: String) = apply { optionTitle = value }
        fun setKey(value: String) = apply { optionKey = value }
        fun setDefaultValue(pos: Int) = apply { optionDefaultValue = optionValues[pos] }
        fun addValue(name: String, description: String, saveValue: Int) = apply {
            optionItems.add(name)
            optionDescriptions.add(description)
            optionValues.add(saveValue)
        }

        fun build() = OptionRadioButtonType(
            title = optionTitle,
            key = optionKey,
            values = optionValues.toIntArray(),
            descriptions = optionDescriptions.toTypedArray(),
            dialogItems = optionItems.toTypedArray(),
            selectedItemPos = optionValues.indexOf(prefs.getInt(optionKey, optionDefaultValue))
        )
    }

    class ViewHolder(view: View, container: OptionContainer) : BaseViewHolder(view) {
        override val txtOptionTitle: TextView = view.findViewById(R.id.txt_sel_option_title)
        override val txtOptionDescription: TextView =
            view.findViewById(R.id.txt_sel_option_description)
        override val layoutOption: LinearLayout = view.findViewById(R.id.lly_sel_option)

        init {
            layoutOption.setOnClickListener {
                container.onOptionClicked(absoluteAdapterPosition)
            }
        }
    }

    override fun getDialog(
        context: Context,
        action: () -> Unit,
    ): AlertDialog = AlertDialog.Builder(context, R.style.AppDialog)
        .setTitle(title)
        .setPositiveButton(context.getString(R.string.ok), dismissAction)
        .setSingleChoiceItems(dialogItems, selectedItemPos) { _, which ->
            selectedItemPos = which
            save(context, values[which])
            action()
        }
        .create()

    override fun onBindViewHolder(holder: BaseViewHolder) {
        holder.txtOptionTitle.text = title
        holder.txtOptionDescription.text = descriptions[selectedItemPos]
    }
}