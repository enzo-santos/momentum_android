package com.example.momentumapp.plotting

import com.example.momentumapp.util.getOrDefault
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.BaseSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

/**
 * Plots horizontal and vertical values on demand.
 */
class RealTimeGraphPlotter private constructor(
    graphView: GraphView,
    /**
     * The time window by which the graph will move.
     */
    private val timeWindow: Int,

    /**
     * The time delay by which the data is being plotted.
     */
    private val timeDelay: Double,

    /**
     * Defines how the left and right bound should be changed.
     *
     * If false, the left and right bound moves along with the appended data, not allowing the user
     * to see the values beyond the right bound. If true, as soon the values reach the right bound,
     * the left bound assumes the value of the right bound and the right bound assumes the value of
     * the previous right bound plus the [timeWindow], allowing the user to see the values beyond
     * the right bound.
     */
    private val isManualScroll: Boolean,
    resizeVertically: Boolean,
) : GraphPlotter(graphView, resizeVertically) {
    private val graphSeries: MutableMap<Int, BaseSeries<DataPoint>> = hashMapOf()

    class Builder {
        private var graphView: GraphView? = null
        private var timeWindow: Int? = null
        private var timeDelay: Double? = null
        private var isManualScroll: Boolean? = null
        private var resizeVertically: Boolean? = null

        fun withGraph(graph: GraphView) = apply { graphView = graph }
        fun setTimeWindow(value: Int) = apply { timeWindow = value }
        fun setTimeDelay(value: Double) = apply { timeDelay = value }
        fun setManualScroll(value: Boolean) = apply { isManualScroll = value }
        fun setVerticalResize(value: Boolean) = apply { resizeVertically = value }

        fun build(): RealTimeGraphPlotter = RealTimeGraphPlotter(
            graphView ?: error("'withGraph' must be defined"),
            timeWindow ?: error("'setTimeWindow' must be defined"),
            timeDelay ?: error("'setTimeDelay' must be defined"),
            isManualScroll ?: error("'setManualScroll' must be defined"),
            resizeVertically ?: error("'setVerticalResize' must be defined")
        )
    }

    override fun plot(data: GraphData, color: Int) {
        require(data.size == 1) { "you can only plot one point at a time" }
        super.plot(data, color)

        val (xPos, yPos) = data.first()

        val series: BaseSeries<DataPoint>
        series = graphSeries.getOrDefault(color) {
            val newSeries = LineGraphSeries<DataPoint>()
            newSeries.color = color
            newSeries.thickness = 2

            graphSeries[color] = newSeries
            graphView.addSeries(newSeries)

            newSeries
        }

        if (isManualScroll) {
            series.appendData(
                DataPoint(xPos, yPos),
                false,
                (1.0 / timeDelay * timeWindow).toInt()
            )

            if (xPos > viewport.getMaxX(false)) {
                viewport.setMinX(xPos)
                viewport.setMaxX(xPos + timeWindow)
            }

        } else {
            series.appendData(
                DataPoint(xPos, yPos),
                xPos > timeWindow,
                (1.0 / timeDelay * timeWindow).toInt()
            )
        }
    }
}