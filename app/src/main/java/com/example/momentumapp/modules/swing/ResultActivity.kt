package com.example.momentumapp.modules.swing

import android.app.Activity
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.momentumapp.MainActivity
import com.example.momentumapp.R
import com.example.momentumapp.layout.horizontalRange
import com.example.momentumapp.layout.verticalRange
import com.example.momentumapp.modules.fml.Range
import com.example.momentumapp.plotting.GraphData
import com.example.momentumapp.plotting.GraphPlotter
import com.example.momentumapp.plotting.UnorderedCurveGraphPlotter
import com.example.momentumapp.util.*
import com.jjoe64.graphview.GraphView
import java.io.StringReader
import java.lang.ref.WeakReference

class ResultActivity : AppCompatActivity() {
    class PlottingAsyncTask(
        activity: Activity,
        private val plotter: GraphPlotter,
    ) : AsyncTask<GraphData, Void, Unit>() {
        private val weakActivity = WeakReference(activity)
        private lateinit var progressDialog: AlertDialog
        private lateinit var progressText: TextView

        override fun onPreExecute() {
            super.onPreExecute()
            val activity = weakActivity.get() ?: return

            progressDialog = activity.launchDialog { builder ->
                val dialogLayout = activity.inflate(R.layout.dialog_progress)
                dialogLayout
                    .findViewById<TextView>(R.id.txt_dialog_progress_title)
                    .text = "Gerando resultado..."

                progressText = dialogLayout.findViewById(R.id.txt_dialog_progress)

                if (plotter is UnorderedCurveGraphPlotter) {
                    plotter.onProgressUpdateListener = { p, t ->
                        progressText.text = "%.2f%%".format(100.0 * p / t)
                    }
                }

                builder.setView(dialogLayout)
                    .setCancelable(false)
                    .setNegativeButton(activity.getString(R.string.cancel)) { dialog, _ ->
                        dialog.dismiss()
                        cancel(true)
                        activity.finish()
                    }
                    .create()
            }

            progressDialog.window?.setBackgroundDrawableResource(R.color.colorToolbar)
        }

        override fun doInBackground(vararg params: GraphData) {
            val (data) = params
            weakActivity.get()?.runOnUiThread {
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            progressDialog.dismiss()
        }
    }

    private lateinit var graphView: GraphView
    private lateinit var widthRange: Range
    private lateinit var heightRange: Range

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swing_result)
        setSupportActionBar(findViewById(R.id.tlb_swing_result))
        supportActionBar?.title = "Resultado"

        graphView = findViewById(R.id.gvw_swing_result)
        graphView.gridLabelRenderer.isHighlightZeroLines = false
        graphView.gridLabelRenderer.horizontalAxisTitle = "Aceleração X (m/s²)"
        graphView.gridLabelRenderer.verticalAxisTitle = "Aceleração Z (m/s²)"
        graphView.viewport.isScalable = true
        graphView.viewport.setScalableY(true)
        graphView.verticalRange = Range(-10.0, 10.0)
        graphView.horizontalRange = Range(-10.0, 10.0)

        val stringData = intent?.getStringExtra(KEY_RECORD_CONTENT) ?: ""
        RecordLoader(StringReader(stringData)).accelerationData?.let { data ->
            val plotData = GraphData(data.xAxis, data.zAxis)

            val curveGraphPlotter = UnorderedCurveGraphPlotter(
                graphView,
                resizeHorizontally = true,
                resizeVertically = true,
                minimum = -10.0,
                maximum = 10.0,
            )

            curveGraphPlotter.plot(plotData, Color.BLACK)
        }

        widthRange = graphView.horizontalRange
        heightRange = graphView.verticalRange

        findViewById<Button>(R.id.btn_swing_result_finish).setOnClickListener {
            launchActivity<MainActivity>()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_swing_result, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_menu_swing_result_options -> {
                val layout = inflate(R.layout.dialog_swing_graph_tune)
                val edtMinWidth = layout.findViewById<EditText>(R.id.edt_swing_width_min)
                val edtMaxWidth = layout.findViewById<EditText>(R.id.edt_swing_width_max)
                edtMinWidth.setText("%.2f".format(widthRange.minimum))
                edtMaxWidth.setText("%.2f".format(widthRange.maximum))

                val edtMinHeight = layout.findViewById<EditText>(R.id.edt_swing_height_min)
                val edtMaxHeight = layout.findViewById<EditText>(R.id.edt_swing_height_max)
                edtMinHeight.setText("%.2f".format(heightRange.minimum))
                edtMaxHeight.setText("%.2f".format(heightRange.maximum))

                AlertDialog.Builder(this, R.style.AppDialog)
                    .setTitle("Ajustar gráfico")
                    .setView(layout)
                    .setPositiveButton("OK") { _, _ ->
                        edtMinWidth.text.toString().toDoubleOrNull()?.also { minWidth ->
                            widthRange = widthRange.withMinimum(minWidth)
                            graphView.horizontalRange = widthRange
                        }

                        edtMaxWidth.text.toString().toDoubleOrNull()?.also { maxWidth ->
                            widthRange = widthRange.withMaximum(maxWidth)
                            graphView.horizontalRange = widthRange
                        }

                        edtMinHeight.text.toString().toDoubleOrNull()?.also { minHeight ->
                            heightRange = heightRange.withMinimum(minHeight)
                            graphView.verticalRange = heightRange
                        }

                        edtMaxHeight.text.toString().toDoubleOrNull()?.also { maxHeight ->
                            heightRange = heightRange.withMaximum(maxHeight)
                            graphView.verticalRange = heightRange
                        }

                        graphView.invalidate()
                    }
                    .create()
                    .show()

                true
            }
            R.id.item_menu_swing_result_share -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}