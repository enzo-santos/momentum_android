package com.example.momentumapp.util

import android.widget.Button

class ButtonController(
    private val button: Button,
    initialMode: Int,
    nodes: Set<Node>,
) {
    data class Node(val fromMode: Int, val toMode: Int, val text: String, val action: () -> Unit)

    private val nodes: Map<Int, Node> = nodes.associateBy { node -> node.fromMode }
    private var mode: Int = initialMode

    init {
        button.setOnClickListener {
            val (_, toMode, text, action) = this.nodes[mode] ?: return@setOnClickListener
            button.text = text
            mode = toMode
            action()
        }
    }
}