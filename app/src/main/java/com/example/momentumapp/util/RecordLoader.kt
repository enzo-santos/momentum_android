package com.example.momentumapp.util

import java.io.BufferedReader
import java.io.FileReader
import java.io.Reader

/**
 * Reads information of a record created by this application.
 */
class RecordLoader(
    /**
     * A reader that contains information to be read.
     */
    reader: Reader,
) {
    companion object {
        /**
         * Creates a record file loader from a [path] of this record in the device.
         */
        fun fromPath(path: String): RecordLoader = RecordLoader(FileReader(path))
    }

    /**
     * Converts the information data of this record to the frequency domain.
     */
    fun toRFFT() = also {
        it.isAcceleration = isAcceleration
        it.isVelocity = isVelocity
        it.isDisplacement = isDisplacement
        it.isRotation = isRotation

        it.accelerationData = it.accelerationData?.toRFFT()
        it.velocityData = it.velocityData?.toRFFT()
        it.displacementData = it.displacementData?.toRFFT()
        it.rotationData = it.rotationData?.toRFFT()
    }

    /**
     * If acceleration data is available is this file.
     */
    private var isAcceleration = false

    /**
     * If velocity data is available is this file.
     */
    private var isVelocity = false

    /**
     * If displacement data is available in this file.
     */
    private var isDisplacement = false

    /**
     * If rotation data is available is this file.
     */
    private var isRotation = false

    /**
     * The acceleration data related to this file.
     *
     * If it's not present in the file, its value is null.
     */
    var accelerationData: RecordData? = null

    /**
     * The velocity data related to this file.
     *
     * If it's not present in the file, its value is null.
     */
    var velocityData: RecordData? = null

    /**
     * The displacement data related to this file.
     *
     * If it's not present in the file, its value is null.
     */
    var displacementData: RecordData? = null

    /**
     * The rotation data related to this file.
     *
     * If it's not present in the file, its value is null.
     */
    var rotationData: RecordData? = null

    /**
     * Reads the [header] of a file.
     *
     * It verifies if information related to each metric is available in the file.
     */
    private fun readHeader(header: String) {
        isAcceleration =
            header.contains("ACC EIXO X${FILE_SEPARATOR}ACC EIXO Y${FILE_SEPARATOR}ACC EIXO Z")
        isVelocity =
            header.contains("VEL EIXO X${FILE_SEPARATOR}VEL EIXO Y${FILE_SEPARATOR}VEL EIXO Z")
        isDisplacement =
            header.contains("DESL EIXO X${FILE_SEPARATOR}DESL EIXO Y${FILE_SEPARATOR}DESL EIXO Z")
        isRotation =
            header.contains("GIR EIXO X${FILE_SEPARATOR}GIR EIXO Y${FILE_SEPARATOR}GIR EIXO Z")

        if (isAcceleration) accelerationData = RecordData()
        if (isVelocity) velocityData = RecordData()
        if (isDisplacement) displacementData = RecordData()
        if (isRotation) rotationData = RecordData()
    }

    /**
     * Reads a [line] of content of a file.
     *
     * It updates information related to each metric.
     */
    private fun readContent(line: String) {
        val rowValues = line.replace(",", ".").split(FILE_SEPARATOR).map(String::toDouble)

        accelerationData?.also { data -> data.duration += rowValues[0] * .001 }
        velocityData?.also { data -> data.duration += rowValues[0] * .001 }
        displacementData?.also { data -> data.duration += rowValues[0] * .001 }
        rotationData?.also { data -> data.duration += rowValues[0] * .001 }

        var isAccWritten = false
        var isVelWritten = false
        var isDisWritten = false
        var isRotWritten = false

        (1 until rowValues.size step 3).forEach { i ->
            when {
                isAcceleration && !isAccWritten -> {
                    isAccWritten = true
                    accelerationData
                }

                isVelocity && !isVelWritten -> {
                    isVelWritten = true
                    velocityData
                }

                isDisplacement && !isDisWritten -> {
                    isDisWritten = true
                    displacementData
                }

                isRotation && !isRotWritten -> {
                    isRotWritten = true
                    rotationData
                }

                else -> null

            }?.also { data ->
                data.xAxis += rowValues[i]
                data.yAxis += rowValues[i + 1]
                data.zAxis += rowValues[i + 2]
            }
        }
    }

    init {
        BufferedReader(reader).use { bufferedReader ->
            readHeader(bufferedReader.readLine() ?: return@use)
            while (true) {
                readContent(bufferedReader.readLine() ?: break)
            }
        }
    }
}