package com.example.momentumapp.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.cardview.widget.CardView
import com.example.momentumapp.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class RecordArrayAdapter(
    context: Context,
    objects: List<Holder>,
    private val listener: RecordContainer,
) : ArrayAdapter<RecordArrayAdapter.Holder>(context, R.layout.item_records_record, objects),
    Iterable<RecordArrayAdapter.Holder> {
    private class ArrayAdapterIterator<T>(val arrayAdapter: ArrayAdapter<T>) : Iterator<T> {
        var i = 0
        val size
            get() = arrayAdapter.count

        override fun hasNext(): Boolean = i < size
        override fun next(): T {
            while (true) {
                if (i == size) error("invalid position")
                return arrayAdapter.getItem(i++) ?: continue
            }
        }
    }

    /**
     * Represents a record file metadata.
     */
    data class Holder(
        /**
         * The filename of a record.
         */
        val filename: String,
        /**
         * The last modified date of this record.
         */
        val lastModified: String,
        /**
         * The directory of this record.
         */
        val directory: String,
    ) {
        var isChecked = false

        companion object {
            /**
             * Retrieves a record metadata from a [file].
             */
            fun fromFile(file: File): Holder = Holder(
                file.name,
                SimpleDateFormat("dd/MM/yyyy, HH:mm:ss", Locale.US).format(file.lastModified()),
                file.absolutePath
            )

            val `null`: Holder
                get() = Holder("", "", "")
        }

        override fun toString(): String = filename.toLowerCase(Locale.getDefault())
    }

    constructor(activity: Activity, objects: List<Holder>)
            : this(activity, objects, activity as RecordContainer)

    private var mode = 0
    private fun CardView.select() = setCardBackgroundColor(0x550000FF)
    private fun CardView.deselect() = setCardBackgroundColor(0x000000FF)

    fun clearSelection() {
        forEach { holder -> holder.isChecked = false }.also { updateMode() }
    }

    fun removeSelection() {
        filter(Holder::isChecked).forEach { `object` ->
            listener.onItemClicked("Excluir", getPosition(`object`))
        }
    }

    fun hasSelection(): Boolean = any(Holder::isChecked)

    private fun updateMode() {
        val newMode = if (!any { holder -> holder.isChecked }) 0 else 1
        if (newMode != mode) {
            mode = newMode
            listener.onSelectionModeChanged(mode)
        }

        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder = getItem(position) ?: Holder.`null`

        return (convertView ?: parent.inflate(R.layout.item_records_record)).also { v ->
            val txtFilename: TextView = v.findViewById(R.id.txt_item_record_name)
            txtFilename.text = holder.filename

            val txtLastModified: TextView = v.findViewById(R.id.txt_item_record_date)
            txtLastModified.text = "Modificado em %s".format(holder.lastModified)

            val parentLayout: LinearLayout = v.findViewById(R.id.lly_item_records)
            val cardView: CardView = parentLayout.findViewById(R.id.cvw_item_records)
            if (holder.isChecked) cardView.select() else cardView.deselect()

            parentLayout.setOnClickListener {
                when (mode) {
                    0 -> listener.onCardClicked(position)
                    1 -> {
                        holder.isChecked = !holder.isChecked
                        updateMode()
                    }
                }
            }

            parentLayout.setOnLongClickListener {
                when (mode) {
                    0 -> {
                        holder.isChecked = true
                        updateMode()
                        true
                    }
                    else -> false
                }

            }

            val btnShowOptions: ImageView = v.findViewById(R.id.btn_item_record_delete)
            if (mode == 0) {
                btnShowOptions.isEnabled = true
                btnShowOptions.setOnClickListener {
                    listener.onItemClicked("Excluir", position)
                }

            } else {
                btnShowOptions.isEnabled = false
                btnShowOptions.setOnClickListener(null)
            }
        }
    }

    override fun iterator(): Iterator<Holder> = ArrayAdapterIterator(this)
}