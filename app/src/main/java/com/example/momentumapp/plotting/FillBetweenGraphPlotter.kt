package com.example.momentumapp.plotting

import android.graphics.Color
import android.os.Build
import android.view.ViewTreeObserver
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries
import kotlin.math.abs

/**
 * Fill some area above and below a curve in a graph.
 */
class FillBetweenGraphPlotter(
    graphView: GraphView,
    /**
     * The area to be plotted above and below some curve.
     */
    private val range: Double,
    resizeVertically: Boolean,
) : GraphPlotter(graphView, resizeVertically) {
    override fun plot(data: GraphData, color: Int) {
        super.plot(data.mapRange { it - range }, color)
        super.plot(data.mapRange { it + range }, color)

        val points = data
            .map { (x, y) -> DataPoint(x, y) }.dropLast(1).toTypedArray()

        val series = PointsGraphSeries(points)

        series.color = Color.argb(
            128,
            Color.red(color),
            Color.green(color),
            Color.blue(color)
        )

        graphView.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    graphView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }

                val xData = data.getXData()
                val dx = (xData[1] - xData[0]) * (graphView.graphContentWidth) / abs(
                    maxX - minX
                )
                val dy = range * (graphView.graphContentHeight) / abs(maxY - minY)

                series.setCustomShape { canvas, paint, x, y, _ ->
                    canvas.drawRect(
                        x,
                        (y + dy).toFloat(),
                        (x + dx).toFloat(),
                        (y - dy).toFloat(),
                        paint
                    )
                }
            }
        })

        graphView.addSeries(series)
    }
}