package com.example.momentumapp.util

import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*

class ExceptionManager(private val directory: String) : Thread.UncaughtExceptionHandler {
    private val defaultHandler = Thread.getDefaultUncaughtExceptionHandler()

    override fun uncaughtException(t: Thread, e: Throwable) {
        val stacktrace = StringWriter()
            .also { PrintWriter(it).use(e::printStackTrace) }
            .toString()

        writeToFile(stacktrace)
        defaultHandler?.uncaughtException(t, e)
    }

    private fun writeToFile(stacktrace: String) {
        try {
            val folder = File(directory, CRASH_FOLDER_NAME).also { folder ->
                if (!folder.exists())
                    folder.mkdirs()
            }

            val filename = SimpleDateFormat(CRASH_FILENAME_FORMAT, Locale.US).format(Date())
            FileWriter(File(folder, "$filename.txt")).use { writer ->
                writer.append(stacktrace)
                writer.flush()
            }

        } catch (e: Exception) {

        }
    }
}