package com.example.momentumapp.util

import android.content.Context
import android.content.SharedPreferences

const val APP_TITLE = "Momentum"
const val SAVE_FOLDER_NAME = "MomentumDados"
const val CRASH_FOLDER_NAME = "CrashReports"
const val BLUETOOTH_RECORD_SUFFIX = "GravacaoBluetooth"
const val APPLICATION_AUTHORITY = "com.example.momentumapp.fileprovider"

const val FILE_SEPARATOR = ";"
const val CRASH_FILENAME_FORMAT = "yyyy_MM_dd_HH_mm_ss"

const val KEY_IS_FIRST_EXECUTION = "KEY_PRIMEIRA_EXECUCAO"
const val KEY_IS_WARN_SENSOR = "KEY_IS_AVISO_SENSOR"
const val KEY_RECORDING_DELAY = "KEY_DELAY_GRAVACAO"
const val KEY_IS_LIMIT_RECORDING = "KEY_IS_NUM_MAX_LINHAS"
const val KEY_MAXIMUM_NUMBER_LINES = "KEY_NUM_MAX_LINHAS"
const val KEY_FILE_EXTENSION = "KEY_EXTENSAO_ARQUIVO"
const val KEY_CALCULATION_MODE = "KEY_MODO_CALCULO"
const val KEY_IS_COMPACT_AXIS = "KEY_COMPACTAR_EIXOS"
const val KEY_VISIBLE_GRAPHS = "KEY_GRAFICOS_VISIVEIS"
const val KEY_ORDER_RECORDS_BY_TYPE = "KEY_ORDENAR_REGISTROS_TIPO"
const val KEY_ORDER_RECORDS_MODE = "KEY_ORDENAR_REGISTROS_MODO"
const val KEY_DECIMAL_PLACES = "KEY_DECIMAL_PLACES"
const val KEY_DECIMAL_SEPARATOR = "KEY_DECIMAL_SEPARATOR"
const val KEY_WARNER_TYPE = "KEY_WARNER_TYPE"

const val KEY_FILE_DIRECTORY = "KEY_DIRETORIO_ARQUIVO"
const val KEY_ERROR_MARGIN = "KEY_ERROR_MARGIN"
const val KEY_AXES_INCLUDED = "KEY_AXES_INCLUDED"
const val KEY_X_AXIS_DATA = "KEY_X_AXIS_DATA"
const val KEY_Y_AXIS = "KEY_Y_AXIS"
const val KEY_Y_AXIS_X_DATA = "KEY_Y_AXIS_X_DATA"
const val KEY_Y_AXIS_Y_DATA = "KEY_Y_AXIS_Y_DATA"
const val KEY_Y_AXIS_Z_DATA = "KEY_Y_AXIS_Z_DATA"
const val KEY_IS_TEMPORARY_FILE = "KEY_IS_TEMPORARY_FILE"
const val KEY_NUMBER_ITERATIONS = "KEY_NUMBER_ITERATIONS"
const val KEY_ACTUAL_ITERATION = "KEY_ACTUAL_ITERATION"
const val KEY_TOTAL_HITS = "KEY_TOTAL_HITS"
const val KEY_TOTAL_MISSES = "KEY_TOTAL_MISSES"
const val KEY_IS_PARENT_ACTIVITY = "KEY_IS_PARENT_ACTIVITY"
const val KEY_X_MEAN_ERROR = "KEY_X_MEAN_ERROR"
const val KEY_Y_MEAN_ERROR = "KEY_Y_MEAN_ERROR"
const val KEY_Z_MEAN_ERROR = "KEY_Z_MEAN_ERROR"
const val KEY_RECORD_CONTENT = "KEY_RECORD_CONTENT"
const val KEY_ACTIVITY_TITLE = "KEY_ACTIVITY_TITLE"

const val RECORDING_MODE_DEFAULT = 0
const val RECORDING_MODE_BLUETOOTH = 1

const val USER_TYPE_RECEIVER = 0
const val USER_TYPE_SENDER = 1

const val BUTTON_TYPE_START = 0
const val BUTTON_TYPE_PAUSE = 1
const val BUTTON_TYPE_RESUME = 2
const val BUTTON_TYPE_STOP = 3

const val HANDLER_TOAST = 0

const val OPTION_TYPE_RADIO_BUTTON = 0
const val OPTION_TYPE_MUTABLE_SWITCH = 1
const val OPTION_TYPE_CHECKBOX = 2
const val OPTION_TYPE_SWITCH = 3
const val OPTION_TYPE_MUTABLE = 4

const val OPTION_DELAY_NORMAL = 200
const val OPTION_DELAY_ABOVE_NORMAL = 100
const val OPTION_DELAY_FAST = 60
const val OPTION_DELAY_ULTRA_FAST = 20

const val OPTION_CSV_EXTENSION = 0
const val OPTION_TXT_EXTENSION = 1

const val OPTION_CALCULATION_RAW = 0
const val OPTION_CALCULATION_VARIATION = 1

const val OPTION_ORDER_BY_NAME = 0
const val OPTION_ORDER_BY_DATE = 1

const val OPTION_ORDER_ASCENDING = 0
const val OPTION_ORDER_DESCENDING = 1

const val OPTION_SEPARATOR_COMMA = 0
const val OPTION_SEPARATOR_PERIOD = 1

const val MESSAGE_STOP_RECORDING = "0"
const val MESSAGE_START_RECORDING = "1"
const val MESSAGE_PAUSE_RECORDING = "2"
const val MESSAGE_RESUME_RECORDING = "3"

const val WARNER_TYPE_BUZZER = 0
const val WARNER_TYPE_VIBRATOR = 1


val Context.fileExtension: String
    get() = when (val value = prefs.getInt(KEY_FILE_EXTENSION, OPTION_CSV_EXTENSION)) {
        OPTION_CSV_EXTENSION -> "csv"
        OPTION_TXT_EXTENSION -> "txt"
        else -> error("invalid value: $value")
    }

val Context.decimalSeparator: String
    get() = when (val value = prefs.getInt(KEY_DECIMAL_SEPARATOR, OPTION_SEPARATOR_PERIOD)) {
        OPTION_SEPARATOR_COMMA -> ","
        OPTION_SEPARATOR_PERIOD -> "."
        else -> error("invalid value: $value")
    }

val Context.calculationMode: (Double, Double) -> Double
    get() = when (val mode = prefs.getInt(KEY_CALCULATION_MODE, OPTION_CALCULATION_RAW)) {
        OPTION_CALCULATION_RAW -> { _, new -> new }
        OPTION_CALCULATION_VARIATION -> { old, new -> new - old }
        else -> error("invalid value: $mode")
    }