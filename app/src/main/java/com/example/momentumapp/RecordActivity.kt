package com.example.momentumapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.example.momentumapp.layout.SensorGraphLegend
import com.example.momentumapp.layout.SensorGraphView
import com.example.momentumapp.layout.SensorGraphViewGroup
import com.example.momentumapp.layout.horizontalRange
import com.example.momentumapp.modules.fml.Range
import com.example.momentumapp.plotting.CurveGraphPlotter
import com.example.momentumapp.plotting.GraphData
import com.example.momentumapp.plotting.SingleGraphSensorPlotter
import com.example.momentumapp.plotting.TripleGraphSensorPlotter
import com.example.momentumapp.util.*
import java.io.File

class RecordActivity : AppCompatActivity() {
    private lateinit var sensors: Array<String>
    private lateinit var metrics: Array<String>
    private lateinit var units: Array<String>

    private lateinit var directory: String
    private lateinit var axisLegend: SensorGraphLegend
    private lateinit var graphsLayout: LinearLayout
    internal var recordingsArray = arrayOfNulls<RecordData>(8)

    private fun populateSingleGraphsFromRecords() {
        var isAccelerometerVisible = false
        recordingsArray.forEachIndexed { i, record ->
            record ?: return@forEachIndexed

            graphsLayout.addView(SensorGraphView(this).also {
                it.sensorAxis = ""
                it.sensorName = sensors[i]
                it.horizontalAxisLabel = units[i]
                it.verticalAxisLabel = metrics[i]
                it.isHideGraphInfo = isAccelerometerVisible

                it.minX = record.duration.amin()
                it.maxX = record.duration.amax()

                SingleGraphSensorPlotter(it).plot(
                    GraphData(record.duration, record.xAxis),
                    GraphData(record.duration, record.yAxis),
                    GraphData(record.duration, record.zAxis),
                    intArrayOf(Color.RED, Color.GREEN, Color.BLUE),
                ) { graph ->
                    CurveGraphPlotter(
                        graph,
                        resizeHorizontally = true,
                        resizeVertically = true,
                    )
                }

                it.setOnGraphClickListener {
                    launchActivity<FullscreenGraphActivity> { intent ->
                        intent.putExtra(KEY_IS_COMPACT_AXIS, true)
                        intent.putExtra(KEY_X_AXIS_DATA, record.duration)
                        intent.putExtra(KEY_Y_AXIS_X_DATA, record.xAxis)
                        intent.putExtra(KEY_Y_AXIS_Y_DATA, record.yAxis)
                        intent.putExtra(KEY_Y_AXIS_Z_DATA, record.zAxis)
                    }
                }
            })

            if (!isAccelerometerVisible && getString(R.string.accelerometer) in sensors[i]) {
                isAccelerometerVisible = true
            }
        }
    }

    private fun populateMultiGraphsFromRecords() {
        recordingsArray.forEachIndexed { i, record ->
            record ?: return@forEachIndexed

            graphsLayout.addView(SensorGraphViewGroup(this).also {
                it.sensorName = sensors[i]
                it.horizontalAxisLabel = units[i]
                it.verticalAxisLabel = metrics[i]

                it.applyToGraphs { graph ->
                    graph.minX = record.duration.amin()
                    graph.maxX = record.duration.amax()
                }

                TripleGraphSensorPlotter.fromList(it.sensorGraphViews.toList(), true).plot(
                    GraphData(record.duration, record.xAxis),
                    GraphData(record.duration, record.yAxis),
                    GraphData(record.duration, record.zAxis),
                    intArrayOf(Color.BLACK, Color.BLACK, Color.BLACK)
                ) { graph ->
                    CurveGraphPlotter(
                        graph,
                        resizeHorizontally = true,
                        resizeVertically = true,
                    )
                }

                val data = arrayOf(record.xAxis, record.yAxis, record.zAxis)
                it.sensorGraphViews.forEachIndexed { i, graph ->
                    graph.setOnGraphClickListener {
                        launchActivity<FullscreenGraphActivity> { intent ->
                            intent.putExtra(KEY_IS_COMPACT_AXIS, false)
                            intent.putExtra(KEY_X_AXIS_DATA, record.duration)
                            intent.putExtra(KEY_Y_AXIS, data[i])
                        }
                    }
                }
            })
        }
    }

    internal var isCompactAxis = false
        set(value) {
            axisLegend.visibility = if (value) View.VISIBLE else View.GONE
            graphsLayout.removeAllViews()

            if (value) {
                populateSingleGraphsFromRecords()

            } else {
                populateMultiGraphsFromRecords()
            }

            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)
        setSupportActionBar(findViewById(R.id.tlb_record))

        directory = intent?.getStringExtra(KEY_FILE_DIRECTORY) ?: ""
        title = intent?.getStringExtra(KEY_ACTIVITY_TITLE) ?: directory.split("/").last()

        sensors = arrayOf(
            getString(R.string.accelerometer),
            getString(R.string.accelerometer),
            getString(R.string.accelerometer),
            getString(R.string.gyroscope),
            "${getString(R.string.accelerometer)} (${getString(R.string.fft)})",
            "${getString(R.string.accelerometer)} (${getString(R.string.fft)})",
            "${getString(R.string.accelerometer)} (${getString(R.string.fft)})",
            "${getString(R.string.gyroscope)} (${getString(R.string.fft)})",
        )

        metrics = arrayOf(
            "${getString(R.string.acceleration)} (${getString(R.string.metre_per_second_squared_unit)})",
            "${getString(R.string.velocity)} (${getString(R.string.meter_per_second_unit)})",
            "${getString(R.string.displacement)} (${getString(R.string.meter_unit)})",
            "${getString(R.string.angular_velocity)} (${getString(R.string.radian_per_second_unit)})",
            "${getString(R.string.acceleration)} (${getString(R.string.metre_per_second_squared_unit)})",
            "${getString(R.string.velocity)} (${getString(R.string.meter_per_second_unit)})",
            "${getString(R.string.displacement)} (${getString(R.string.meter_unit)})",
            "${getString(R.string.angular_velocity)} (${getString(R.string.radian_per_second_unit)})",
        )

        units = arrayOf(
            "${getString(R.string.time)} (${getString(R.string.second_unit)})",
            "${getString(R.string.time)} (${getString(R.string.second_unit)})",
            "${getString(R.string.time)} (${getString(R.string.second_unit)})",
            "${getString(R.string.time)} (${getString(R.string.second_unit)})",
            "${getString(R.string.frequency)} (${getString(R.string.hertz_unit)})",
            "${getString(R.string.frequency)} (${getString(R.string.hertz_unit)})",
            "${getString(R.string.frequency)} (${getString(R.string.hertz_unit)})",
            "${getString(R.string.frequency)} (${getString(R.string.hertz_unit)})",
        )

        axisLegend = findViewById(R.id.sgl_record)
        graphsLayout = findViewById(R.id.lly_record_graphs)

        RecordLoadingTask(this).execute(directory)

        findViewById<Switch>(R.id.swt_record_compact_axis)
            .setOnCheckedChangeListener { _, isChecked ->
                isCompactAxis = isChecked
            }

        findViewById<Button>(R.id.btn_record_analysis)
            .setOnClickListener {
                launchActivity<AnalysisActivity> { intent ->
                    intent.putExtra(KEY_FILE_DIRECTORY, directory)
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_record, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_record_share -> {
                val file = File(directory)

                val uri = FileProvider.getUriForFile(this, APPLICATION_AUTHORITY, file)
                val intent = Intent(Intent.ACTION_SEND).also {
                    it.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    it.putExtra(Intent.EXTRA_TEXT, getString(R.string.record_intent_send_message))
                    it.putExtra(Intent.EXTRA_STREAM, uri)
                    it.setDataAndType(uri, "text/csv")
                }

                if (file.exists() && intent.resolveActivity(packageManager) != null) {
                    startActivity(
                        Intent.createChooser(
                            intent,
                            getString(R.string.record_intent_send_title)
                        )
                    )
                }

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}