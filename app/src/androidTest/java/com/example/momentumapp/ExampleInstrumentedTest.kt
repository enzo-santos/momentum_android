package com.example.momentumapp

import androidx.test.espresso.Espresso.*
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class ExampleInstrumentedTest {
    @Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun validateMainScreen() {
        onView(withId(R.id.lly_main_start)).check(matches(isDisplayed()))
        onView(withId(R.id.lly_main_options)).check(matches(isDisplayed()))
        onView(withId(R.id.lly_main_about)).check(matches(isDisplayed()))

        onView(withId(R.id.txt_main_start))
            .check(matches(isDisplayed()))

    }
}
