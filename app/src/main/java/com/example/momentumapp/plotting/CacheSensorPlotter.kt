package com.example.momentumapp.plotting

import com.example.momentumapp.util.Plotting

/**
 * Provided functionality for a three-axis sensor plotter, with abstract caching.
 *
 * Caching plotters may be useful when you need to keep the reference of a plotter (and consequently
 * the data that had already been plotted in it).
 */
interface CacheSensorPlotter : SensorPlotter {
    /**
     * Plots the [xAxisData], the [yAxisData] and the [zAxisData] given its respective [colors]
     * and a [plotting] action.
     *
     * [cache] defines if the caching is enabled or disabled.
     */
    fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        cache: Boolean,
        plotting: Plotting,
    )

    /**
     * Plots the [xAxisData], the [yAxisData] and the [zAxisData] given its respective [colors]
     * and a [plotting] action, with caching disabled.
     */
    override fun plot(
        xAxisData: GraphData?,
        yAxisData: GraphData?,
        zAxisData: GraphData?,
        colors: IntArray,
        plotting: Plotting,
    ) {
        plot(xAxisData, yAxisData, zAxisData, colors, false, plotting)
    }
}
