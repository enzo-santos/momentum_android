package com.example.momentumapp

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.momentumapp.util.*
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries

class FullscreenGraphActivity : AppCompatActivity() {
    private lateinit var graph: GraphView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreengraph)

        graph = findViewById(R.id.graphRegistroFullscreen)

        val xData = intent.getDoubleArrayExtra(KEY_X_AXIS_DATA)!!

        if (intent.getBooleanExtra(KEY_IS_COMPACT_AXIS, false)) {
            val yDataXAxis = intent.getDoubleArrayExtra(KEY_Y_AXIS_X_DATA) ?: doubleArrayOf()
            val yDataYAxis = intent.getDoubleArrayExtra(KEY_Y_AXIS_Y_DATA) ?: doubleArrayOf()
            val yDataZAxis = intent.getDoubleArrayExtra(KEY_Y_AXIS_Z_DATA) ?: doubleArrayOf()
            setCurve(xData, yDataXAxis, yDataYAxis, yDataZAxis)

        } else {
            val yData = intent.getDoubleArrayExtra(KEY_Y_AXIS) ?: doubleArrayOf()
            setCurve(xData, yData)
        }
    }

    private fun setCurve(xValues: DoubleArray, yValues: DoubleArray) {
        graph.viewport.isScalable = true
        graph.viewport.setScalableY(true)
        graph.viewport.isXAxisBoundsManual = true
        graph.viewport.setMinX(xValues.amin())
        graph.viewport.setMaxX(xValues.amax())
        graph.gridLabelRenderer.isHighlightZeroLines = false

        graph.viewport.isYAxisBoundsManual = true
        graph.viewport.setMinY(yValues.amin() - 10.0)
        graph.viewport.setMaxY(yValues.amax() + 10.0)

        val graphSeries = LineGraphSeries(Array(xValues.size) {
            DataPoint(xValues[it], yValues[it])
        })
        graphSeries.thickness = 3

        graph.addSeries(graphSeries)
    }

    private fun setCurve(
        xValues: DoubleArray, yValuesXAxis: DoubleArray,
        yValuesYAxis: DoubleArray, yValuesZAxis: DoubleArray,
    ) {

        val minY =
            doubleArrayOf(yValuesXAxis.amin(), yValuesYAxis.amin(), yValuesZAxis.amin()).amin()
        val maxY =
            doubleArrayOf(yValuesXAxis.amax(), yValuesYAxis.amax(), yValuesZAxis.amax()).amax()

        graph.viewport.isScalable = true
        graph.viewport.setScalableY(true)
        graph.viewport.isXAxisBoundsManual = true
        graph.viewport.setMinX(xValues.amin())
        graph.viewport.setMaxX(xValues.amax())
        graph.gridLabelRenderer.isHighlightZeroLines = false

        graph.viewport.isYAxisBoundsManual = true
        graph.viewport.setMinY(minY - 10.0)
        graph.viewport.setMaxY(maxY + 10.0)

        intArrayOf(Color.RED, Color.GREEN, Color.BLUE).forEach { color ->
            val graphSeries = LineGraphSeries(Array(xValues.size) {
                when (color) {
                    Color.RED -> DataPoint(xValues[it], yValuesXAxis[it])
                    Color.GREEN -> DataPoint(xValues[it], yValuesYAxis[it])
                    Color.BLUE -> DataPoint(xValues[it], yValuesZAxis[it])
                    else -> throw RuntimeException()
                }
            })

            graphSeries.thickness = 3
            graphSeries.color = color
            graph.addSeries(graphSeries)
        }
    }
}