package com.example.momentumapp.layout

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import com.example.momentumapp.R
import com.example.momentumapp.util.toPixels

class Table : TableLayout {
    private lateinit var tableLayout: TableLayout

    constructor (context: Context) : super(context) {
        attrs = null
        initComponents()
    }

    constructor (context: Context, attrSet: AttributeSet) : super(context, attrSet) {
        attrs = attrSet
        initComponents()
    }

    private val attrs: AttributeSet?

    private fun initComponents() {
        inflate(context, R.layout.view_table, this)
        tableLayout = findViewById(R.id.tly_view_table)
    }

    private fun addSubHeader(title: String) {
        val px5 = context.toPixels(5)

        val row = TableRow(context)
        row.setPadding(px5, px5, px5, px5)
        row.gravity = Gravity.CENTER

        row.addView(TextView(context).also {
            it.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT, 1.0f
            )
            TextViewCompat.setTextAppearance(it, R.style.AppAnaliseMetrica)
            it.text = title
        })

        tableLayout.addView(row)
    }

    private fun addItem(key: String, value: String) {
        val px5 = context.toPixels(5)

        val row = TableRow(context)
        row.setPadding(px5, px5, px5, px5)
        row.gravity = Gravity.CENTER

        row.addView(TextView(context).also {
            it.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.MATCH_PARENT, 7.0f
            )
            TextViewCompat.setTextAppearance(it, R.style.AppAnaliseKey)
            it.text = key
        })

        row.addView(TextView(context).also {
            it.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.MATCH_PARENT, 3.0f
            )
            TextViewCompat.setTextAppearance(it, R.style.AppAnaliseValue)
            it.text = value
        })

        tableLayout.addView(row)
    }

    fun addHeader(title: String, subtitle: String) {
        val px5 = context.toPixels(5)

        val layout = LinearLayout(context)
        layout.setPadding(px5, px5, px5, px5)

        layout.addView(TextView(context).also {
            it.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            val params = it.layoutParams as LinearLayout.LayoutParams
            params.setMargins(0, 0, px5, 0)
            TextViewCompat.setTextAppearance(it, R.style.AppGraphLegendAxis)
            it.gravity = Gravity.BOTTOM
            it.text = title
        })

        layout.addView(TextView(context).also {
            it.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            TextViewCompat.setTextAppearance(it, R.style.AppGraphLegendSensor)
            it.gravity = Gravity.BOTTOM
            it.text = subtitle
        })

        tableLayout.addView(layout)
    }

    fun addContent(title: String, content: LinkedHashMap<String, String>) {
        addSubHeader(title)
        content.forEach { (k, v) -> addItem(k, v) }
    }
}