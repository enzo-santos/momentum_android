package com.example.momentumapp

import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.iterator
import androidx.drawerlayout.widget.DrawerLayout
import com.example.momentumapp.util.*
import com.google.android.material.navigation.NavigationView
import com.example.momentumapp.modules.chair.MainActivity as ChairMainActivity
import com.example.momentumapp.modules.fml.MainActivity as FmlMainActivity
import com.example.momentumapp.modules.swing.MainActivity as SwingMainActivity
import com.example.momentumapp.modules.tug.MainActivity as TugMainActivity

class MainActivity : AppCompatActivity() {
    private lateinit var mainLayout: RelativeLayout
    private lateinit var btnStartRecording: LinearLayout

    private lateinit var navigationView: NavigationView
    private lateinit var drawerLayout: DrawerLayout

    companion object {
        private const val WRITE_PERMISSION_REQUEST_CODE = 1
    }

    private fun generateSensorWarning() {
        if (!prefs.getBoolean(KEY_IS_WARN_SENSOR, true))
            return

        val isSensorsMissing = booleanArrayOf(true, true)
        (getSystemService(SENSOR_SERVICE) as SensorManager).also {
            isSensorsMissing[0] = it.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null
            isSensorsMissing[1] = it.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null
        }

        if (isSensorsMissing.all { !it })
            return

        val sensorWarningLayout = inflate(R.layout.content_main_warning)

        var warningMessage = getString(R.string.main_warning_first_chunk)
        warningMessage += when {
            isSensorsMissing.all() -> getString(R.string.main_warning_second_chunk_both)
            isSensorsMissing[0] -> getString(R.string.main_warning_second_chunk_accelerometer)
            isSensorsMissing[1] -> getString(R.string.main_warning_second_chunk_gyroscope)
            else -> throw RuntimeException()
        }

        warningMessage += getString(R.string.main_warning_third_chunk)
        warningMessage += if (isSensorsMissing.all()) {
            getString(R.string.main_warning_fourth_chunk_plural)
        } else {
            getString(R.string.main_warning_fourth_chunk_singular)
        }

        warningMessage += getString(R.string.main_warning_fifth_chunk)

        sensorWarningLayout
            .findViewById<TextView>(R.id.txtAvisoSensores)
            .text = warningMessage

        sensorWarningLayout.findViewById<ImageView>(R.id.btnFecharCardview)
            .setOnClickListener {
                findViewById<RelativeLayout>(R.id.rly_main_fillable)
                    .removeView(sensorWarningLayout)

                prefs.edit().putBoolean(KEY_IS_WARN_SENSOR, false).apply()
            }

        findViewById<RelativeLayout>(R.id.rly_main_fillable).addView(sensorWarningLayout)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_main_drawer)

        clearTemporaryFiles()

        val toolbar = findViewById<Toolbar>(R.id.tlb_main)
        setSupportActionBar(toolbar)
        title = ""
        drawerLayout = findViewById(R.id.drawer_main)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.ok,
            R.string.cancel
        )

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navigationView = findViewById(R.id.navview_main)
        navigationView.setNavigationItemSelectedListener listener@{ item ->
            if (item.isChecked) {
                drawerLayout.closeDrawer(GravityCompat.START)
                return@listener false
            }

            when (item.itemId) {
                R.id.item_main_drawer_fml -> launchActivity<FmlMainActivity>()
                R.id.item_main_drawer_swing -> launchActivity<SwingMainActivity>()
                R.id.item_main_drawer_chair -> launchActivity<ChairMainActivity>()
                R.id.item_main_drawer_tug -> launchActivity<TugMainActivity>()
            }

            drawerLayout.closeDrawer(GravityCompat.START)
            return@listener false
        }

        findViewById<LinearLayout>(R.id.lly_main_about).setOnClickListener {
            ToneGenerator(AudioManager.STREAM_MUSIC, 75)
                .startTone(ToneGenerator.TONE_CDMA_PIP, 20)
        }

        Thread.setDefaultUncaughtExceptionHandler(
            ExceptionManager(getExternalFilesDir(null)?.path ?: "")
        )

        if (!canWrite) {
            requestWritePermission(WRITE_PERMISSION_REQUEST_CODE)
        }

        btnStartRecording = findViewById(R.id.lly_main_start)
        mainLayout = findViewById(R.id.rly_main)

        generateSensorWarning()

        btnStartRecording.setOnClickListener {
            val dialogLayout = inflate(R.layout.dialog_main_filename)
            val edtFilename = dialogLayout.findViewById<EditText>(R.id.edtDialog)
            edtFilename.hint = getString(R.string.filename)

            val checkBoxTempRec = dialogLayout.findViewById<CheckBox>(R.id.cbRegistroTemp)
            checkBoxTempRec.setOnCheckedChangeListener { _, isChecked ->
                edtFilename.isEnabled = !isChecked
            }

            launchDialog(getString(R.string.main_dialog_filename_title)) { builder ->
                builder.setView(dialogLayout)
                    .setPositiveButton(getString(R.string.start)) { dialog, _ ->
                        val isTempRecording = checkBoxTempRec.isChecked
                        val filename = edtFilename.text.toString()

                        val recordingDirectory = if (!isTempRecording) {
                            getSaveDirectory(filename)
                        } else {
                            ""
                        }

                        if (!isTempRecording && filename.isEmpty()) {
                            toastLong(getString(R.string.main_toast_invalid_filename))

                        } else {
                            dialog.dismiss()
                            startRecording(recordingDirectory, RECORDING_MODE_DEFAULT)
                        }
                    }
                    .setNegativeButton(getString(R.string.cancel), dismissAction)
            }
        }

        if (prefs.getBoolean(KEY_IS_FIRST_EXECUTION, true)) {
            prefs.edit()
                .putBoolean(KEY_IS_FIRST_EXECUTION, false)
                .putInt(KEY_RECORDING_DELAY, OPTION_DELAY_ULTRA_FAST)
                .putBoolean(KEY_IS_LIMIT_RECORDING, false)
                .putInt(KEY_MAXIMUM_NUMBER_LINES, 120)
                .putInt(KEY_FILE_EXTENSION, OPTION_CSV_EXTENSION)
                .putInt(KEY_CALCULATION_MODE, OPTION_CALCULATION_RAW)
                .putBoolean(KEY_IS_COMPACT_AXIS, true)
                .putInt(KEY_VISIBLE_GRAPHS, 0b11)
                .putInt(KEY_ORDER_RECORDS_BY_TYPE, OPTION_ORDER_BY_DATE)
                .putInt(KEY_ORDER_RECORDS_MODE, OPTION_ORDER_DESCENDING)
                .apply()
        }

        findViewById<LinearLayout>(R.id.lly_main_options)
            .setOnClickListener { launchActivity<OptionsActivity>() }

        findViewById<Button>(R.id.btn_main_records)
            .setOnClickListener { launchActivity<RecordsActivity>() }
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()

        navigationView.menu.iterator().forEach { item -> item.isChecked = false }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_main_bluetooth -> setBluetoothLayout()
        }

        return true
    }

    private fun startRecording(fileDirectory: String, recordingMode: Int) {
        if (canWrite) {
            val data: (Intent) -> Unit = { it.putExtra(KEY_FILE_DIRECTORY, fileDirectory) }

            when (recordingMode) {
                RECORDING_MODE_DEFAULT -> launchActivity<MainRecorderActivity>(data)
                RECORDING_MODE_BLUETOOTH -> launchActivity<BluetoothRecorderActivity>(data)
            }

            finish()

        } else {
            toastLong(getString(R.string.main_toast_cant_start))
        }
    }

    private fun setBluetoothLayout() {
        toast(getString(R.string.main_toast_launch_bluetooth))
        mainLayout.removeView(btnStartRecording)
        startRecording(getSaveDirectory(BLUETOOTH_RECORD_SUFFIX), RECORDING_MODE_BLUETOOTH)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        when (requestCode) {
            WRITE_PERMISSION_REQUEST_CODE -> {
                if (!(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    toastLong(getString(R.string.main_toast_cant_write))
                    return
                }
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}

