package com.example.momentumapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.momentumapp.option.*
import com.example.momentumapp.util.*

class OptionsActivity : AppCompatActivity(), OptionContainer {
    private var options = mutableListOf<BaseOptionType>()
    private lateinit var optionsAdapter: OptionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options)
        setSupportActionBar(findViewById(R.id.tlb_options))

        val optionRecordingFrequency = OptionRadioButtonType.Builder(this)
            .setTitle(getString(R.string.options_recording_delay_title))
            .setKey(KEY_RECORDING_DELAY)
            .addValue(
                getString(R.string.options_recording_delay_item_normal),
                getString(R.string.options_recording_delay_subtitle_normal),
                OPTION_DELAY_NORMAL
            )
            .addValue(
                getString(R.string.options_recording_delay_item_above_normal),
                getString(R.string.options_recording_delay_subtitle_above_normal),
                OPTION_DELAY_ABOVE_NORMAL
            )
            .addValue(
                getString(R.string.options_recording_delay_item_fast),
                getString(R.string.options_recording_delay_subtitle_fast),
                OPTION_DELAY_FAST
            )
            .addValue(
                getString(R.string.options_recording_delay_item_ultra_fast),
                getString(R.string.options_recording_delay_subtitle_ltra_fast),
                OPTION_DELAY_ULTRA_FAST
            )
            .setDefaultValue(3).build()

        val optionFileExtension = OptionRadioButtonType.Builder(this)
            .setTitle(getString(R.string.options_file_extension_title))
            .setKey(KEY_FILE_EXTENSION)
            .addValue(
                getString(R.string.options_file_extension_item_csv),
                getString(R.string.options_file_extension_subtitle_csv),
                OPTION_CSV_EXTENSION
            )
            .addValue(
                getString(R.string.options_file_extension_item_txt),
                getString(R.string.options_file_extension_subtitle_txt),
                OPTION_TXT_EXTENSION
            )
            .setDefaultValue(0).build()

        val optionLineLimit = OptionMutableSwitchType.Builder(this)
            .setTitle(getString(R.string.options_line_limit_title))
            .setKey(KEY_IS_LIMIT_RECORDING)
            .setMutableValueKey(KEY_MAXIMUM_NUMBER_LINES)
            .setOnValue(getString(R.string.options_line_limit_subtitle_enabled))
            .setOffValue(getString(R.string.options_line_limit_subtitle_disabled))
            .setDefaultMutableValue(120).setOffValueAsDefault().build()

        val optionDecimalPlaces = OptionMutableType.Builder<Int>()
            .setTitle("Definir casas decimais")
            .setKey(KEY_DECIMAL_PLACES)
            .setDefaultValue(prefs.getInt(KEY_DECIMAL_PLACES, 2))
            .setDescription("%d casas decimais")
            .setHint("Número de casas decimais")
            .setTypeDecoding(String::toInt).build()

        val optionDecimalSeparator = OptionRadioButtonType.Builder(this)
            .setTitle("Definir separador decimal")
            .setKey(KEY_DECIMAL_SEPARATOR)
            .addValue(
                "Vírgula",
                "O separador de casas decimais será a vírgula.",
                OPTION_SEPARATOR_COMMA
            )
            .addValue(
                "Ponto",
                "O separator de casas decimais será o ponto.",
                OPTION_SEPARATOR_PERIOD
            )
            .setDefaultValue(1).build()

        val optionCalculationMode = OptionRadioButtonType.Builder(this)
            .setTitle(getString(R.string.options_calculation_mode_title))
            .setKey(KEY_CALCULATION_MODE)
            .addValue(
                getString(R.string.options_calculation_mode_item_raw),
                getString(R.string.options_calculation_mode_subtitle_raw),
                OPTION_CALCULATION_RAW
            )
            .addValue(
                getString(R.string.options_calculation_mode_item_variation),
                getString(R.string.options_calculation_mode_subtitle_variation),
                OPTION_CALCULATION_VARIATION
            )
            .setDefaultValue(0).build()

        val optionCompactAxis = OptionSwitchType.Builder(this)
            .setTitle(getString(R.string.options_compact_axis_title))
            .setKey(KEY_IS_COMPACT_AXIS)
            .setOnValue(getString(R.string.options_compact_axis_subtitle_enabled))
            .setOffValue(getString(R.string.options_compact_axis_subtitle_disabled))
            .setOnValueAsDefault().build()

        val optionVisibleGraphs = OptionCheckBoxType.Builder(this)
            .setTitle(getString(R.string.options_visible_graphs_title))
            .setKey(KEY_VISIBLE_GRAPHS)
            .addValue(
                getString(R.string.accelerometer),
                getString(R.string.options_visible_graphs_subtitle_accelerometer)
            )
            .addValue(
                getString(R.string.gyroscope),
                getString(R.string.options_visible_graphs_subtitle_gyroscope)
            )
            .setDefaultValues(0b11)
            .setDefaultMessage(getString(R.string.options_visible_graphs_subtitle_none)).build()

        val optionSensorWarning = OptionSwitchType.Builder(this)
            .setTitle(getString(R.string.options_warn_sensors_title))
            .setKey(KEY_IS_WARN_SENSOR)
            .setOnValue(getString(R.string.options_warn_sensors_subtitle_enabled))
            .setOffValue(getString(R.string.options_warn_sensors_subtitle_disabled))
            .setOnValueAsDefault().build()

        options = mutableListOf(
            optionRecordingFrequency,
            optionFileExtension,
            optionLineLimit,
            optionDecimalPlaces,
            optionDecimalSeparator,
            optionCalculationMode,
            optionCompactAxis,
            optionVisibleGraphs,
            optionSensorWarning
        )

        findViewById<RecyclerView>(R.id.rvw_options).also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

            optionsAdapter = OptionAdapter(options, this)
            it.adapter = optionsAdapter
        }
    }

    override fun onOptionClicked(position: Int) {
        options[position].getDialog(this) {
            optionsAdapter.notifyItemChanged(position)

        }?.show()
    }
}