package com.example.momentumapp.plotting

/**
 * Represents the data to be plotted by a graph.
 */
class GraphData(
    /**
     * The horizontal values to be plotted.
     */
    private var xData: DoubleArray,

    /**
     * The vertical values to be plotted.
     */
    private var yData: DoubleArray,
) : Iterable<Pair<Double, Double>> {
    init {
        requireEqualSize(xData, yData)
    }

    /**
     * Checks if [xData] and [yData] have the same data, otherwise throws an error.
     */
    private fun requireEqualSize(xData: DoubleArray, yData: DoubleArray) {
        require(xData.size == yData.size) {
            "horizontal data size must be equal to the vertical data size"
        }
    }

    /**
     * Number of the points in this graph data.
     */
    val size: Int
        get() = xData.size

    /**
     * Returns the horizontal values of this graph data.
     */
    fun getXData() = xData

    /**
     * Returns the vertical values of this graph data.
     */
    fun getYData() = yData

    /**
     * Sets data to this graph data.
     *
     * [xData] and [yData] must have the same size.
     */
    fun setData(xData: DoubleArray, yData: DoubleArray) {
        requireEqualSize(xData, yData)
        this.xData = xData
        this.yData = yData
    }

    /**
     * Returns an iterator that yields a (x, y) point of this graph data.
     */
    override fun iterator(): Iterator<Pair<Double, Double>> = (xData zip yData).iterator()

    /**
     * Maps the horizontal values of this data using a [transform] function.
     */
    fun mapDomain(transform: (Double) -> Double): GraphData {
        val mappedXData = DoubleArray(size) { i -> transform(xData[i]) }
        return GraphData(mappedXData, yData)
    }

    /**
     * Maps the vertical values of this data using a [transform] function.
     */
    fun mapRange(transform: (Double) -> Double): GraphData {
        val mappedYData = DoubleArray(size) { i -> transform(yData[i]) }
        return GraphData(xData, mappedYData)
    }

    companion object {
        /**
         * Creates a graph data from a point ([x], [y]).
         */
        fun fromPoint(x: Double, y: Double): GraphData =
            GraphData(doubleArrayOf(x), doubleArrayOf(y))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GraphData

        if (!xData.contentEquals(other.xData)) return false
        if (!yData.contentEquals(other.yData)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = xData.contentHashCode()
        result = 31 * result + yData.contentHashCode()
        return result
    }
}