package com.example.momentumapp.modules.fml

import android.content.Context
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import com.example.momentumapp.MainActivity
import com.example.momentumapp.R
import com.example.momentumapp.RecorderActivity
import com.example.momentumapp.layout.SensorGraphLegend
import com.example.momentumapp.plotting.*
import com.example.momentumapp.util.*
import java.io.File
import kotlin.math.abs

class ComparatorRecorderActivity : RecorderActivity(), FollowerListener {
    /**
     * Notifies when the user exits the margin error of the base recording.
     */
    override var followerNotifier: FollowerNotifier? = null

    /**
     * Listens when the user exits the margin error of the base recording.
     */
    private var followerListener: FollowerListener? = null

    /**
     * Checks which accelerometer axes should be used in the analysis.
     *
     * You can also check using [isUsingX], [isUsingY] and [isUsingZ] properties.
     */
    private lateinit var useAxes: BooleanArray

    /**
     * Checks if the accelerometer X-axis should be used in the analysis.
     */
    private val isUsingX: Boolean get() = useAxes[0]

    /**
     * Checks if the accelerometer Y-axis should be used in the analysis.
     */
    private val isUsingY: Boolean get() = useAxes[1]

    /**
     * Checks if the accelerometer Z-axis should be used in the analysis.
     */
    private val isUsingZ: Boolean get() = useAxes[2]

    /**
     * Contains the base recording data.
     */
    private lateinit var accelerationData: RecordData

    /**
     * Checks if the analysis has already started.
     */
    private var isRecording = false

    /**
     * Contains the actual iteration of the analysis.
     */
    private var actualIteration = 1

    /**
     * Contains the total of iterations of this analysis.
     *
     * This property is set on [onCreate].
     */
    private var numberIterations = 1

    /**
     * The margin error of the base recording.
     *
     * For some value `B_a[i]` in [accelerationData], where `a` is some accelerometer axis and `i`
     * is an index of the base recording, it's verified if the acceleration value `U_a` of the user
     * satisfies
     *
     *     (U_a > B_a[i] - marginError) && (U_a < B_a[i] + marginError)
     *
     * If it does, then `i` is incremented and this verification is done again.
     */
    private var marginError = 0.0

    /**
     * Index at which the actual user has reached in the base recording data.
     *
     * When a user is inside the margin error of the base recording, this index is incremented.
     *
     * See [marginError] for details.
     */
    private var actualPos = 0

    /**
     * The tolerable ranges of the base recording.
     *
     * The first, second and third position of this list corresponds the tolerable ranges of the
     * X, Y and Z accelerometer axes of the base recording.
     *
     * See [marginError] for details.
     */
    private val tolerableRanges = mutableListOf<Range>()

    /**
     * The graph legend of the recording screen.
     */
    private var legend: SensorGraphLegend? = null

    /**
     * Number of times that the user was inside the margin error of the base recording.
     *
     * The maximum value of this property is the [List.size] value of [accelerationData].
     */
    private var hits = 0

    /**
     * Number of times that the user was outside the margin error of the base recording.
     */
    private var misses = 0

    /**
     * [totalHits] values from all iterations.
     *
     * The position `i` of this array corresponds the number of hits of the iteration `i+1`.
     */
    private var totalHits = intArrayOf()

    /**
     * [totalMisses] values from all iterations.
     *
     * The position `i` of this array corresponds the number of misses of the iteration `i+1`.
     */
    private var totalMisses = intArrayOf()

    /**
     * How much the user was far from the base recording considering the accelerometer X-axis.
     */
    private var xError = 0.0

    /**
     * How much the user was far from the base recording considering the accelerometer Y-axis.
     */
    private var yError = 0.0

    /**
     * How much the user was far from the base recording considering the accelerometer Z-axis.
     */
    private var zError = 0.0

    /**
     * [xError] values from all iterations.
     *
     * The position `i` of this array corresponds the X-axis error of the iteration `i+1`.
     */
    private var xErrors = doubleArrayOf()

    /**
     * [yError] values from all iterations.
     *
     * The position `i` of this array corresponds the Y-axis error of the iteration `i+1`.
     */
    private var yErrors = doubleArrayOf()

    /**
     * [zError] values from all iterations.
     *
     * The position `i` of this array corresponds the Z-axis error of the iteration `i+1`.
     */
    private var zErrors = doubleArrayOf()

    /**
     * Represents a warner that emits a buzz when the user is out of the margin error.
     */
    class BuzzWarner(duration: Int) : NeutralWarner(duration) {
        val toneGen = ToneGenerator(AudioManager.STREAM_MUSIC, 100)

        override fun warn() {
            toneGen.startTone(ToneGenerator.TONE_CDMA_PIP, duration)
        }
    }

    /**
     * Represents a warner that vibrates the device when the user is out of the margin error.
     */
    class VibrationWarner(
        private val vibrator: Vibrator?,
        duration: Int,
    ) : LeveledWarner(duration, Range(1.0, 255.0)) {
        override fun warn(value: Double) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator?.vibrate(VibrationEffect.createOneShot(duration.toLong(), value.toInt()))
            } else {
                vibrator?.vibrate(duration.toLong())
            }
        }
    }

    override val isGyroscopeGraph: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recorder)

        val layoutGraphs = findViewById<LinearLayout>(R.id.lly_recorder_graphs)
        layoutGraphs.inflateGraphs()

        val btnPause = findViewById<Button>(R.id.btn_recorder_pause)
        val btnStop = findViewById<Button>(R.id.btn_recorder_stop)

        actualIteration = intent?.getIntExtra(KEY_ACTUAL_ITERATION, 1) ?: 1
        numberIterations = intent?.getIntExtra(KEY_NUMBER_ITERATIONS, 1) ?: 1
        marginError = intent?.getDoubleExtra(KEY_ERROR_MARGIN, 0.0) ?: 0.0
        useAxes = intent?.getBooleanArrayExtra(KEY_AXES_INCLUDED) ?: 0b111.toBooleanArray(3)

        totalHits += intent?.getIntArrayExtra(KEY_TOTAL_HITS) ?: intArrayOf()
        totalMisses += intent?.getIntArrayExtra(KEY_TOTAL_MISSES) ?: intArrayOf()

        xErrors += intent?.getDoubleArrayExtra(KEY_X_MEAN_ERROR) ?: doubleArrayOf()
        yErrors += intent?.getDoubleArrayExtra(KEY_Y_MEAN_ERROR) ?: doubleArrayOf()
        zErrors += intent?.getDoubleArrayExtra(KEY_Z_MEAN_ERROR) ?: doubleArrayOf()

        if (numberIterations > 1) {
            toast("$actualIteration/$numberIterations")
        }

        followerListener = this

        val vibrator = (getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator)
            ?.takeIf { it.hasVibrator() }

        followerNotifier = when (intent?.getIntExtra(KEY_WARNER_TYPE, WARNER_TYPE_VIBRATOR)) {
            WARNER_TYPE_BUZZER -> object : NeutralFollowerNotifier {
                override val warner: NeutralWarner? = BuzzWarner(recordingDelay)
            }

            WARNER_TYPE_VIBRATOR -> object : LeveledFollowerNotifier {
                override val warner: LeveledWarner? = VibrationWarner(vibrator, recordingDelay)
            }

            else -> null
        }

        RecordLoader.fromPath(fileDirectory).accelerationData?.also { data ->
            accelerationData = data
            plotBaseRecording()
        }

        startRegistering()

        var buttonStatus = BUTTON_TYPE_START
        btnPause.text = getString(R.string.start)
        btnPause.setOnClickListener {
            when (buttonStatus) {
                BUTTON_TYPE_START -> {
                    btnPause.text = getString(R.string.pause)
                    buttonStatus = BUTTON_TYPE_PAUSE
                    startRecording()
                }

                BUTTON_TYPE_PAUSE -> {
                    btnPause.text = getString(R.string.resume)
                    buttonStatus = BUTTON_TYPE_RESUME
                    pauseRecording()
                }

                BUTTON_TYPE_RESUME -> {
                    btnPause.text = getString(R.string.pause)
                    buttonStatus = BUTTON_TYPE_PAUSE
                    resumeRecording()
                }
            }
        }

        btnStop.setOnClickListener { onUserCancelled() }
    }

    /**
     * Checks if the user has reached the end of the base recording.
     */
    private val canFinish: Boolean
        get() = actualPos == accelerationData.duration.size

    /**
     * Plots the base recording in the recording screen.
     */
    private fun plotBaseRecording() {
        val xData = accelerationData.duration
        val xGraphData = GraphData(xData, accelerationData.xAxis)
        val yGraphData = GraphData(xData, accelerationData.yAxis)
        val zGraphData = GraphData(xData, accelerationData.zAxis)

        sensorPlotter.plot(
            xGraphData.takeIf { isUsingX },
            yGraphData.takeIf { isUsingY },
            zGraphData.takeIf { isUsingZ },
            intArrayOf(
                Color.RED.mixWithColor(Color.WHITE, .5f),
                Color.GREEN.mixWithColor(Color.WHITE, .5f),
                Color.BLUE.mixWithColor(Color.WHITE, .5f)
            )
        ) { graph -> FillBetweenGraphPlotter(graph, marginError, true) }

        sensorPlotter.plot(
            xGraphData.takeIf { isUsingX },
            yGraphData.takeIf { isUsingY },
            zGraphData.takeIf { isUsingZ },
            intArrayOf(
                Color.RED.mixWithColor(Color.BLACK, .5f),
                Color.GREEN.mixWithColor(Color.BLACK, .5f),
                Color.BLUE.mixWithColor(Color.BLACK, .5f)
            )
        ) { graph ->
            CurveGraphPlotter(
                graph,
                resizeHorizontally = false,
                resizeVertically = false,
            )
        }

        updateTolerableRanges()
    }

    /**
     * Updates the tolerable ranges as the user proceeds in the analysis.
     */
    private fun updateTolerableRanges() {
        val (_, trueX, trueY, trueZ) = accelerationData[actualPos]
        val xRange = Range(trueX - marginError, trueX + marginError)
        val yRange = Range(trueY - marginError, trueY + marginError)
        val zRange = Range(trueZ - marginError, trueZ + marginError)

        if (tolerableRanges.isEmpty()) {
            tolerableRanges.add(xRange)
            tolerableRanges.add(yRange)
            tolerableRanges.add(zRange)

        } else {
            tolerableRanges[0] = xRange
            tolerableRanges[1] = yRange
            tolerableRanges[2] = zRange
        }
    }

    override fun startRecording() {
        isRecording = true
    }

    override fun stopRecording() {
        stopRegistering()
        isRecording = false

        (followerNotifier?.warner as? BuzzWarner)?.toneGen?.release()

        totalHits += intArrayOf(hits)
        totalMisses += intArrayOf(misses)

        xErrors += doubleArrayOf(xError)
        yErrors += doubleArrayOf(yError)
        zErrors += doubleArrayOf(zError)

        if (!canFinish) {
            toast(getString(R.string.fml_recorder_toast_cancelled))
            launchActivity<MainActivity>()
            finish()

        } else if (actualIteration == numberIterations) {
            toast(getString(R.string.fml_recorder_toast_succeded))

            if (intent?.getBooleanExtra(KEY_IS_TEMPORARY_FILE, false) == true) {
                File(fileDirectory).delete()
            }

            launchActivity<ResultActivity> { intent ->
                intent.putExtra(KEY_TOTAL_HITS, totalHits)
                intent.putExtra(KEY_TOTAL_MISSES, totalMisses)
                intent.putExtra(KEY_RECORDING_DELAY, recordingDelay)

                if (isUsingX) intent.putExtra(KEY_X_MEAN_ERROR,
                    DoubleArray(xErrors.size) { i -> xErrors[i] / accelerationData.duration.size })

                if (isUsingY) intent.putExtra(KEY_Y_MEAN_ERROR,
                    DoubleArray(yErrors.size) { i -> yErrors[i] / accelerationData.duration.size })

                if (isUsingZ) intent.putExtra(KEY_Z_MEAN_ERROR,
                    DoubleArray(zErrors.size) { i -> zErrors[i] / accelerationData.duration.size })
            }

            finish()

        } else {
            restartActivity()
        }
    }

    override fun pauseRecording() {
        isRecording = false
    }

    override fun resumeRecording() {
        isRecording = true
    }

    private fun restartActivity() {
        startActivity(intent.also { intent ->
            intent.putExtra(KEY_ACTUAL_ITERATION, actualIteration + 1)
            intent.putExtra(KEY_TOTAL_HITS, totalHits)
            intent.putExtra(KEY_TOTAL_MISSES, totalMisses)
            intent.putExtra(KEY_X_MEAN_ERROR, xErrors)
            intent.putExtra(KEY_Y_MEAN_ERROR, yErrors)
            intent.putExtra(KEY_Z_MEAN_ERROR, zErrors)
        })

        finish()
        overridePendingTransition(0, 0)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null || event.sensor.type != Sensor.TYPE_ACCELEROMETER) return

        val (userX, userY, userZ) = event.values.map(Float::toDouble)
        val (xRange, yRange, zRange) = tolerableRanges
        val isXInRange = !isUsingX || userX in xRange
        val isYInRange = !isUsingY || userY in yRange
        val isZInRange = !isUsingZ || userZ in zRange

        if (isRecording) {
            if (isXInRange && isYInRange && isZInRange) {
                followerListener?.onUserInRange(arrayOf(userX, userY, userZ))

                ++actualPos
                if (!canFinish) {
                    updateTolerableRanges()
                }

            } else {
                followerListener?.onUserOutOfRange(
                    listOf(userX, userY, userZ),
                    tolerableRanges.mapIndexed { i, range -> range.takeIf { useAxes[i] } }
                )
            }
        }

        legend?.xAxisColor = if (isXInRange) Color.RED else Color.BLACK
        legend?.yAxisColor = if (isYInRange) Color.GREEN else Color.BLACK
        legend?.zAxisColor = if (isZInRange) Color.BLUE else Color.BLACK

        if (canFinish) {
            stopRecording()

        } else {
            val (xPos, _, _, _) = accelerationData[actualPos]

            sensorPlotter.plot(
                GraphData.fromPoint(xPos, userX).takeIf { isUsingX },
                GraphData.fromPoint(xPos, userY).takeIf { isUsingY },
                GraphData.fromPoint(xPos, userZ).takeIf { isUsingZ },
                intArrayOf(Color.RED, Color.GREEN, Color.BLUE), true
            ) { graph -> ScatterGraphPlotter(graph, false) }
        }
    }

    override fun onUserCancelled() {
        stopRecording()
    }

    override fun onUserInRange(values: Array<Double>) {
        ++hits

        val (userX, userY, userZ) = values
        val (xPos, trueX, trueY, trueZ) = accelerationData[actualPos]

        sensorPlotter.plot(
            GraphData.fromPoint(xPos, userX).takeIf { isUsingX },
            GraphData.fromPoint(xPos, userY).takeIf { isUsingY },
            GraphData.fromPoint(xPos, userZ).takeIf { isUsingZ },
            intArrayOf(
                Color.RED - 0x010000,
                Color.GREEN - 0x000100,
                Color.BLUE - 0x000001
            ),
            true
        ) { graph ->
            RealTimeGraphPlotter.Builder()
                .withGraph(graph)
                .setTimeWindow(8)
                .setTimeDelay(recordingDelay * .001)
                .setManualScroll(true)
                .setVerticalResize(false)
                .build()
        }

        xError += abs((trueX - userX) / trueX)
        yError += abs((trueY - userY) / trueY)
        zError += abs((trueZ - userZ) / trueZ)
    }

    override fun onUserOutOfRange(values: List<Double>, ranges: List<Range?>) {
        super.onUserOutOfRange(values, ranges)
        ++misses
    }
}
