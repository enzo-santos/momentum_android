package com.example.momentumapp.option

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.example.momentumapp.R
import com.example.momentumapp.util.*

/**
 * Implementation of a multiple choice option.
 */
class OptionCheckBoxType(
    title: String,
    saveKey: String,

    /**
     * The available values for this option subtitle. This subtitle will be changed according to
     * the selected value in the dialog by the user.
     */
    val descriptions: Array<String>,

    /**
     * The available values for the items in the dialog.
     */
    val dialogValues: Array<String>,

    /**
     * The values that will already be selected when opening the dialog.
     *
     * The status of the selected values will be read when converting this value to base 2. If the
     * value 2 (0b10) is passed as a parameter, only the value of position 2 will be selected,
     * since the position 2 bit is set, from right to left.
     */
    var selectedValues: Int,

    /**
     * The value that will appear in this option subtitle if no value in the dialog is selected.
     */
    val noSelectedValueMessage: String,
) : BaseOptionType(title, OPTION_TYPE_CHECKBOX, saveKey) {
    class Builder(context: Context) {
        private val prefs = context.prefs
        private var optionTitle = ""
        private var optionKey = ""
        private var optionItems = arrayListOf<String>()
        private var optionDescriptions = arrayListOf<String>()
        private var optionSelectedItems = 0
        private var optionDefaultMessage = ""

        fun setTitle(value: String) = apply { optionTitle = value }
        fun setKey(value: String) = apply { optionKey = value }
        fun setDefaultMessage(value: String) = apply { optionDefaultMessage = value }
        fun addValue(name: String, description: String) = apply {
            optionItems.add(name)
            optionDescriptions.add(description)
        }

        fun setDefaultValues(values: Int) = apply {
            optionItems.indices.forEach { i ->
                optionSelectedItems = optionSelectedItems.set(i, values[i])
            }
        }

        fun build() = OptionCheckBoxType(
            title = optionTitle,
            saveKey = optionKey,
            descriptions = optionDescriptions.toTypedArray(),
            dialogValues = optionItems.toTypedArray(),
            selectedValues = prefs.getInt(optionKey, optionSelectedItems),
            noSelectedValueMessage = optionDefaultMessage
        )
    }

    /**
     * Number of options available for selection.
     */
    val size: Int get() = descriptions.size

    class ViewHolder(
        view: View,

        /**
         * Container associated with this option layout.
         */
        container: OptionContainer,
    ) : BaseViewHolder(view) {
        override val txtOptionTitle: TextView = view.findViewById(R.id.txt_sel_option_title)
        override val txtOptionDescription: TextView =
            view.findViewById(R.id.txt_sel_option_description)
        override val layoutOption: LinearLayout = view.findViewById(R.id.lly_sel_option)

        init {
            layoutOption.setOnClickListener {
                container.onOptionClicked(absoluteAdapterPosition)
            }
        }
    }

    override fun getDialog(
        context: Context,
        action: () -> Unit,
    ): AlertDialog = AlertDialog.Builder(context, R.style.AppDialog)
        .setTitle(title)
        .setPositiveButton(context.getString(R.string.ok), dismissAction)
        .setMultiChoiceItems(
            dialogValues,
            selectedValues.toBooleanArray(size)
        ) { _, which, isChecked ->
            selectedValues = selectedValues.set(which, isChecked)
            save(context, selectedValues)
            action()
        }
        .create()

    override fun onBindViewHolder(holder: BaseViewHolder) {
        holder.txtOptionTitle.text = title

        val selectedValuesNames = dialogValues.filterIndexed { i, _ -> selectedValues[i] }
        holder.txtOptionDescription.text = if (selectedValuesNames.isEmpty()) {
            noSelectedValueMessage
        } else {
            selectedValuesNames.joinToString(", ")
        }
    }
}