package com.example.momentumapp.layout

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.StyleableRes
import com.example.momentumapp.R
import com.example.momentumapp.util.upper
import com.jjoe64.graphview.GraphView

class SensorGraphView : LinearLayout {
    @StyleableRes
    private val indexAxisLabel = 0

    @StyleableRes
    private val indexSensorLabel = 1

    @StyleableRes
    private val indexHorizontalLabel = 2

    @StyleableRes
    private val indexVerticalLabel = 3

    @StyleableRes
    private val indexIsHideGraphInfo = 4

    @StyleableRes
    private val indexIsFillGraph = 5

    private val attrs: AttributeSet?
    private lateinit var graphInfoLayout: LinearLayout
    private lateinit var axisLabel: TextView
    private lateinit var sensorLabel: TextView
    lateinit var graph: GraphView

    constructor (context: Context) : super(context) {
        attrs = null
        initComponents()
    }

    constructor (context: Context, attrSet: AttributeSet) : super(context, attrSet) {
        attrs = attrSet
        initComponents()
    }

    constructor(
        context: Context,
        attrSet: AttributeSet,
        defStyleAttr: Int,
    ) : super(context, attrSet, defStyleAttr) {
        attrs = attrSet
        initComponents()
    }

    var sensorAxis: String
        get() = axisLabel.text.toString().upper()
        set(value) {
            axisLabel.text = value
        }

    var sensorName: String
        get() = sensorLabel.text.toString()
        set(value) {
            sensorLabel.text = value
        }

    var isHideGraphInfo: Boolean
        get() = graphInfoLayout.visibility == View.GONE
        set(value) {
            graphInfoLayout.visibility = if (value) View.GONE else View.VISIBLE
        }

    fun fillGraph() {
        graph.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )
    }

    fun setOnGraphClickListener(onClickListener: (View) -> Unit) {
        graph.setOnClickListener(onClickListener)
    }

    private fun initComponents() {
        inflate(context, R.layout.view_sensor_graph_view, this)

        graphInfoLayout = findViewById(R.id.lly_graph_info)
        axisLabel = findViewById(R.id.txt_graph_axis)
        sensorLabel = findViewById(R.id.txt_graph_sensor)
        graph = findViewById(R.id.gvw_graph_data)

        minX = .0
        maxX = .0
        graph.gridLabelRenderer.isHighlightZeroLines = false

        attrs ?: return
        val attrsArray = context.obtainStyledAttributes(
            attrs,
            intArrayOf(
                R.attr.axis,
                R.attr.sensor,
                R.attr.xLabel,
                R.attr.yLabel,
                R.attr.isHideInfo,
                R.attr.isFillGraph
            )
        )

        val attrSensorAxis = attrsArray.getText(indexAxisLabel)?.toString() ?: ""
        val attrSensorName = attrsArray.getText(indexSensorLabel)?.toString() ?: ""
        val attrHorizontalAxisLabel = attrsArray.getText(indexHorizontalLabel)?.toString() ?: ""
        val attrVerticalAxisLabel = attrsArray.getText(indexVerticalLabel)?.toString() ?: ""
        val attrIsHideGraphInfo = attrsArray.getBoolean(indexIsHideGraphInfo, false)
        val attrIsFillGraph = attrsArray.getBoolean(indexIsFillGraph, false)

        attrsArray.recycle()

        sensorAxis = attrSensorAxis
        sensorName = attrSensorName
        horizontalAxisLabel = attrHorizontalAxisLabel
        verticalAxisLabel = attrVerticalAxisLabel
        isHideGraphInfo = attrIsHideGraphInfo

        if (attrIsFillGraph) fillGraph()
    }

    var minX: Double
        get() = graph.viewport.getMinX(false)
        set(value) {
            val viewport = graph.viewport
            if (!viewport.isXAxisBoundsManual) {
                viewport.isXAxisBoundsManual = true
            }

            viewport.setMinX(value)
        }

    var maxX: Double
        get() = graph.viewport.getMaxX(false)
        set(value) {
            val viewport = graph.viewport
            if (!viewport.isXAxisBoundsManual) {
                viewport.isXAxisBoundsManual = true
            }

            viewport.setMaxX(value)
        }

    var horizontalAxisLabel: String
        get() = graph.gridLabelRenderer.horizontalAxisTitle
        set(value) {
            graph.gridLabelRenderer.horizontalAxisTitle = value
        }

    var verticalAxisLabel: String
        get() = graph.gridLabelRenderer.verticalAxisTitle
        set(value) {
            graph.gridLabelRenderer.verticalAxisTitle = value
        }
}