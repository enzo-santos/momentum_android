package com.example.momentumapp.plotting

import com.example.momentumapp.util.getOrDefault
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.BaseSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.PointsGraphSeries

/**
 * Uses Cartesian coordinates to plot values in a graph.
 */
class ScatterGraphPlotter(
    graphView: GraphView,
    resizeVertically: Boolean,
) : GraphPlotter(graphView, resizeVertically) {
    private val graphSeries: MutableMap<Int, BaseSeries<DataPoint>> = hashMapOf()

    override fun plot(data: GraphData, color: Int) {
        require(data.size == 1) { "You can only scatter one point at a time" }
        super.plot(data, color)

        val (xPos, yPos) = data.first()

        val series: BaseSeries<DataPoint>
        series = graphSeries.getOrDefault(color) {
            val newSeries = PointsGraphSeries<DataPoint>(arrayOf(DataPoint(xPos, yPos)))
            newSeries.color = color
            newSeries.size = 5f
            newSeries
        }

        if (color in graphSeries) {
            series.resetData(arrayOf(DataPoint(xPos, yPos)))
        } else {
            graphSeries[color] = series
            graphView.addSeries(series)
        }
    }
}