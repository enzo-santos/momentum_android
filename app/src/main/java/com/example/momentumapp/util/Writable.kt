package com.example.momentumapp.util

/**
 * Represents an object that can be represented in a file.
 */
interface Writable {
    /**
     * The header of the file related to the implemented object.
     */
    val header: String?

    /**
     * The content of the file related to the implemented object.
     */
    val content: String?
}