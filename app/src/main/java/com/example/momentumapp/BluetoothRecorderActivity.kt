package com.example.momentumapp

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.momentumapp.layout.SensorGraphView
import com.example.momentumapp.plotting.*
import com.example.momentumapp.util.*
import java.io.FileWriter

class BluetoothRecorderActivity : InertialRecorderActivity() {
    private var writer: FileWriter? = null
    private lateinit var timeFilter: TimePrecisionFilter

    private var userType = USER_TYPE_SENDER
    internal var bluetoothManager: BluetoothManager? = null
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    companion object {
        private const val ENABLE_BLUETOOTH_REQUEST_CODE = 0x01
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        timeFilter = TimePrecisionFilter(recordingDelay)

        if (bluetoothAdapter == null) {
            toastLong(getString(R.string.recorder_bluetooth_toast_unavailable))
            stopRecording()

        } else if (!bluetoothAdapter.isEnabled) {
            startActivityForResult(
                Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                ENABLE_BLUETOOTH_REQUEST_CODE
            )

        } else {
            setBluetoothLayout()
        }
    }

    private fun showPairedDevices() {
        val pairedDevices = bluetoothAdapter.bondedDevices.toList()

        AlertDialog.Builder(this, R.style.AppDialog)
            .setTitle(getString(R.string.recorder_bluetooth_dialog_devices_title))
            .setCancelable(false)
            .setItems(pairedDevices.map { it.name }.toTypedArray()) { dialog, which ->
                bluetoothManager?.startSender(pairedDevices[which], true)

                RecordingConnectionTask(this).execute()
                setSenderLayout()

                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                dialog.cancel()
                stopRecording()
            }
            .create()
            .show()
    }

    private fun setSenderLayout() {
        setContentView(R.layout.activity_recorder_sender)
        // window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun setReceiverLayout() {
        var statusReceiverButton = BUTTON_TYPE_START



        btnPause.text = getString(R.string.start)
        btnPause.setOnClickListener {
            when (statusReceiverButton) {
                BUTTON_TYPE_START -> {
                    btnPause.text = getString(R.string.pause)
                    statusReceiverButton = BUTTON_TYPE_PAUSE

                    writer = FileWriter(fileDirectory, false)
                    writer?.append(sensorContent.header)
                    bluetoothManager?.sendMessage(MESSAGE_START_RECORDING)
                }

                BUTTON_TYPE_PAUSE -> {
                    btnPause.text = getString(R.string.resume)
                    statusReceiverButton = BUTTON_TYPE_RESUME
                    bluetoothManager?.sendMessage(MESSAGE_PAUSE_RECORDING)
                }

                BUTTON_TYPE_RESUME -> {
                    btnPause.text = getString(R.string.pause)
                    statusReceiverButton = BUTTON_TYPE_PAUSE
                    bluetoothManager?.sendMessage(MESSAGE_RESUME_RECORDING)
                }
            }
        }

        btnStop.setOnClickListener {
            bluetoothManager?.sendMessage(MESSAGE_STOP_RECORDING)
            stopRecording()
        }
    }

    private fun setBluetoothLayout() {
        bluetoothManager = BluetoothManager(this)

        val items = arrayOf(
            getString(R.string.recorder_bluetooth_dialog_type_item_receiver),
            getString(R.string.recorder_bluetooth_dialog_type_item_sender),
        )

        AlertDialog.Builder(this, R.style.AppDialog)
            .setTitle(getString(R.string.recorder_bluetooth_dialog_type_title))
            .setCancelable(false)
            .setItems(items) { dialog, which ->
                userType = if (which == 0) USER_TYPE_RECEIVER else USER_TYPE_SENDER

                if (userType == USER_TYPE_RECEIVER) {
                    bluetoothManager?.startReceiver()

                    RecordingConnectionTask(this).execute()
                    setReceiverLayout()

                } else {
                    showPairedDevices()
                }

                dialog.dismiss()

            }.setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                dialog.cancel()
                stopRecording()
            }
            .create().show()
    }

    fun writeToFile(msg: String) {
        try {
            writer?.append(msg)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        val values = (if (msg.endsWith("\n")) msg.dropLast(1) else msg)
            .split("\n").map { line -> line.split(FILE_SEPARATOR).map(String::toDouble) }

        values.forEach { (duration, x, y, z) ->
            runOnUiThread {
                sensorPlotter.plotAccelerometer(
                    GraphData.fromPoint(duration * .001, x),
                    GraphData.fromPoint(duration * .001, y),
                    GraphData.fromPoint(duration * .001, z),
                    intArrayOf(Color.RED, Color.GREEN, Color.BLUE),
                    true
                ) { graph ->
                    RealTimeGraphPlotter.Builder()
                        .withGraph(graph)
                        .setTimeWindow(8)
                        .setTimeDelay(recordingDelay * .001)
                        .setManualScroll(false)
                        .setVerticalResize(true)
                        .build()
                }
            }

            if (checkForLineLimit()) return
        }
    }

    override fun startRecording() {
        startRegistering()

        if (fileDirectory.isNotEmpty() && userType == USER_TYPE_RECEIVER) {
            writer = FileWriter(fileDirectory, false)
        }

        writer?.append(sensorContent.header)
        timeFilter.start()
    }

    override fun stopRecording() {
        timeFilter.stop()
        stopRegistering()
        writer?.closeIfContent()
        launchActivity<MainActivity>()
        finish()
    }

    override fun pauseRecording() {
        timeFilter.pause()
    }

    override fun resumeRecording() {
        timeFilter.resume()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent) {
        sensorContent.update(event)

        timeFilter.use {
            if (userType == USER_TYPE_SENDER) {
                sensorContent.content?.also { content ->
                    bluetoothManager?.sendMessage(content)
                }
            }

            checkForLineLimit()
            sensorPlotter.updateGraphs()
            sensorContent.duration += recordingDelay
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, dataInt: Intent?) {
        if (requestCode == ENABLE_BLUETOOTH_REQUEST_CODE) {
            when (resultCode) {
                RESULT_OK -> setBluetoothLayout()
                RESULT_CANCELED -> {
                    toastLong(getString(R.string.recorder_bluetooth_toast_require_bluetooth))
                    stopRecording()
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, dataInt)
    }
}