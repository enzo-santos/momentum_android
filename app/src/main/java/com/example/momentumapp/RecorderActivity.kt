package com.example.momentumapp

import android.app.Activity
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import com.example.momentumapp.layout.SensorGraphView
import com.example.momentumapp.layout.horizontalRange
import com.example.momentumapp.modules.fml.Range
import com.example.momentumapp.plotting.*
import com.example.momentumapp.util.*
import java.io.IOException
import java.io.Writer

/**
 * Represents a activity that records sensor data.
 */
abstract class RecorderActivity : AppCompatActivity(), SensorEventListener, Recorder {
    /**
     * Returns which graphs should be present in the recording screen.
     *
     * The boolean at position 0 notifies if the accelerometer graph should be present and the
     * boolean at position 1 notifies if the gyroscope graph should be present.
     */
    private val visibleGraphs: BooleanArray
        get() = prefs.getInt(KEY_VISIBLE_GRAPHS, 0b11).toBooleanArray(2)

    /**
     * Returns the file directory which the record of this activity should be saved.
     *
     * If it's empty, no directory was specified or the record should not be saved.
     */
    protected val fileDirectory: String
        get() = intent?.getStringExtra(KEY_FILE_DIRECTORY) ?: ""

    /**
     * Contains the number of lines already written in the record file of this activity.
     */
    private var numLinesWritten: Int = 0

    /**
     * Checks if the number of lines of the record file should be limited.
     */
    private val isLimitNumberLines: Boolean
        get() = prefs.getBoolean(KEY_IS_LIMIT_RECORDING, false)

    /**
     * Returns the maximum number of lines that the record file should contain.
     */
    private val maxNumLines: Int
        get() = prefs.getInt(KEY_MAXIMUM_NUMBER_LINES, 120)

    /**
     * Returns the recording delay defined by the user.
     */
    protected val recordingDelay: Int
        get() = prefs.getInt(KEY_RECORDING_DELAY, OPTION_DELAY_ULTRA_FAST)

    /**
     * Checks if the accelerometer graph should be present in the recording screen.
     */
    protected open val isAccelerometerGraph: Boolean
        get() = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null && visibleGraphs[0]

    /**
     * Checks if the gyroscope graph should be present in the recording screen.
     */
    protected open val isGyroscopeGraph: Boolean
        get() = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null && visibleGraphs[1]

    /**
     * Checks if the graphs should be compacted in one graph instead of three graphs for each axis.
     */
    protected val isCompactAxis: Boolean
        get() = prefs.getBoolean(KEY_IS_COMPACT_AXIS, true)

    /**
     * Checks if there should be only one graph in the recording screen.
     */
    private val isSingleGraphLayout: Boolean
        get() = (isAccelerometerGraph xor isGyroscopeGraph) && isCompactAxis

    /**
     * Returns the sensor manager of the system.
     */
    private val sensorManager: SensorManager
        get() = getSystemService(SENSOR_SERVICE) as SensorManager

    /**
     * Returns a handler that manages the messages of this activity.
     */
    private val handler: Handler by lazy {
        object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                return when (msg.what) {
                    HANDLER_TOAST -> toastLong((msg.obj as? String) ?: "")
                    else -> super.handleMessage(msg)
                }
            }
        }
    }

    /**
     * Returns the default graph builder of this activity.
     */
    protected val graphLayoutBuilder: GraphLayoutBuilder by lazy {
        GraphLayoutBuilder()
            .setSingleGraphLayout(isSingleGraphLayout)
            .includeAccelerometerGraph(isAccelerometerGraph)
            .includeGyroscopeGraph(isGyroscopeGraph)
            .setCompactAxis(isCompactAxis)
    }

    /**
     * Returns the default sensor plotter of this activity.
     *
     * You should call [inflateGraphs] or [GraphLayoutBuilder.inflateGraphs] method of
     * [graphLayoutBuilder] before using this property.
     */
    protected val sensorPlotter: InertialMultiSensorPlotter by lazy {
        graphLayoutBuilder.sensorPlotter
    }

    /**
     * Returns the default sensor content of this activity.
     */
    protected val sensorContent: SensorContent by lazy {
        val decimalPlaces = prefs.getInt(KEY_DECIMAL_PLACES, 2)

        SensorContent(
            AccelerometerMetric(
                decimalSeparator,
                decimalPlaces,
                calculationMode
            ).takeIf { isAccelerometerGraph },
            GyroscopeMetric(
                decimalSeparator,
                decimalPlaces,
                calculationMode
            ).takeIf { isGyroscopeGraph },
        )
    }

    /**
     * Inflates the graphs of the sensors based on user-defined settings.
     */
    class GraphLayoutBuilder {
        companion object {
            private fun getStringLoader(activity: Activity): (Int) -> String {
                return { id -> activity.getString(id) }
            }
        }

        private var isSingleGraphLayout = true
        private var isAccelerometerGraph = true
        private var isGyroscopeGraph = true
        private var isCompactAxis = true

        private val accelerometerGraphs = mutableListOf<SensorGraphView>()
        private val gyroscopeGraphs = mutableListOf<SensorGraphView>()

        fun setSingleGraphLayout(value: Boolean) = apply {
            isSingleGraphLayout = value
        }

        fun includeAccelerometerGraph(value: Boolean) = apply {
            isAccelerometerGraph = value
        }

        fun includeGyroscopeGraph(value: Boolean) = apply {
            isGyroscopeGraph = value
        }

        fun setCompactAxis(value: Boolean) = apply {
            isCompactAxis = value
        }

        private fun ViewGroup.buildGraphs(
            sensorName: String,
            horizontalAxisLabel: String,
            verticalAxisLabel: String,
            axisLabels: Array<String>,
            graphs: MutableList<SensorGraphView>,
        ) {
            val activity = activity ?: return

            if (isCompactAxis) {
                val sensorGraphView = SensorGraphView(activity).also { graph ->
                    graph.sensorName = sensorName
                    graph.horizontalAxisLabel = horizontalAxisLabel
                    graph.verticalAxisLabel = verticalAxisLabel

                    graphs.add(graph)
                }

                addView(sensorGraphView)

            } else {
                axisLabels.forEach { axisLabel ->
                    val sensorGraphView = SensorGraphView(activity).also { graph ->
                        graph.sensorAxis = axisLabel
                        graph.sensorName = sensorName
                        graph.horizontalAxisLabel = horizontalAxisLabel
                        graph.verticalAxisLabel = verticalAxisLabel

                        graphs.add(graph)
                    }

                    addView(sensorGraphView)
                }
            }
        }

        /**
         * Returns the respective sensor plotter of this layout builder.
         *
         * Since this property acts based on [accelerometerGraphs] and [gyroscopeGraphs], before
         * calling this property, you should call [inflateGraphs].
         */
        val sensorPlotter: InertialMultiSensorPlotter
            get() {
                val accelerometerGraphs = accelerometerGraphs.map(SensorGraphView::graph)
                val gyroscopeGraphs = gyroscopeGraphs.map(SensorGraphView::graph)

                val sensorPlotter = InertialMultiSensorPlotter()
                val plotters = mutableListOf<GraphSensorPlotter>()
                if (isSingleGraphLayout) {
                    plotters.add(
                        SingleGraphSensorPlotter
                            .fromList(accelerometerGraphs.ifEmpty { gyroscopeGraphs })
                    )

                } else {
                    if (accelerometerGraphs.size == 1)
                        plotters.add(SingleGraphSensorPlotter.fromList(accelerometerGraphs))
                    else if (accelerometerGraphs.size > 1)
                        plotters.add(TripleGraphSensorPlotter.fromList(accelerometerGraphs, true))

                    if (gyroscopeGraphs.size == 1)
                        plotters.add(SingleGraphSensorPlotter.fromList(gyroscopeGraphs))
                    else if (gyroscopeGraphs.size > 1)
                        plotters.add(TripleGraphSensorPlotter.fromList(gyroscopeGraphs, true))
                }

                plotters.forEach(sensorPlotter::addSensorPlotter)
                return sensorPlotter
            }

        /**
         * Inflates the accelerometer or gyroscope graphs in the layout as children of [parentView].
         */
        fun inflateGraphs(parentView: ViewGroup) {
            val load = getStringLoader(parentView.activity ?: return)

            val horizontalAxisLabel = "${load(R.string.time)} (${load(R.string.second_unit)})"
            val axisLabels = arrayOf(
                load(R.string.x_axis),
                load(R.string.y_axis),
                load(R.string.z_axis),
            )

            if (isAccelerometerGraph) {
                parentView.buildGraphs(
                    load(R.string.accelerometer),
                    horizontalAxisLabel,
                    "${load(R.string.acceleration)} (${load(R.string.meter_per_second_unit)})",
                    axisLabels,
                    accelerometerGraphs,
                )

                if (isSingleGraphLayout)
                    accelerometerGraphs[0].fillGraph()
            }

            if (isGyroscopeGraph) {
                parentView.buildGraphs(
                    load(R.string.gyroscope),
                    horizontalAxisLabel,
                    "${load(R.string.angular_velocity)} (${load(R.string.radian_per_second_unit)})",
                    axisLabels,
                    gyroscopeGraphs,
                )

                if (isSingleGraphLayout)
                    gyroscopeGraphs[0].fillGraph()
            }

            accelerometerGraphs.forEach { graph ->
                graph.graph.horizontalRange = Range(0.0, 8.0)
//                graph.minX = 0.0
//                graph.maxX = 8.0
            }

            gyroscopeGraphs.forEach { graph ->
                graph.graph.horizontalRange = Range(0.0, 8.0)
//                graph.minX = 0.0
//                graph.maxX = 8.0
            }
        }
    }

    /**
     * Registers the accelerometer and gyroscope sensors in the application, if present.
     */
    protected fun startRegistering() {
        if (isAccelerometerGraph) {
            sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                recordingDelay * 1000,
            )
        }

        if (isGyroscopeGraph) {
            sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                recordingDelay * 1000,
            )
        }
    }

    /**
     * Unregisters the accelerometer and gyroscope sensors from the application, if registered.
     */
    protected fun stopRegistering() {
        sensorManager.unregisterListener(this)
    }

    /**
     * Plots data in the accelerometer and gyroscope graphs using this plotter.
     */
    protected fun InertialMultiSensorPlotter.updateGraphs() {
        val xData = sensorContent.duration * .001

        sensorContent.accelerometerMetric?.values?.also { (x, y, z) ->
            plotAccelerometer(
                GraphData.fromPoint(xData, x),
                GraphData.fromPoint(xData, y),
                GraphData.fromPoint(xData, z),
                intArrayOf(Color.RED, Color.GREEN, Color.BLUE), true
            ) { graph ->
                RealTimeGraphPlotter.Builder()
                    .withGraph(graph)
                    .setTimeWindow(8)
                    .setTimeDelay(recordingDelay * .001)
                    .setManualScroll(false)
                    .setVerticalResize(true)
                    .build()
            }
        }

        sensorContent.gyroscopeMetric?.values?.also { (x, y, z) ->
            plotGyroscope(
                GraphData.fromPoint(xData, x),
                GraphData.fromPoint(xData, y),
                GraphData.fromPoint(xData, z),
                intArrayOf(Color.RED, Color.GREEN, Color.BLUE), true
            ) { graph ->
                RealTimeGraphPlotter.Builder()
                    .withGraph(graph)
                    .setTimeWindow(8)
                    .setTimeDelay(recordingDelay * .001)
                    .setManualScroll(false)
                    .setVerticalResize(true)
                    .build()
            }
        }
    }

    /**
     * Checks if the line limit, if activated, has reached.
     */
    protected fun checkForLineLimit(): Boolean {
        if (isLimitNumberLines && numLinesWritten == maxNumLines) {
            handler.sendMessage { message ->
                message.what = HANDLER_TOAST
                message.obj = getString(R.string.recorder_toast_limit)
            }

            stopRecording()
            return true
        }

        return false
    }

    /**
     * Creates a writer based on some [builder] if the file saving is enabled, else returns null.
     */
    protected fun <T : Writer> createWriter(builder: () -> T): T? {
        var writer: T? = null

        if (fileDirectory.isNotEmpty()) {
            if (intent?.getBooleanExtra(KEY_IS_TEMPORARY_FILE, false) == false)
                toastLong(getString(R.string.recorder_default_toast_start, fileDirectory))

            writer = builder()
        }

        writer?.append(sensorContent.header)
        return writer
    }

    /**
     * Writes the accelerometer and gyroscope data using this writer.
     */
    protected fun Writer.writeContent() {
        try {
            sensorContent.content?.also { content -> append(content) }

        } catch (e: IOException) {
            e.printStackTrace()
        }

        ++numLinesWritten
    }

    /**
     * Inflate the graphs in this ViewGroup based in this activity graph layout builder.
     */
    protected fun ViewGroup.inflateGraphs() {
        if (isCompactAxis) {
            graphLayoutBuilder.inflateGraphs(this)

        } else {
            val parentView = ScrollView(this@RecorderActivity)
            addView(parentView)
            parentView.addView(LinearLayout(this@RecorderActivity).also { lly ->
                lly.orientation = LinearLayout.VERTICAL
                lly.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                )

                graphLayoutBuilder.inflateGraphs(lly)
            })
        }
    }

    /**
     * Try to close this writer if some content was written in it.
     */
    protected fun Writer.closeIfContent() {
        if (numLinesWritten > 1) {
            try {
                close()

            } catch (e: Exception) {
                handler.sendMessage { message ->
                    message.what = HANDLER_TOAST
                    message.obj = e.toString()
                }
            }
        }
    }

    /**
     * Updates this sensor content with some sensor [event].
     */
    protected fun SensorContent.update(event: SensorEvent) {
        when (event.sensor.type) {
            Sensor.TYPE_GYROSCOPE -> {
                gyroscopeMetric?.update(
                    event.values[0].toDouble(),
                    event.values[1].toDouble(),
                    event.values[2].toDouble()
                )
            }

            Sensor.TYPE_ACCELEROMETER -> {
                accelerometerMetric?.update(
                    event.values[0].toDouble(),
                    event.values[1].toDouble(),
                    event.values[2].toDouble()
                )
            }
        }
    }
}