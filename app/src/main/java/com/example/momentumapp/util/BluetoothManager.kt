package com.example.momentumapp.util

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import com.example.momentumapp.BluetoothRecorderActivity
import com.example.momentumapp.RecorderActivity
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.util.*

class BluetoothManager(private val activity: RecorderActivity) {
    var actualState = STATUS_NULL
        get() = synchronized(this) { field }
        set(value) = synchronized(this) { field = value }

    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var secureAcceptThread: AcceptThread? = null
    private var insecureAcceptThread: AcceptThread? = null
    private var connectThread: ConnectThread? = null
    private var connectedThread: ConnectedThread? = null

    companion object {
        private const val NAME_SECURE = "BluetoothSecure"
        private const val NAME_INSECURE = "BluetoothInsecure"
        private val MY_UUID_SECURE = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66")
        private val MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66")

        const val STATUS_NULL = 0
        const val STATUS_LISTENING = 1
        const val STATUS_CONNECTING = 2
        const val STATUS_CONNECTED = 3
    }

    fun startReceiver() = startAcceptThread()
    fun startSender(device: BluetoothDevice, secure: Boolean) = startConnectThread(device, secure)

    fun sendMessage(message: String) {
        val r = synchronized(this) {
            if (actualState != STATUS_CONNECTED) {
                null
            } else {
                connectedThread
            }
        }

        val byteMsg = message.toByteArray(Charset.defaultCharset())
        r?.write(byteMsg)
    }

    fun stopSearching() {
        connectThread = connectThread?.let {
            it.cancel()
            null
        }

        connectedThread = connectedThread?.let {
            it.cancel()
            null
        }

        secureAcceptThread = secureAcceptThread?.let {
            it.cancel()
            null
        }

        insecureAcceptThread = insecureAcceptThread?.let {
            it.cancel()
            null
        }
    }

    @Synchronized
    private fun startAcceptThread() {
        connectThread = connectThread?.let {
            it.cancel()
            null
        }

        connectedThread = connectedThread?.let {
            it.cancel()
            null
        }

        if (secureAcceptThread == null) {
            secureAcceptThread = AcceptThread(true).also(AcceptThread::start)
        }

        if (insecureAcceptThread == null) {
            insecureAcceptThread = AcceptThread(false).also(AcceptThread::start)
        }
    }

    @Synchronized
    private fun startConnectThread(device: BluetoothDevice, secure: Boolean) {
        if (actualState == STATUS_CONNECTING) {
            connectThread = connectThread?.let {
                it.cancel()
                null
            }
        }

        connectedThread = connectedThread?.let {
            it.cancel()
            null
        }

        connectThread = ConnectThread(device, secure).also(ConnectThread::start)
    }

    @Synchronized
    fun startConnectedThread(socket: BluetoothSocket) {
        stopSearching()
        connectedThread = ConnectedThread(socket).also(ConnectedThread::start)
    }

    private inner class AcceptThread(secure: Boolean) : Thread() {
        private var serverSocket: BluetoothServerSocket? = null

        init {
            serverSocket = try {
                if (secure) {
                    bluetoothAdapter.listenUsingRfcommWithServiceRecord(
                        NAME_SECURE,
                        MY_UUID_SECURE
                    )

                } else {
                    bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(
                        NAME_INSECURE,
                        MY_UUID_INSECURE
                    )
                }

            } catch (e: IOException) {
                e.printStackTrace()
                null
            }

            actualState = STATUS_LISTENING
        }

        override fun run() {
            var socket: BluetoothSocket?

            while (actualState != STATUS_CONNECTED) {
                try {
                    socket = serverSocket?.accept()

                } catch (e: IOException) {
                    break
                }

                socket?.also {
                    synchronized(this@BluetoothManager) {
                        when (actualState) {
                            STATUS_LISTENING, STATUS_CONNECTING -> startConnectedThread(it)
                            STATUS_NULL, STATUS_CONNECTED -> {
                                try {
                                    it.close()

                                } catch (e: IOException) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                }
            }
        }

        fun cancel() {
            try {
                serverSocket?.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private inner class ConnectedThread(socket: BluetoothSocket) : Thread() {
        private val connectedSocket: BluetoothSocket = socket
        private val inputStream: InputStream?
        private val outputStream: OutputStream?

        init {
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null

            try {
                tmpIn = socket.inputStream
                tmpOut = socket.outputStream

            } catch (e: IOException) {
                e.printStackTrace()
            }

            inputStream = tmpIn
            outputStream = tmpOut
            actualState = STATUS_CONNECTED
        }

        override fun run() {
            val buffer = ByteArray(1024)

            while (actualState == STATUS_CONNECTED) {
                try {
                    inputStream?.read(buffer)?.also {
                        when (val incomingMessage = String(buffer, 0, it)) {
                            MESSAGE_STOP_RECORDING -> activity.stopRecording()
                            MESSAGE_START_RECORDING -> activity.startRecording()
                            MESSAGE_PAUSE_RECORDING -> activity.pauseRecording()
                            MESSAGE_RESUME_RECORDING -> activity.resumeRecording()
                            else -> {
                                if (activity is BluetoothRecorderActivity) {
                                    activity.writeToFile(incomingMessage)
                                }
                            }
                        }
                    }

                } catch (e: IOException) {
                    break
                }
            }
        }

        fun write(buffer: ByteArray) {
            try {
                outputStream?.write(buffer)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        fun cancel() {
            try {
                connectedSocket.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private inner class ConnectThread(device: BluetoothDevice, secure: Boolean) : Thread() {
        private val connectSocket: BluetoothSocket?

        init {
            connectSocket = try {
                if (secure) {
                    device.createRfcommSocketToServiceRecord(MY_UUID_SECURE)
                } else {
                    device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE)
                }

            } catch (e: IOException) {
                e.printStackTrace()
                null
            }

            actualState = STATUS_CONNECTING
        }

        override fun run() {
            bluetoothAdapter.cancelDiscovery()

            try {
                connectSocket?.connect()

            } catch (e: IOException) {
                try {
                    connectSocket?.close()

                } catch (e: IOException) {
                    e.printStackTrace()
                }

                return
            }

            synchronized(this@BluetoothManager) {
                connectThread = null
            }

            connectSocket?.also(this@BluetoothManager::startConnectedThread)
        }

        fun cancel() {
            try {
                connectSocket?.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}