package com.example.momentumapp.option

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.momentumapp.util.prefs

/**
 * Provides functionality for a option that a user can select.
 */
abstract class BaseOptionType(
    /**
     * The title of this option.
     */
    val title: String,

    /**
     * The type of this option.
     */
    val type: Int,

    /**
     * The key whose values of this option are associated.
     */
    private val saveKey: String,
) {
    /**
     * Saves a [value] in the application [context].
     */
    fun <V> save(context: Context, value: V) {
        context.prefs.edit().also {
            when (value) {
                is Boolean -> it.putBoolean(saveKey, value)
                is Int -> it.putInt(saveKey, value)
                is String -> it.putString(saveKey, value)
                is Float -> it.putFloat(saveKey, value)
                is Long -> it.putLong(saveKey, value)
            }
        }.apply()
    }

    /**
     * Describes a option view and metadata about its place within the RecyclerView.
     */
    abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        /**
         * The parent layout associated with this option.
         */
        abstract val layoutOption: LinearLayout

        /**
         * The TextView associated with this option title.
         */
        abstract val txtOptionTitle: TextView

        /**
         * The TextView associated with this option description.
         */
        abstract val txtOptionDescription: TextView
    }

    /**
     * Updates the contents of the option layout from a [holder].
     */
    abstract fun onBindViewHolder(holder: BaseViewHolder)

    /**
     * Configures the dialog to appear when this setting is clicked.
     *
     * The parent [context] of this dialog and the [action] to be executed when the positive
     * button of this dialog is clicked must be provided.
     *
     * If this method returns null, no dialog will appear when this setting is clicked.
     */
    abstract fun getDialog(context: Context, action: () -> Unit): AlertDialog?
}