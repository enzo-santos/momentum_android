package com.example.momentumapp.util

/**
 * Represents actions to be done in a record container.
 */
interface RecordContainer {
    /**
     * Action to be done when a card is clicked in a adapter [position].
     */
    fun onCardClicked(position: Int)

    /**
     * Action to be done when the one of the options of a card in a adapter [position] is clicked.
     *
     * The [name] of the option selected in the options list can be passed as parameter.
     */
    fun onItemClicked(name: String, position: Int)

    fun onSelectionModeChanged(mode: Int)
}