package com.example.momentumapp.util

import kotlin.concurrent.thread

class TimePrecisionFilter(private val delay: Int) {
    private var canUse = true
    private var stopChecking: Boolean = false
    private var pauseChecking: Boolean = false

    private val thread = thread(start = false) {
        var startTime = System.currentTimeMillis()

        do {
            if (pauseChecking) continue

            val actualTime = System.currentTimeMillis()
            if (actualTime - startTime > delay) {
                canUse = true
                startTime = actualTime
            }

        } while (!stopChecking)
    }

    fun use(action: () -> Unit) {
        if (canUse) {
            action()
            canUse = false
        }
    }

    fun start() {
        pauseChecking = false
        stopChecking = false
        thread.start()
    }

    fun pause() {
        pauseChecking = true
        stopChecking = false
    }

    fun resume() {
        pauseChecking = false
        stopChecking = false
    }

    fun stop() {
        pauseChecking = false
        stopChecking = true
        thread.join()
    }
}