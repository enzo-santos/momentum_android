package com.example.momentumapp.option

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.momentumapp.R
import com.example.momentumapp.util.OPTION_TYPE_MUTABLE_SWITCH
import com.example.momentumapp.util.OptionContainer
import com.example.momentumapp.util.inflate
import com.example.momentumapp.util.prefs

/**
 * Implementation of an editable and binary option.
 *
 * This option can be enabled and disabled. When enabled, its value can be changed.
 */
class OptionMutableSwitchType(
    title: String,
    key: String,
    var valueKey: String,
    var descriptions: Array<String>,
    var isSwitchOn: Boolean,
    var mutableValue: Int,
) : BaseOptionType(title, OPTION_TYPE_MUTABLE_SWITCH, key) {
    class Builder(private val context: Context) {
        private var optionTitle = ""
        private var optionKey = ""
        private var optionMutableValueKey = ""
        private var optionDescriptionOn = ""
        private var optionDescriptionOff = ""
        private var isOnValueDefault = false
        private var optionDefaultMutableValue = 0

        fun setTitle(value: String) = apply { optionTitle = value }
        fun setKey(value: String) = apply { optionKey = value }
        fun setMutableValueKey(value: String) = apply { optionMutableValueKey = value }
        fun setOnValue(value: String) = apply { optionDescriptionOn = value }
        fun setOffValue(value: String) = apply { optionDescriptionOff = value }
        fun setOnValueAsDefault() = apply { isOnValueDefault = true }
        fun setOffValueAsDefault() = apply { isOnValueDefault = false }
        fun setDefaultMutableValue(value: Int) = apply { optionDefaultMutableValue = value }

        fun build(): OptionMutableSwitchType = OptionMutableSwitchType(
            title = optionTitle,
            key = optionKey,
            valueKey = optionMutableValueKey,
            mutableValue = context.prefs.getInt(optionMutableValueKey, optionDefaultMutableValue),
            descriptions = arrayOf(optionDescriptionOff, optionDescriptionOn),
            isSwitchOn = context.prefs.getBoolean(optionKey, isOnValueDefault)
        )
    }

    class ViewHolder(
        view: View,

        /**
         * The container associated with this option layout.
         */
        container: OptionContainer,

        /**
         * The parent adapter containing this option.
         */
        adapter: RecyclerView.Adapter<*>,

        /**
         * This option.
         */
        options: MutableList<BaseOptionType>,
    ) : BaseViewHolder(view) {
        override val txtOptionTitle: TextView = view.findViewById(R.id.txt_edt_option_title)
        override val txtOptionDescription: TextView =
            view.findViewById(R.id.txt_edt_option_description)
        override val layoutOption: LinearLayout = view.findViewById(R.id.lly_edt_option)
        val optionSwitch: Switch = view.findViewById(R.id.swt_option_enable)
        var isUserCheck = true

        init {
            layoutOption.setOnClickListener {
                if (optionSwitch.isChecked) {
                    container.onOptionClicked(absoluteAdapterPosition)
                }
            }

            optionSwitch.setOnCheckedChangeListener { _, isChecked ->
                if (isUserCheck) {
                    val option = options[absoluteAdapterPosition] as? OptionMutableSwitchType
                        ?: return@setOnCheckedChangeListener

                    option.isSwitchOn = isChecked
                    option.save(view.context, isChecked)
                    adapter.notifyItemChanged(absoluteAdapterPosition)
                }
            }
        }
    }

    /**
     * Saves the [value] contained in this option in the [context] of the application.
     */
    private fun saveValue(context: Context, value: Int) {
        context.prefs.edit().putInt(valueKey, value).apply()
    }

    override fun getDialog(
        context: Context,
        action: () -> Unit,
    ): AlertDialog = AlertDialog.Builder(context, R.style.AppDialog).let { builder ->
        val input = (context as Activity).inflate(R.layout.dialog_editable)
        val edtNomeArq = input.findViewById<EditText>(R.id.edtDialog).also {
            it.setText("$mutableValue")
            it.hint = context.getString(R.string.options_dialog_lines_hint)
            it.inputType = InputType.TYPE_CLASS_NUMBER
        }

        builder.setTitle(context.getString(R.string.options_dialog_lines_title))
            .setView(input)
            .setPositiveButton(context.getString(R.string.ok)) { dialog, _ ->
                mutableValue = edtNomeArq.text.toString().toInt()
                saveValue(context, mutableValue)

                action()
                dialog.dismiss()
            }.create()
    }

    override fun onBindViewHolder(holder: BaseViewHolder) {
        holder as ViewHolder

        holder.txtOptionTitle.text = title

        holder.isUserCheck = false
        holder.optionSwitch.isChecked = isSwitchOn
        holder.isUserCheck = true

        holder.txtOptionDescription.text = if (isSwitchOn) {
            descriptions[1].format(mutableValue)
        } else {
            descriptions[0]
        }
    }
}