package com.example.momentumapp.util

/**
 * Manages the three-axis sensors data related to this application.
 */
class SensorContent(
    /**
     * A accelerometer metric.
     *
     * If it's not present in the device or if it's hidden by the user, its value is null.
     */
    val accelerometerMetric: AccelerometerMetric?,

    /**
     * A gyroscope metric.
     *
     * If it's not present in the device or if it's hidden by the user, its value is null.
     */
    val gyroscopeMetric: GyroscopeMetric?,
) : Writable {
    /**
     * The actual duration of the recording.
     *
     * It should be updated every time any [SensorMetric.update] method of the [accelerometerMetric]
     * and/or the [gyroscopeMetric] is called.
     */
    var duration = 0

    override val header: String
        get() {
            val strBuilder = StringBuilder("DURAÇÃO")
            if (accelerometerMetric != null) strBuilder.append(FILE_SEPARATOR)
                .append(accelerometerMetric.header)
            if (gyroscopeMetric != null) strBuilder.append(FILE_SEPARATOR)
                .append(gyroscopeMetric.header)
            strBuilder.append("\n")
            return strBuilder.toString()
        }

    override val content: String?
        get() {
            val strBuilder = StringBuilder("$duration")

            if (accelerometerMetric != null) {
                if (accelerometerMetric.values == null) return null
                strBuilder.append(FILE_SEPARATOR).append(accelerometerMetric.content)
            }

            if (gyroscopeMetric != null) {
                if (gyroscopeMetric.values == null) return null
                strBuilder.append(FILE_SEPARATOR).append(gyroscopeMetric.content)
            }

            strBuilder.append("\n")
            return strBuilder.toString()
        }
}