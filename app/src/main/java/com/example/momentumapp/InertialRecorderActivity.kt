package com.example.momentumapp

import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import com.example.momentumapp.util.BUTTON_TYPE_PAUSE
import com.example.momentumapp.util.BUTTON_TYPE_RESUME
import com.example.momentumapp.util.ButtonController

abstract class InertialRecorderActivity : RecorderActivity() {
    protected lateinit var btnPause: Button
    protected lateinit var btnStop: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recorder)

        val layoutGraphs = findViewById<LinearLayout>(R.id.lly_recorder_graphs)
        layoutGraphs.inflateGraphs()

        btnPause = findViewById(R.id.btn_recorder_pause)
        btnStop = findViewById(R.id.btn_recorder_stop)

        ButtonController(
            btnPause,
            BUTTON_TYPE_PAUSE,
            setOf(
                ButtonController.Node(
                    BUTTON_TYPE_PAUSE,
                    BUTTON_TYPE_RESUME,
                    "Continuar",
                    ::pauseRecording,
                ),

                ButtonController.Node(
                    BUTTON_TYPE_RESUME,
                    BUTTON_TYPE_PAUSE,
                    "Pausar",
                    ::resumeRecording,
                ),
            ),
        )

//        var btnPauseMode = BUTTON_TYPE_PAUSE
//        btnPause.setOnClickListener {
//            when (btnPauseMode) {
//                BUTTON_TYPE_PAUSE -> {
//                    pauseRecording()
//                    btnPauseMode = BUTTON_TYPE_RESUME
//                    btnPause.text = getString(R.string.resume)
//                }
//
//                BUTTON_TYPE_RESUME -> {
//                    resumeRecording()
//                    btnPauseMode = BUTTON_TYPE_PAUSE
//                    btnPause.text = getString(R.string.pause)
//                }
//            }
//        }
    }
}